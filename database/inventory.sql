-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 27, 2016 at 11:07 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `alerts`
--

CREATE TABLE `alerts` (
  `id` int(11) UNSIGNED NOT NULL,
  `products` int(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alerts`
--

INSERT INTO `alerts` (`id`, `products`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_account` varchar(255) DEFAULT NULL,
  `description` text,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `user_id`, `name`, `country`, `state`, `city`, `zip`, `address`, `contact`, `phone`, `email`, `website`, `bank`, `bank_account`, `description`, `updated_at`, `created_at`) VALUES
(1, 2, 'John', 'Philippines', 'Manila', 'Quezon City', '1116', 'Quezon City', '09054879047', '1234567', 'john@testemail.com', '', '', '', '', '2016-03-25', '2016-03-25'),
(2, 3, 'Vhin', 'Phil', '', 'Valenzuela', '1440', 'Marulas', '09091234567', '123456', 'vhin@yahoo.com', 'http://www.website.com', 'Bank1', 'Bank1', 'Bank1', '2016-06-23', '2016-06-23'),
(3, 4, 'Greed', 'Phil', '', 'Valenzuela', '1440', 'Valenzuel', '0906123456', '1234567', 'greed@yahoo.com', 'http://www.domain.com', 'Bank2', 'Bank2', 'Bank2', '2016-06-23', '2016-06-23'),
(4, 5, 'Envy', 'Phil', '', 'Valenzuela', '1440', 'Valenzuela', '09051234567', '1234567', 'envy@yahoo.com', 'http://www.domain3.com', 'Bank3', 'Bank3', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ', '2016-06-23', '2016-06-23'),
(5, 6, 'Lust', NULL, NULL, NULL, NULL, 'Valenzuela', '09041234567', NULL, 'lust@yahoo.com', NULL, NULL, NULL, 'My Description', '2016-06-23', '2016-06-23'),
(6, 7, 'Pride', NULL, NULL, NULL, NULL, 'Valenzuela', '09031234567', NULL, 'pride@yahoo.com', NULL, NULL, NULL, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2016-06-23', '2016-06-23');

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

CREATE TABLE `currencies` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL,
  `position` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `user_id`, `name`, `position`) VALUES
(1, 1, 'Php', 1);

-- --------------------------------------------------------

--
-- Table structure for table `estimates`
--

CREATE TABLE `estimates` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `client_id` int(11) UNSIGNED NOT NULL,
  `currency_id` int(11) UNSIGNED NOT NULL,
  `estimate` varchar(100) DEFAULT NULL,
  `reference` varchar(100) DEFAULT NULL,
  `amount` double(10,2) UNSIGNED DEFAULT NULL,
  `discount` double(10,2) UNSIGNED DEFAULT NULL,
  `type` tinyint(1) UNSIGNED DEFAULT '0',
  `start_date` date NOT NULL,
  `due_date` date NOT NULL,
  `description` text,
  `terms` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `estimate_products`
--

CREATE TABLE `estimate_products` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `estimate_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL,
  `price` double(10,2) UNSIGNED NOT NULL,
  `tax` double(10,2) UNSIGNED NOT NULL,
  `discount` double(10,2) UNSIGNED DEFAULT NULL,
  `discount_type` tinyint(1) UNSIGNED NOT NULL,
  `discount_value` double(10,2) UNSIGNED NOT NULL,
  `amount` double(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `format_emails`
--

CREATE TABLE `format_emails` (
  `id` int(11) UNSIGNED NOT NULL,
  `client` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `generals`
--

CREATE TABLE `generals` (
  `id` int(11) NOT NULL,
  `version` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `generals`
--

INSERT INTO `generals` (`id`, `version`) VALUES
(1, '1.0');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `width` int(2) UNSIGNED DEFAULT NULL,
  `height` int(2) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `user_id`, `name`, `width`, `height`) VALUES
(1, 1, '1455422400.png', 100, 100);

-- --------------------------------------------------------

--
-- Table structure for table `invitations`
--

CREATE TABLE `invitations` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `client_id` int(11) UNSIGNED NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invitations`
--

INSERT INTO `invitations` (`id`, `user_id`, `client_id`, `status`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1),
(3, 1, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `invitation_settings`
--

CREATE TABLE `invitation_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invitation_settings`
--

INSERT INTO `invitation_settings` (`id`, `title`, `content`) VALUES
(1, 'Web Maintenance', '<p><br />\r\nWe will maintain your website.</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `client_id` int(11) UNSIGNED NOT NULL,
  `status_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `currency_id` int(11) UNSIGNED NOT NULL,
  `number` int(11) UNSIGNED NOT NULL,
  `amount` double(10,2) UNSIGNED NOT NULL,
  `shipping_fee` double(10,2) UNSIGNED NOT NULL,
  `discount` double(10,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `type` tinyint(1) DEFAULT '0',
  `start_date` date NOT NULL,
  `due_date` date NOT NULL,
  `description` text,
  `terms` text,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `user_id`, `client_id`, `status_id`, `currency_id`, `number`, `amount`, `shipping_fee`, `discount`, `type`, `start_date`, `due_date`, `description`, `terms`, `state`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 5, 1, 2, 1117.76, 0.00, 0.00, 0, '2016-03-25', '2016-04-01', '', '', 0, '2016-03-25', '2016-03-25'),
(2, 1, 1, 5, 1, 3, 11200.00, 0.00, 0.00, 0, '2016-06-07', '2016-06-08', '', '', 0, '2016-06-07', '2016-06-07'),
(20, 1, 2, 2, 1, 46, 2000.00, 0.00, 0.00, 0, '2016-06-27', '2016-06-29', '', NULL, 0, '2016-06-27', '2016-06-27'),
(23, 1, 1, 2, 1, 49, 1000.00, 0.00, 0.00, 0, '2016-06-28', '2016-06-30', '', NULL, 0, '2016-06-27', '2016-06-27'),
(24, 1, 4, 2, 1, 50, 10000.00, 0.00, 0.00, 0, '2016-06-29', '2016-06-30', '', NULL, 0, '2016-06-27', '2016-06-27'),
(25, 1, 2, 5, 1, 51, 2000.00, 0.00, 0.00, 0, '2016-06-21', '2016-06-23', '', NULL, 0, '2016-06-27', '2016-06-27'),
(26, 1, 4, 2, 1, 52, 2000.00, 0.00, 0.00, 0, '2016-06-29', '2016-06-30', '', NULL, 0, '2016-06-27', '2016-06-27');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payments`
--

CREATE TABLE `invoice_payments` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `invoice_id` int(11) UNSIGNED NOT NULL,
  `payment_id` int(11) UNSIGNED NOT NULL,
  `payment_date` date NOT NULL,
  `payment_amount` double(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_products`
--

CREATE TABLE `invoice_products` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `invoice_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `quantity` int(11) UNSIGNED NOT NULL,
  `price` double(10,2) UNSIGNED NOT NULL,
  `tax` double(10,2) UNSIGNED NOT NULL,
  `shipping_fee` double(10,2) UNSIGNED NOT NULL,
  `discount` double(10,2) UNSIGNED DEFAULT NULL,
  `discount_type` tinyint(1) UNSIGNED NOT NULL,
  `discount_value` double(10,2) UNSIGNED NOT NULL,
  `amount` double(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_products`
--

INSERT INTO `invoice_products` (`id`, `user_id`, `invoice_id`, `product_id`, `quantity`, `price`, `tax`, `shipping_fee`, `discount`, `discount_type`, `discount_value`, `amount`) VALUES
(1, 1, 1, 2, 2, 499.00, 12.00, 0.00, 0.00, 1, 0.00, 1117.76),
(2, 1, 2, 1, 2, 5000.00, 12.00, 0.00, 0.00, 0, 0.00, 11200.00),
(22, 1, 20, 3, 1, 2000.00, 0.00, 0.00, NULL, 0, 2000.00, 2000.00),
(25, 1, 23, 3, 1, 1000.00, 0.00, 0.00, NULL, 0, 1000.00, 1000.00),
(26, 1, 24, 3, 10, 1000.00, 0.00, 0.00, NULL, 0, 10000.00, 10000.00),
(27, 1, 25, 3, 1, 2000.00, 0.00, 0.00, NULL, 0, 2000.00, 2000.00),
(28, 1, 26, 3, 2, 1000.00, 0.00, 0.00, NULL, 0, 2000.00, 2000.00);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_settings`
--

CREATE TABLE `invoice_settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `number` int(11) UNSIGNED DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_settings`
--

INSERT INTO `invoice_settings` (`id`, `user_id`, `number`, `code`, `text`) VALUES
(1, 1, 52, '16-01', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_statuses`
--

CREATE TABLE `invoice_statuses` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `invoice_statuses`
--

INSERT INTO `invoice_statuses` (`id`, `user_id`, `name`) VALUES
(1, 1, 'paid'),
(2, 1, 'unpaid'),
(3, 1, 'partially paid'),
(4, 1, 'cancelled'),
(5, 1, 'overdue');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `short` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `short`) VALUES
(1, 'English', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `language_texts`
--

CREATE TABLE `language_texts` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` text,
  `content` text,
  `type` tinyint(1) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `from_id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) UNSIGNED DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `user_id`, `from_id`, `parent_id`, `title`, `content`, `status`, `state`, `updated_at`, `created_at`) VALUES
(1, 2, 1, NULL, 'test', '<p>test</p>\r\n', 0, 0, '2016-03-25', '2016-03-25');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `user_id`, `name`) VALUES
(1, 1, '100');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` text NOT NULL,
  `quantity` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `price` double(10,2) UNSIGNED NOT NULL,
  `retail` double(10,2) UNSIGNED NOT NULL,
  `reseller` double(10,2) UNSIGNED NOT NULL,
  `wholesale` double(10,2) UNSIGNED NOT NULL,
  `bulk` double(10,2) UNSIGNED NOT NULL,
  `factory` double(10,2) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `user_id`, `name`, `quantity`, `code`, `price`, `retail`, `reseller`, `wholesale`, `bulk`, `factory`, `description`, `status`, `updated_at`, `created_at`) VALUES
(1, 1, 'Video Editing Package', 70, 've-01', 5000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 'Video Translation 1 – 5 Minutes Length\n- Transcribe Video to Italian\n- Translate Italian to Native English\n- Voice Over English\nVideo Editing, Sync, Vocal Removal', 1, '2016-06-27', '2016-02-14'),
(2, 1, 'Kelboy Super Supreme Pizza', 3, 'KSSP', 499.00, 0.00, 0.00, 0.00, 0.00, 0.00, 'Kelboy Super Supreme Pizza has all meat, seafood and vegetable toppings with 2 thick layers of cheese.', 1, '2016-06-27', '2016-03-25'),
(3, 1, 'Laptop', -16, '1v23', 100000.00, 1000.00, 2000.00, 3000.00, 30000.00, 50000.00, 'Transformer', 1, '2016-06-27', '2016-06-23'),
(4, 1, 'Truck', 20, '1ggl2', 15000.00, 0.00, 0.00, 0.00, 0.00, 0.00, 'Fastest truck on earth!', 0, '2016-06-23', '2016-06-23'),
(5, 1, 'House ', 5, '', 0.00, 10000.00, 8000.00, 25000.00, 250000.00, 500000.00, 'Castlevania', 1, '2016-06-27', '2016-06-23');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) UNSIGNED NOT NULL,
  `invoice_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `start_date` int(1) UNSIGNED NOT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `invoice_id`, `user_id`, `start_date`, `updated_at`, `created_at`) VALUES
(1, 1, 1, 1, '2016-03-25', '2016-03-25');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `currency_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_account` varchar(255) DEFAULT NULL,
  `description` text,
  `receive_emails` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `currency_id`, `name`, `country`, `state`, `city`, `zip`, `address`, `contact`, `phone`, `email`, `website`, `bank`, `bank_account`, `description`, `receive_emails`) VALUES
(1, 1, 'TAKTIKOS Company', 'Philippines', 'NCR', 'Valenzuela', '1440', '72 General Tibo Street Marulas Valenzuela City', '639499902450', '4458363', 'sales@taktikosph.com', 'http://taktikosph.com/', '', '', 'Web and Mobile Development Services', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `value` double(10,2) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `user_id`, `value`) VALUES
(1, 1, 12.00);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `role_id` int(11) UNSIGNED NOT NULL,
  `parent_id` int(11) UNSIGNED NOT NULL,
  `language_id` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(64) NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `remember_token` varchar(100) DEFAULT NULL,
  `updated_at` date NOT NULL,
  `created_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `parent_id`, `language_id`, `name`, `email`, `password`, `status`, `remember_token`, `updated_at`, `created_at`) VALUES
(1, 1, 0, 1, 'Admin', 'reyvillamar@gmail.com', '$2y$10$gjKQrUHuLRhQVPxD0BkJB.vxaUiHcXptuQjxJXK6eMoeU9XOlRNBC', 1, '4e9VaiTXhk8a18vfLUczvAPZEcmD9m0O7Hx4stayLSgf0DEMsoakloXMnAcu', '2016-05-11', '2014-11-20'),
(2, 5, 1, 1, '09054879047', 'john@testemail.com', '$2y$10$2ZCb.6kwXfxmvITmiqFUiumRRZlzQw8d2IUN6fTQGOLxN5IvzzT8u', 1, NULL, '2016-03-25', '2016-03-25'),
(3, 5, 1, 1, '09091234567', 'vhin@yahoo.com', '$2y$10$zd/yYKetJDFBI51Pb3ZAIe46PNxELTrkUoxWjDlOj9k8GfJjd5r4W', 1, NULL, '2016-06-23', '2016-06-23'),
(4, 5, 1, 1, '0906123456', 'greed@yahoo.com', '$2y$10$f8q6MZATtxWDGleDBW7mROTkudCvdtTCRJe/UknAdyuuAGyLpSVbS', 1, NULL, '2016-06-23', '2016-06-23'),
(5, 5, 1, 1, '09051234567', 'envy@yahoo.com', '$2y$10$q7Ey78jzEx9zPnO.nJoItuvkKNJbplMNIGELO5SPMSy8sNYoRVNtO', 1, NULL, '2016-06-23', '2016-06-23'),
(6, 5, 1, 1, '09041234567', 'lust@yahoo.com', '$2y$10$Q77xY/9cm4a/LJJlN.roLOWhS6MAF8aOi3bPVymQwC/yzhXcem/Ne', 1, NULL, '2016-06-23', '2016-06-23'),
(7, 5, 1, 1, '09031234567', 'pride@yahoo.com', '$2y$10$A21oifwVQkfFOn55iDVKG.tnB/jqLJnnewNvcfX4Eqi1lcYhe2aFi', 1, NULL, '2016-06-23', '2016-06-23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alerts`
--
ALTER TABLE `alerts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimates`
--
ALTER TABLE `estimates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estimate_products`
--
ALTER TABLE `estimate_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `format_emails`
--
ALTER TABLE `format_emails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `generals`
--
ALTER TABLE `generals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitations`
--
ALTER TABLE `invitations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invitation_settings`
--
ALTER TABLE `invitation_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_products`
--
ALTER TABLE `invoice_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_settings`
--
ALTER TABLE `invoice_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_statuses`
--
ALTER TABLE `invoice_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language_texts`
--
ALTER TABLE `language_texts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerts`
--
ALTER TABLE `alerts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `estimates`
--
ALTER TABLE `estimates`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `estimate_products`
--
ALTER TABLE `estimate_products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `format_emails`
--
ALTER TABLE `format_emails`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `generals`
--
ALTER TABLE `generals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invitations`
--
ALTER TABLE `invitations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `invitation_settings`
--
ALTER TABLE `invitation_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice_products`
--
ALTER TABLE `invoice_products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `invoice_settings`
--
ALTER TABLE `invoice_settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `invoice_statuses`
--
ALTER TABLE `invoice_statuses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `language_texts`
--
ALTER TABLE `language_texts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
