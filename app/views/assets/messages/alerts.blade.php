<div class="col-md-12">
	@if(Session::has('error'))
		<div role="alert" class="alert alert-danger top20 solsoHideAlert">
			<strong>{{ trans('translate.message') }}: </strong> {{ Session::get('error') }} !
		</div>		
	@endif		

	@if(Session::has('warning'))
		<div role="alert" class="alert alert-warning top20 solsoHideAlert">
			<strong>{{ trans('translate.message') }}: </strong> {{ Session::get('warning') }} !
		</div>		
	@endif	

	@if(Session::has('message'))
		<div role="alert" class="alert alert-success top20 solsoHideAlert">
			<strong>{{ trans('translate.message') }}: </strong> {{ Session::get('message') }} !
		</div>		
	@endif		

	{{ Session::forget('message') }}
</div>