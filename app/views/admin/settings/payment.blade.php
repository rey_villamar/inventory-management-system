<div class="col-md-12 col-lg-8">
	<h3>{{ trans('translate.payments') }}</h3>

	<div class="row">
		<div class="col-md-6">		
		{{ Form::open(array('url' => 'payment', 'role' => 'form', 'class' => 'solsoForm')) }}

			<label for="value">{{ trans('translate.new_value') }}</label>
			<div class="input-group">
				<input type="text" name="value" class="form-control required" autocomplete="off" value="{{ Input::old('value') }}" data-parsley-errors-container=".groupPayment">

				<span class="input-group-btn">
					<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					<button type="submit" class="btn btn-success solsoAjax" 
						data-href="{{ URL::to('payment') }}" data-form="payment" data-method="post" data-return="tab6" 
						data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" 
						data-message-success="{{ trans('translate.data_was_saved') }}" data-message-warning="{{ trans('translate.value_already_exist') }}">
						<i class="fa fa-save"></i> {{ trans('translate.save') }}
					</button>
				</span>	
			</div>
			
			<div class="groupPayment"></div>
			<?php echo $errors->first('value', '<p class="error">:messages</p>');?>
			
		{{ Form::close() }}
		</div>
	</div>
	<div class="clearfix"></div>		

	<div class="table-responsive">
		<table class="table top20" data-alert="{{ isset($alert) ? $alert : false }}">
			<thead>
				<tr>
					<th>{{ trans('translate.crt') }}.</th>
					<th>{{ trans('translate.payments') }}</th>
					<th class="col-md-4 col-4">{{ trans('translate.action') }}</th>
					<th class="small">{{ trans('translate.action') }}</th>
				</tr>
			</thead>
			
			<tbody>
			
				@foreach ($payments as $crt => $v)
			
				<tr>
					<td> 
						{{ $crt + 1 }} 
					</td>
					
					<td> 
						{{ $v->name }}
					</td>
					
					<td>
						<form>
							<div class="input-group">
								<input type="text" name="payment" class="form-control required" autocomplete="off" value="{{ $v->name }}" data-parsley-errors-container=".paymentError{{ $crt }}">
								
								<span class="input-group-btn">
									<button type="submit" class="btn btn-success solsoAjax" 
										data-href="{{ URL::to('payment/' . $v->id) }}" data-method="put" data-return="tab6"
										data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" 
										data-message-success="{{ trans('translate.data_was_saved') }}" data-message-warning="{{ trans('translate.value_already_exist') }}">
										<i class="fa fa-save"></i> {{ trans('translate.update') }}
									</button>
								</span>	
							</div>
							
							<div class="paymentError{{ $crt }}"></div>
						</form>
					</td>
					
					<td>
						<a class="btn btn-danger solsoConfirm" data-toggle="modal" data-target="#solsoDeleteModal" 
						data-href="{{ URL::to('payment/' . $v->id) }}" data-return="tab6">
							<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
						</a>						
					</td>
				</tr>
				
				@endforeach
				
			</tbody>
			
			@if ( sizeof($payments) == 0 )
			
			<tfoot>
				<tr>
					<td colspan="4">
						{{ trans('translate.no_data_available') }}
					</td>
				</tr>
			</tfoot>

			@endif	
		</table>
	</div>
</div>