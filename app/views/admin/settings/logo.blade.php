<div class="col-md-6 col-lg-4">		
	<h3>{{ trans('translate.logo_size') }}</h3>
	
	{{ Form::open(array('role' => 'form')) }}
	
		<div class="form-group">
			<input type="text" name="width" class="form-control required" placeholder="width" autocomplete="off" value="{{ isset($logo->width) ? $logo->width : '' }}">
			<?php echo $errors->first('width', '<p class="error">:messages</p>');?>	
		</div>

		<div class="form-group">
			<input type="text" name="height" class="form-control required" placeholder="height" autocomplete="off" value="{{ isset($logo->height) ? $logo->height : '' }}">
			<?php echo $errors->first('height', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
			<button type="submit" class="btn btn-success solsoAjax" 
				data-href="{{ URL::to('upload/image-size') }}" data-method="post" data-return="tab2" 
				data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" 
				data-message-success="{{ trans('translate.data_was_saved') }}" data-message-warning="">				
				
				<i class="fa fa-save"></i> {{ trans('translate.save') }}
			</button>
		</div>	
	
	{{ Form::close() }}
</div>	
<div class="clearfix"></div>

<div class="col-md-6 col-lg-4">	
	<h3>{{ trans('translate.logo') }}</h3>

	@if ( isset($logo->name) )
		<img src="{{ URL::to('public/upload/' . $logo->name) }}" class="img-responsive thumbnail" width="{{ $logo->width }}" height="{{ $logo->height }}">
	@else
		<div role="alert" class="alert alert-warning top20">
			<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.message_logo') }}
		</div>			
	@endif	
	
	{{ Form::open(array('url' => URL::to('upload/upload-image'), 'role' => 'form', 'class' => 'solsoUploadForm', 'files' => 'true')) }}
	
		<div class="form-group">
			<label for="image">
				{{ trans('translate.upload_logo') }} => <span class="lowercase">{{ trans('translate.allowed_file_extensions') }}: 'jpg', 'jpeg', 'gif', 'png', 'bmp'</span>
			</label>
			<input type="file" name="image" class="solsoFileInput required" autocomplete="off">
		</div>

		<?php echo $errors->first('image', '<p class="error">:messages</p>');?>
		
	{{ Form::close() }}

</div>