<div class="col-md-12 col-lg-8">
	<h3>{{ trans('translate.basic_format_for_emails') }}</h3>
	
	{{ Form::open(array('role' => 'form', 'class' => 'top40')) }}
	
		<div class="form-group">
			<h4>{{ trans('translate.format_email_new_client') }}</h4>
			<table class="table">	
				<tr>
					<td colspan="2">{{ trans('translate.format_email_shortcode') }}</td>
				</tr>
				
				<tr>
					<td class="col-md-3">[client_name]</td> 		
					<td>= {{ trans('translate.explain_shortcode_client_name') }}</td>
				</tr>				
				
				<tr>
					<td>[client_email]</td> 		
					<td>= {{ trans('translate.explain_shortcode_client_email') }}</td>
				</tr>	

				<tr>
					<td>[client_password]</td> 		
					<td>= {{ trans('translate.explain_shortcode_client_password') }}</td>
				</tr>
				
				<tr class="label-overdue">
					<td colspan="2">{{ trans('translate.shortcode_mandatory') }}</td>
				</tr>				
			</table>
			<textarea name="client" class="form-control required solsoEditor" rows="7" autocomplete="off">{{ isset($emails->client) ? $emails->client : (isset($inputs['client']) ? $inputs['client'] : '') }}</textarea>
			
			<?php echo $errors->first('client', '<p class="error">:messages</p>');?>
		</div>	
		
		<div class="form-group">
			<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					
			@if (isset($emails->client))
				<button type="submit" class="btn btn-success solsoAjax" 
					data-href="{{ URL::to('format-email/' . $emails->id) }}" data-method="put" data-return="tabEmailsFormat" 
					data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
					<i class="fa fa-save"></i> {{ trans('translate.save') }}
				</button>
			@else	
				<button type="submit" class="btn btn-success solsoAjax" 
					data-href="{{ URL::to('format-email') }}" data-method="post" data-return="tabEmailsFormat" 
					data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_saved') }}">
					<i class="fa fa-save"></i> {{ trans('translate.save') }}
				</button>
			@endif			
			
		</div>			
		
	{{ Form::close() }}
</div>