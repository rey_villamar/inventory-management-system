<div class="col-md-12 col-lg-8">
	<h3>{{ trans('translate.tax_rate') }}</h3>
	<div class="row">
		<div class="col-md-6">		
		{{ Form::open(array('id' => 'tax', 'url' => 'tax', 'role' => 'form', 'class' => 'solsoForm')) }}

			<label for="value">{{ trans('translate.new_value') }}</label>
			<div class="input-group">
				<input type="text" name="value" class="form-control required" autocomplete="off" value="{{ Input::old('value') }}" data-parsley-errors-container=".createTax">

				<span class="input-group-btn">
					<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					<button type="submit" class="btn btn-success solsoAjax" 
						data-href="{{ URL::to('tax') }}" data-form="tax" data-method="post" data-return="tab4" 
						data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" 
						data-message-success="{{ trans('translate.data_was_saved') }}" data-message-warning="{{ trans('translate.value_already_exist') }}">
						<i class="fa fa-save"></i> {{ trans('translate.save') }}
					</button>
				</span>	
			</div>
			
			<div class="createTax"></div>
			<?php echo $errors->first('value', '<p class="error">:messages</p>');?>
			
		{{ Form::close() }}
		</div>
	</div>
	<div class="clearfix"></div>		

	<div class="table-responsive">
		<table class="table top20" data-alert="{{ isset($alert) ? $alert : false }}">
			<thead>
				<tr>
					<th>{{ trans('translate.crt') }}.</th>
					<th>{{ trans('translate.values') }}</th>
					<th class="col-md-4 col-4">{{ trans('translate.action') }}</th>
					<th class="small">{{ trans('translate.action') }}</th>
				</tr>
			</thead>
			
			<tbody>
				@foreach ($taxes as $crt => $v)
			
				<tr>
					<td> 
						{{ $crt + 1 }} 
					</td>
					
					<td> 
						{{ $v->value }} %
					</td>
					
					<td>
						<form>
							<div class="input-group">
								<input type="text" name="tax" class="form-control required" autocomplete="off" value="{{ $v->value }}" data-parsley-errors-container=".taxError{{ $crt }}">
								
								<span class="input-group-btn">
									<button type="submit" class="btn btn-success solsoAjax" 
										data-href="{{ URL::to('tax/' . $v->id) }}" data-form="updateTax" data-method="put" data-return="tab4" 
										data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" 
										data-message-success="{{ trans('translate.data_was_saved') }}" data-message-warning="{{ trans('translate.value_already_exist') }}">
										<i class="fa fa-save"></i> {{ trans('translate.update') }}
									</button>
								</span>	
							</div>
							
							<div class="taxError{{ $crt }}"></div>
						</form>
					</td>
					
					<td>
						<a class="btn btn-danger solsoConfirm" data-toggle="modal" data-target="#solsoDeleteModal" 
						data-href="{{ URL::to('tax/' . $v->id) }}" data-return="tab4">
							<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
						</a>
					</td>
				</tr>
				
				@endforeach
				
			</tbody>
			
			@if ( sizeof($taxes) == 0 )
			
			<tfoot>
				<tr>
					<td colspan="4">
						{{ trans('translate.no_data_available') }}
					</td>
				</tr>
			</tfoot>

			@endif	
		</table>
	</div>
</div>