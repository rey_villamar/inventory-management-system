<div class="col-md-12 col-lg-8">
	<h3>{{ trans('translate.alerts') }}</h3>
	<div class="row">
		<div class="col-md-6">		
		{{ Form::open(array('url' => 'alert/products', 'role' => 'form', 'class' => 'solsoForm')) }}

			<label for="products"> {{ trans('translate.alert_text_product') }}</label>
			<div class="input-group">
				<input type="text" name="products" class="form-control required" autocomplete="off" value="{{ isset($alerts->products) ? $alerts->products : '' }}" data-parsley-errors-container=".alertProduct">

				<span class="input-group-btn">
					<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					<button type="submit" class="btn btn-success solsoAjax" 
						data-href="{{ URL::to('alert/products') }}" data-method="post" data-return="tabAlert" 
						data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
						<i class="fa fa-save"></i> {{ trans('translate.save') }}
					</button>	
				</span>	
			</div>
			
			<div class="alertProduct"></div>
			<?php echo $errors->first('products', '<p class="error">:messages</p>');?>
			
		{{ Form::close() }}
		</div>
	</div>
</div>