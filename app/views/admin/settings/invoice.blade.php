<div class="col-md-12">
	<h3>{{ trans('translate.invoice_start_number') }}</h3>
	<div class="row">
		<div class="col-md-6 col-lg-3">		
		{{ Form::open(array('id' => 'invoiceNumber', 'url' => 'invoice/number', 'role' => 'form', 'class' => 'solsoForm')) }}

			<label for="number"> {{ trans('translate.new_value') }}</label>
			<div class="input-group">
				<input type="text" name="number" class="form-control required" autocomplete="off" value="{{ isset($invoiceSettings->number) ? $invoiceSettings->number : '' }}" data-parsley-errors-container=".createNumber">

				<span class="input-group-btn">
					<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					<button type="submit" class="btn btn-success solsoAjax" 
						data-href="{{ URL::to('invoice/number') }}" data-form="invoiceNumber" data-method="post" data-return="tab3" 
						data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
						<i class="fa fa-save"></i> {{ trans('translate.save') }}
					</button>	
				</span>	
			</div>
			
			<div class="createNumber"></div>
			<?php echo $errors->first('number', '<p class="error">:messages</p>');?>
			
		{{ Form::close() }}
		</div>
	</div>

	<h3 class="top20">{{ trans('translate.invoice_code') }}</h3>
	<div class="row">
		<div class="col-md-6 col-lg-3">		
		{{ Form::open(array('id' => 'invoiceCode', 'url' => 'invoice/code', 'role' => 'form', 'class' => 'solsoForm')) }}

			<label for="code">{{ trans('translate.new_value') }}</label>
			<div class="input-group">
				<input type="text" name="code" class="form-control required" autocomplete="off" value="{{ isset($invoiceSettings->code) ? $invoiceSettings->code : '' }}" data-parsley-errors-container=".createCode">

				<span class="input-group-btn">
					<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					<button type="submit" class="btn btn-success solsoAjax" 
						data-href="{{ URL::to('invoice/code') }}" data-form="invoiceCode" data-method="post" data-return="tab3" 
						data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
						<i class="fa fa-save"></i> {{ trans('translate.save') }}
					</button>	
				</span>	
			</div>
			
			<div class="createCode"></div>
			<?php echo $errors->first('code', '<p class="error">:messages</p>');?>
			
		{{ Form::close() }}
		</div>
	</div>	
</div>	

<div class="col-md-12 col-lg-8">	
	<h3>{{ trans('translate.invoice_personal_title') }}</h3>
	{{ Form::open(array('id' => 'invoiceText', 'url' => 'invoice/text', 'role' => 'form', 'class' => 'solsoForm')) }}

		<div class="form-group">
			<label for="description" class="lowercase">{{ trans('translate.invoice_personal_description') }}</label>
			<textarea name="description" class="form-control required solsoEditor" rows="7" autocomplete="off">{{ isset($invoiceSettings->text) ? $invoiceSettings->text : '' }}</textarea>
		</div>	

		<?php echo $errors->first('description', '<p class="error">:messages</p>');?>
		
		<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
		<button type="submit" class="btn btn-success solsoAjax" 
			data-href="{{ URL::to('invoice/text') }}" data-method="post" data-return="tab3" 
			data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>		
	{{ Form::close() }}
</div>
