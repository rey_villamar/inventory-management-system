<div class="col-md-12 col-lg-8">	
	<h3>{{ trans('translate.currencies') }}</h3>
	
	<div class="row">
		<div class="col-md-6">		
		{{ Form::open(array('id' => 'currency', 'url' => 'currency', 'role' => 'form', 'class' => 'solsoForm')) }}

			<label for="value"> {{ trans('translate.new_value') }}</label>
			<div class="input-group">
				<input type="text" name="value" class="form-control required" autocomplete="off" value="{{ Input::old('value') }}" data-parsley-errors-container=".createCurency">

				<span class="input-group-btn">
					<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
					<button type="submit" class="btn btn-success solsoAjax" 
						data-href="{{ URL::to('currency') }}" data-form="currency" data-method="post" data-return="tab5" 
						data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" 
						data-message-success="{{ trans('translate.data_was_saved') }}" data-message-warning="{{ trans('translate.value_already_exist') }}">
						<i class="fa fa-save"></i> {{ trans('translate.save') }}
					</button>
				</span>	
			</div>
			
			<div class="createCurency"></div>
			<?php echo $errors->first('value', '<p class="error">:messages</p>');?>
			
		{{ Form::close() }}
		</div>
	</div>
	<div class="clearfix"></div>		
	
	<div class="table-responsive">
		<table class="table top20" data-alert="{{ isset($alert) ? $alert : false }}">
			<thead>
				<tr>
					<th>{{ trans('translate.crt') }}.</th>
					<th>{{ trans('translate.values') }}</th>
					<th>{{ trans('translate.price_position') }}</th>
					<th>{{ trans('translate.default_currency') }}</th>
					<th class="col-md-4 col-4">{{ trans('translate.action') }}</th>
					<th class="small">{{ trans('translate.action') }}</th>
				</tr>
			</thead>
			
			<tbody>
				@foreach ($currencies as $crt => $v)
			
				<tr>
					<td> 
						{{ $crt + 1 }} 
					</td>
					
					<td> 
						{{ $v->name }}
					</td>
					
					<td>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default {{ $v->position == 1 ? 'active' : '' }} solsoCurrencySettings" data-id="{{ $v->id }}" 
							data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
								<input type="radio" name="position" autocomplete="off" value="1" {{ $v->position == 1 ? "checked='checked'" : '' }}> 
								<span class="capitalize">{{ trans('translate.left') }}</span>
							</label>
							
							<label class="btn btn-default {{ $v->position == 2 ? 'active' : '' }} solsoCurrencySettings" data-id="{{ $v->id }}"
							data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
								<input type="radio" name="position" autocomplete="off" value="2" {{ $v->position == 2 ? "checked='checked'" : '' }}> 
								<span class="capitalize">{{ trans('translate.right') }}</span>
							</label>
						</div>	
					</td>
					
					<td>
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default {{ $company->currency_id == $v->id  ? 'active' : '' }} solsoCurrencySettings" data-id="{{ $v->id }}"
							data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
								<input type="radio" name="default" autocomplete="off" value="1" {{ $company->currency_id == $v->id ? "checked='checked'" : '' }}> 
								<span class="capitalize">{{ trans('translate.yes') }}</span>
							</label>
							
							<label class="btn btn-default {{ $company->currency_id != $v->id ? 'active' : '' }} solsoCurrencySettings" data-id="{{ $v->id }}"
							data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
								<input type="radio" name="default" autocomplete="off" value="2" {{ $company->currency_id != $v->id ? "checked='checked'" : '' }}> 
								<span class="capitalize">{{ trans('translate.no') }}</span>
							</label>
						</div>	
					</td>					
					
					<td>
						<form>
							<div class="input-group">
								<input type="text" name="currency" class="form-control required" autocomplete="off" value="{{ $v->name }}" data-parsley-errors-container=".currencyError{{ $crt }}">
								
								<span class="input-group-btn">
									<button type="submit" class="btn btn-success solsoAjax" 
										data-href="{{ URL::to('currency/' . $v->id) }}" data-method="put" data-return="tab5"
										data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
										<i class="fa fa-save"></i> {{ trans('translate.update') }}
									</button>
								</span>	
							</div>
							
							<div class="currencyError{{ $crt }}"></div>
						</form>
					</td>
					
					<td>
						<a class="btn btn-danger solsoConfirm" data-toggle="modal" data-target="#solsoDeleteModal" 
						data-href="{{ URL::to('currency/' . $v->id) }}" data-return="tab5">
							<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
						</a>						
					</td>
				</tr>
				
				@endforeach
				
			</tbody>
			
			@if (sizeof($currencies) == 0)
			
			<tfoot>
				<tr>
					<td colspan="4">
						{{ trans('translate.no_data_available') }}
					</td>
				</tr>
			</tfoot>

			@endif	
		</table>
	</div>
</div>