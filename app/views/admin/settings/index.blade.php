@section('content')

	<div class="col-md-12">
		<h1><i class="fa fa-cogs"></i> {{ trans('translate.settings') }}</h1>
	</div>		
	
	<div class="col-md-12">
		<ul id="solsoTabs" class="nav nav-tabs" role="tablist">
			<li class="active"><a href="#tab1" role="tab" data-toggle="tab">{{ trans('translate.company') }}</a></li>
			<li><a href="#tab2" role="tab" data-toggle="tab">{{ trans('translate.logo') }}</a></li>
			<li><a href="#tab3" role="tab" data-toggle="tab">{{ trans('translate.invoice') }}</a></li>
			<li><a href="#tab4" role="tab" data-toggle="tab">{{ trans('translate.tax_rate') }}</a></li>
			<li><a href="#tab5" role="tab" data-toggle="tab">{{ trans('translate.currencies') }}</a></li>
			<li><a href="#tab6" role="tab" data-toggle="tab">{{ trans('translate.payments') }}</a></li>
			<li><a href="#tab7" role="tab" data-toggle="tab">{{ trans('translate.invitation') }}</a></li>
			<li><a href="#tab8" role="tab" data-toggle="tab">{{ trans('translate.languages') }}</a></li>
			<li><a href="#tab9" role="tab" data-toggle="tab">{{ trans('translate.account') }}</a></li>
			<li><a href="#tab10" role="tab" data-toggle="tab">{{ trans('translate.password') }}</a></li>
			<li><a href="#tabEmail" role="tab" data-toggle="tab">{{ trans('translate.email') }}</a></li>
			<li><a href="#tabEmailsFormat" role="tab" data-toggle="tab">{{ trans('translate.basic_format_for_emails') }}</a></li>
			<li><a href="#tabAlerts" role="tab" data-toggle="tab">{{ trans('translate.alerts') }}</a></li>
		</ul>
		
		<div class="row tab-content">
			<div class="tab-pane active" id="tab1">
				@include('admin.settings.company')
			</div>
			
			<div class="tab-pane" id="tab2">
				@include('admin.settings.logo')
			</div>	

			<div class="tab-pane" id="tab3">
				@include('admin.settings.invoice')
			</div>			

			<div class="tab-pane" id="tab4">
				@include('admin.settings.tax')
			</div>			
			
			<div class="tab-pane" id="tab5">
				@include('admin.settings.currency')
			</div>	

			<div class="tab-pane" id="tab6">
				@include('admin.settings.payment')
			</div>					
			
			<div class="tab-pane" id="tab7">
				@include('admin.settings.invitation')
			</div>

			<div class="tab-pane" id="tab8">
				@include('admin.settings.language')
			</div>	

			<div class="tab-pane" id="tab9">
				@include('admin.settings.account')
			</div>	

			<div class="tab-pane" id="tab10">
				@include('admin.settings.password')
			</div>	

			<div class="tab-pane" id="tabEmail">
				@include('admin.settings.email')
			</div>		

			<div class="tab-pane" id="tabEmailsFormat">
				@include('admin.settings.emails-format')
			</div>		

			<div class="tab-pane" id="tabAlerts">
				@include('admin.settings.alerts')
			</div>			
		</div>		
	</div>

	@include('assets.modals.modal-delete')
		
@stop