@include('begin')

<header>
	@include('admin.navbar')
</header>

<main class="body slide">
	<aside class="sidebar show perfectScrollbar">
		@include('admin.sidebar')
	</aside>
	
	<div class="container-fluid solso-content">
	<div class="row">
		@include('assets.messages.alerts')
		
		@yield('content')
		
		@include('assets.modals.modal-crud')
		@include('assets.modals.modal-delete')		
	</div>
	</div>
</main>

@include('end')