@section('content')
	
	<div class="col-md-12">
		<h1>
			<i class="fa fa-home"></i> {{ trans('translate.dashboard') }}
		</h1>
	
		@if (!isset($owner->receive_emails))
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_receive_emails') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.email') }}</a>
			</div>
		@endif
		
		@if (!isset($owner->name))
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_details') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.company') }}</a>
			</div>
		@endif
				
		@if (!isset($logo->name) )		
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.message_logo') }} 
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}" >{{ trans('translate.settings') }} -> {{ trans('translate.logo') }}</a>
			</div>				
		@endif		
				
		@if (!$invitation)
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_invitation') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.invitation') }}</a>
			</div>
		@endif	
		
		@if ($tax == 0)
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_tax') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.tax_rate') }}</a>
			</div>
		@endif		
		
		@if ($currencies == 0)
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_currencies') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.currencies') }}</a>
			</div>
		@endif		
		
		@if ($payments == 0)
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_payments') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.payments') }}</a>
			</div>
		@endif		
		
		@if (!isset($alerts->products))
			<div role="alert" class="alert alert-warning top20">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_alerts') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }} -> {{ trans('translate.alerts') }}</a>
			</div>
		@endif		
	</div>
	
	<div class="col-sm-6 col-md-3">
		<div class="widget widget-stats bg-green">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-users fa-fw"></i></div>
			<div class="stats-title">{{ trans('translate.clients') }}</div>
			<div class="stats-number">{{ $totalClients }}</div>
			<hr>
			<div class="stats-desc">{{ trans('translate.number_of_clients') }}</div>
		</div> 	
	</div> 	

	<div class="col-sm-6 col-md-3">
		<div class="widget widget-stats bg-blue">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-puzzle-piece fa-fw"></i></div>
			<div class="stats-title">{{ trans('translate.products') }}</div>
			<div class="stats-number">{{ $totalProducts }}</div>
			<hr>
			<div class="stats-desc">{{ trans('translate.number_of_products') }}</div>
		</div> 	
	</div>  

	<div class="col-sm-6 col-md-3">
		<div class="widget widget-stats bg-purple">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-file-pdf-o fa-fw"></i></div>
			<div class="stats-title">{{ trans('translate.invoices') }}</div>
			<div class="stats-number">{{ $totalInvoices }}</div>
			<hr>
			<div class="stats-desc">{{ trans('translate.number_of_invoices') }}</div>
		</div> 
	</div> 		

	<div class="col-sm-6 col-md-3">
		<div class="widget widget-stats bg-grey">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-money fa-fw"></i></div>
			<div class="stats-title">{{ trans('translate.amount') }}</div>
			<div class="stats-number">{{ $amount }}</div>
			<hr>
			<div class="stats-desc">{{ trans('translate.total_value_of_amounts') }}</div>
		</div> 
	</div> 		


	<div class="col-sm-6 col-md-4 col-lg-2">
		<div class="solso-pie-chart thumbnail chart-invoice-pending">
			<p>{{ trans('translate.paid') }}: {{ $invoiceChart['paid']['count'] }}</p>
			<p>{{ trans('translate.unpaid') }}: {{ $invoiceChart['unpaid']['count'] }}</p>
			<p>{{ trans('translate.partially_paid') }}: {{ $invoiceChart['partiallypaid']['count'] }}</p>
			<p>{{ trans('translate.overdue') }}: {{ $invoiceChart['overdue']['count'] }}</p>
			<p>{{ trans('translate.cancelled') }}: {{ $invoiceChart['cancelled']['count'] }}</p>
		</div> 
	</div>		

	<div class="col-sm-6 col-md-4 col-lg-2">
		<div class="solso-pie-chart thumbnail chart-invoice-paid">
			<div class="chart" data-percent="{{ $invoiceChart['paid']['percent']}}">
				<span class="percent"></span>
			</div>	
			
			<h4>{{ trans('translate.paid') }}</h4>
		</div>
	</div> 	

	<div class="col-sm-6 col-md-4 col-lg-2">
		<div class="solso-pie-chart thumbnail chart-invoice-unpaid">
			<div class="chart" data-percent="{{ $invoiceChart['unpaid']['percent'] }}">
				<span class="percent"></span>
			</div>	
			
			<h4>{{ trans('translate.unpaid') }}</h4>
		</div>
	</div>  

	<div class="col-sm-6 col-md-4 col-lg-2">
		<div class="solso-pie-chart thumbnail chart-invoice-partial-paid">
			<div class="chart" data-percent="{{ $invoiceChart['partiallypaid']['percent'] }}">
				<span class="percent"></span>
			</div>	
			
			<h4>{{ trans('translate.partially_paid') }}</h4>
		</div>
	</div> 		

	<div class="col-sm-6 col-md-4 col-lg-2">
		<div class="solso-pie-chart thumbnail chart-invoice-overdue">
			<div class="chart" data-percent="{{ $invoiceChart['overdue']['percent'] }}">
				<span class="percent"></span>
			</div>	
			
			<h4>{{ trans('translate.overdue') }}</h4>
		</div>
	</div> 

	<div class="col-sm-6 col-md-4 col-lg-2">
		<div class="solso-pie-chart thumbnail chart-invoice-canceled">
			<div class="chart" data-percent="{{ $invoiceChart['cancelled']['percent']}}">
				<span class="percent"></span>
			</div>	
			
			<h4>{{ trans('translate.cancelled') }}</h4>
		</div>
	</div>
	
	<div class="col-sm-12 col-md-12 col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-tasks"></i> {{ trans('translate.last') }} 5 {{ trans('translate.clients') }}
				</h2>
			</div>

			<ul class="list-group">
				@if (sizeof($lastClients) == 0)
					<li class="list-group-item">
						{{ trans('translate.no_data_available') }}
					</li>
				@else			
					@foreach ($lastClients as $client)
						<li class="list-group-item">
							{{ $client->name }}
						</li>
					@endforeach
				@endif
			</ul>		
		</div>				
	</div> 

	<div class="col-sm-12 col-md-12 col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-puzzle-piece"></i> {{ trans('translate.last') }} 5 {{ trans('translate.products') }}
				</h2>
			</div>

			<ul class="list-group">
				@if (sizeof($lastProducts) == 0)
					<li class="list-group-item">
						{{ trans('translate.no_data_available') }}
					</li>
				@else			
					@foreach ($lastProducts as $product)
						<li class="list-group-item">
							{{ $product->name }}
						</li>
					@endforeach
				@endif
			</ul>		
		</div>				
	</div>	
	
	<div class="col-sm-12 col-md-12 col-lg-4">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-comment"></i> {{ trans('translate.last') }} 5 {{ trans('translate.invoices') }}
				</h2>
			</div>

			<ul class="list-group">
				@if (sizeof($lastInvoices) == 0)
					<li class="list-group-item">
						{{ trans('translate.no_data_available') }}
					</li>
				@else			
					@foreach ($lastInvoices as $invoice)
						<li class="list-group-item">
							{{ $invoice->number }}
							
							<span class="pull-right">
				<span class="label label-{{ str_replace(' ', '-', $invoice->status) }}">
					{{ trans('translate.' . str_replace(' ', '_', $invoice->status)) }}
				</span>							
							</span>
						</li>
					@endforeach
				@endif
			</ul>		
		</div>				
	</div>	
	<div class="clearfix"></div>
	
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-bar-chart"></i> {{ trans('translate.clients_and_products') }} -> {{ trans('translate.this_month') }}
				</h2>
			</div>
			
			<div class="panel-body">
				<canvas id="reportClientsProducts" height="120" class="col-md-12"></canvas>
			</div>
		</div>	
	</div>	

	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-bar-chart"></i> {{ trans('translate.estimates_and_invoices') }} -> {{ trans('translate.this_month') }}
				</h2>
			</div>
			
			<div class="panel-body">
				<canvas id="reporEstimatesInvoices" height="120" class="col-md-12"></canvas>
			</div>
		</div>	
	</div>	
	
@stop	 