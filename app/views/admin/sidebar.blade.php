<div id="solso-sidebar" class="list-group">
	<a href="{{ URL::to('admin') }}" class="list-group-item <?php if ( Request::segment(1) == 'admin' && ! Request::segment(2)) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-home"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.dashboard') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.reports') }}
			</p>
		</div>	  
	</a>
	
	<a href="{{ URL::to('client') }}" class="list-group-item <?php if ( Request::segment(1) == 'client' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-users"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.clients') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>

	<a href="{{ URL::to('product') }}" class="list-group-item <?php if ( Request::segment(1) == 'product' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-puzzle-piece"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.products') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>	

	<!-- <a href="{{ URL::to('estimate') }}" class="list-group-item <?php if ( Request::segment(1) == 'estimate' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-file"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.estimates') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>	 -->

	<a href="{{ URL::to('invoice') }}" class="list-group-item <?php if ( Request::segment(1) == 'invoice' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-file-pdf-o"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.invoices') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>

	<!-- <a href="{{ URL::to('schedule') }}" class="list-group-item <?php if ( Request::segment(1) == 'schedule' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.schedules') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a> -->

	<!-- <a href="{{ URL::to('message') }}" class="list-group-item <?php if ( Request::segment(1) == 'message' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-weixin"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.messages') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>	 -->
	
	<a href="{{ URL::to('report') }}" class="list-group-item <?php if ( Request::segment(1) == 'report' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-line-chart"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.reports') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.view') }}
			</p>					
		</div>	  
	</a>
	
	
<!-- 	<a href="{{ URL::to('language') }}" class="list-group-item <?php if ( Request::segment(1) == 'language' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-book"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.languages') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }} | {{ trans('translate.edit') }} | {{ trans('translate.delete') }}
			</p>
		</div>	  
	</a> -->

	<a href="{{ URL::to('admin/settings') }}" class="list-group-item <?php if ( Request::segment(2) == 'settings' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-cogs"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.settings') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }} | {{ trans('translate.edit') }} | {{ trans('translate.delete') }}
			</p>
		</div>	  
	</a>	
</div>
