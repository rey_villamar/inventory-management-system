@section('content')

<div class="container top40">
<div class="row thumbnail">
	<div class="col-md-12">
		<h3>{{ $ticket->title }}</h3>
		
		{{ $ticket->content }}
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<a href="{{ URL::to('login') }}" class="btn solso-pdf btn-block"> {{ trans('translate.log_in') }}</a>
	</div>	
</div>
</div>

@stop