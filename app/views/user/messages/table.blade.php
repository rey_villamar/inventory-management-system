<h3>{{ trans('translate.received_messages') }}</h3>
<div class="table-responsive">
	<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}" data-all="{{ sizeof($received) }}">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="col-md-2">{{ trans('translate.from') }}</th>
				<th class="col-md-3">{{ trans('translate.title') }}</th>
				<th>{{ trans('translate.content') }}</th>
				<th class="col-md-1 text-center">{{ trans('translate.date') }}</th>
				<th class="small">{{ trans('translate.status') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
			</tr>
		</thead>
		
		<tbody>
		
		@foreach ($received as $crt => $v)
		
			<tr>
				<td>
					{{ $crt+1 }}
				</td>

				<td>
					{{ $v->name }}
				</td>
				
				<td>
					{{ $v->title }}
				</td>	

				<td>
					{{ strip_tags(substr($v->content, 0, 100)) }}
				</td>							
				
				<td class="text-center">
					{{ $v->created_at }}
				</td>				

				<td>
					@if ($v->status == 1)
						<label class="label label-paid">{{ trans('translate.read') }}</label>
					@else
						<label class="label label-overdue">{{ trans('translate.unread') }}</label>
					@endif
				</td>					

				<td>
					<button type="button" class="btn btn-info solsoShowModal" 
					data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('message/' . $v->id) }}" data-modal-title="{{ trans('translate.show_message') }}">
						<i class="fa fa-eye"></i> {{ trans('translate.show') }}
					</button>
				</td>		

				<td>		
					<button type="button" class="btn btn-danger solsoConfirm" 
					data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('message/' . $v->id) }}">
						<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
					</button>		
				</td>
			</tr>
			
		@endforeach
		
		</tbody>
	</table>	
</div>	



<h3>{{ trans('translate.sent_messages') }}</h3>
<div class="table-responsive">
	<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}" data-all="{{ sizeof($received) }}">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="col-md-2">{{ trans('translate.sender') }}</th>
				<th class="col-md-3">{{ trans('translate.title') }}</th>
				<th>{{ trans('translate.content') }}</th>
				<th class="col-md-1 text-center">{{ trans('translate.date') }}</th>
				<th class="small">{{ trans('translate.status') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
			</tr>
		</thead>
		
		<tbody>
		
		@foreach ($sent as $crt => $v)
		
			<tr>
				<td>
					{{ $crt+1 }}
				</td>

				<td>
					{{ $v->name }}
				</td>
				
				<td>
					{{ $v->title }}
				</td>	

				<td>
					{{ strip_tags(substr($v->content, 0, 100)) }}
				</td>							
				
				<td class="text-center">
					{{ $v->created_at }}
				</td>				

				<td>
					@if ($v->status == 1)
						<label class="label label-paid">{{ trans('translate.read') }}</label>
					@else
						<label class="label label-overdue">{{ trans('translate.unread') }}</label>
					@endif
				</td>					
				
				<td>
					<button type="button" class="btn btn-info solsoShowModal" 
					data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('message/' . $v->id . '/sent') }}" data-modal-title="{{ trans('translate.show_message') }}">
						<i class="fa fa-eye"></i> {{ trans('translate.show') }}
					</button>
				</td>				

				<td>		
					<button type="button" class="btn btn-danger solsoConfirm" 
					data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('message/' . $v->id) }}">
						<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
					</button>		
				</td>
			</tr>
			
		@endforeach
		
		</tbody>
	</table>
</div>