{{ Form::open(array('url' => 'message/' . Request::segment(2), 'role' => 'form', 'method' => 'PUT', 'class' => 'solsoForm')) }}

	<div class="form-group">
		<label for="title"> {{ trans('translate.title') }} </label>
		<input type="text" name="title" class="form-control required" autocomplete="off" value="{{ trans('translate.reply_to') }}: {{ $message->title }}">

		<?php echo $errors->first('title', '<p class="error">:messages</p>');?>
	</div>		
	
	<div class="form-group">
		<label for="content"> {{ trans('translate.message') }} </label>
		<textarea name="content" class="form-control solsoEditor" rows="7" autocomplete="off"></textarea>
		
		<?php echo $errors->first('content', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group">
		<input type="hidden" name="type" value="reply">
		<input type="hidden" name="client" value="{{ $message->from_id }}">
		<button type="submit" class="btn btn-success">
			<i class="fa fa-share"></i> {{ trans('translate.reply') }}
		</button>	
	</div>
	
{{ Form::close() }}


