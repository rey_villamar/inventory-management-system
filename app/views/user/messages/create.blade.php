{{ Form::open(array('url' => 'message', 'role' => 'form', 'class' => 'solsoForm')) }}
	
	<div class="form-group col-md-4">
		<label for="user">{{ trans('translate.user') }}</label>
		<select name="user" class="form-control required select2">
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($users as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('user', '<p class="error">:messages</p>');?>
	</div>
	
	<div class="form-group col-md-8">
		<label for="title">{{ trans('translate.title') }}</label>
		<input type="text" name="title" class="form-control required" autocomplete="off" value="{{ isset($inputs['title']) ? $inputs['title'] : '' }}">

		<?php echo $errors->first('title', '<p class="error">:messages</p>');?>
	</div>		
	
	<div class="form-group col-md-12">
		<label for="content"> {{ trans('translate.message') }} </label>
		<textarea name="content" class="form-control required solsoEditor" rows="7" autocomplete="off">{{ isset($inputs['content']) ? $inputs['content'] : '' }}</textarea>
		
		<?php echo $errors->first('content', '<p class="error">:messages</p>');?>
	</div>	

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.message_was_sent') }}">
			<i class="fa fa-share"></i> {{ trans('translate.send') }}
		</button>
	</div>
	
{{ Form::close() }}

