<div class="col-md-12">
	<h2>
		{{ $message->title }}
	</h2>
	<hr>
	
	<div class="table-responsive">
		<table class="table table-striped">
			<tbody>
				<tr>
					<td>
						{{ $message->content }}
					</td>
				</tr>
			</tbody>
		</table>
	
		@if ($histories)
			<div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
				<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="heading1">
						<h4 class="panel-title">
							{{ trans('translate.histories') }}
							<a data-toggle="collapse" data-parent="#accordion2" href="#collapse1" aria-expanded="true" aria-controls="collapse1" class="pull-right">
								<i class="solsoCollapseIcon fa fa-chevron-down"></i>	
							</a>
						</h4>
					</div>
							
					<div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
						<div class="panel-body">						
							<table class="table table-striped">
								<tbody>				
									@foreach ($histories as $id => $history)
										<tr>
											<td>
												<h4>{{ $history->title }}</h4>
												
												{{ $history->content }}
											</td>
										</tr>					
									@endforeach
								</tbody>
							</table>
						</div>
					</div>					
				</div>					
			</div>		
		@endif
	</div>		
	
	@if (!$owner)
		<h3>{{ trans('translate.reply_to_message') }} </h3>	
		
		<?php
			$messageID = $message->parent_id ? $message->parent_id : $message->id;
		?>
		
		{{ Form::open(array('url' => 'message/' . $messageID . '/reply', 'role' => 'form', 'method' => 'POST', 'class' => 'solsoForm')) }}
		
			<div class="form-group">
				<label for="title"> {{ trans('translate.title') }} </label>
				<input type="text" name="title" class="form-control required" autocomplete="off" value="{{ trans('translate.reply_to') }}: {{ $message->title }}">

				<?php echo $errors->first('title', '<p class="error">:messages</p>');?>
			</div>		
			
			<div class="form-group">
				<label for="content"> {{ trans('translate.message') }} </label>
				<textarea name="content" class="form-control required solsoEditor" rows="7" autocomplete="off"></textarea>
				
				<?php echo $errors->first('content', '<p class="error">:messages</p>');?>
			</div>	
			
			<div class="form-group">
				<input type="hidden" name="userID" value="{{ $message->from_id }}">
				<button type="button" class="btn btn-success solsoSave" 
				data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
					<i class="fa fa-share"></i> {{ trans('translate.send') }}
				</button>
			</div>
			
		{{ Form::close() }}
	@endif
</div>				