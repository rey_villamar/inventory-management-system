@section('content')

	<div class="col-md-12 ">
		<h1>
			<i class="fa fa-list"></i> {{ trans('translate.messages') }}
		</h1>
		
		<button type="button" class="btn btn-primary solsoShowModal"
		data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('message/create') }}" data-modal-title="{{ trans('translate.create_new_message') }}">
			<i class="fa fa-plus"></i> {{ trans('translate.create_new_message') }}
		</button>
	</div>	

	<div class="col-md-12 top40">
		<div id="ajaxTable" class="table-responsive">
			@include('user.messages.table')	
		</div>	
	</div>
	
@stop