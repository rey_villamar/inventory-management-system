{{ Form::open(array('url' => 'product/' . $product->id . '/quantity', 'role' => 'form', 'method' => 'POST', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}

	<div class="form-group col-md-4">
		<label for="quantity">{{ trans('translate.quantity') }}</label>
		<input type="text" name="quantity" class="form-control" autocomplete="off" value="{{ $product->quantity }}">

		<?php echo $errors->first('quantity', '<p class="error">:messages</p>');?>
	</div>

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}