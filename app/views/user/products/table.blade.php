<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}">
	<thead>
		<tr>
			<th>{{ trans('translate.crt') }}.</th>
			<th class="col-md-3">{{ trans('translate.product') }}</th>
			<!-- <th class="col-md-1">{{ trans('translate.code') }}</th> -->
			<th class="col-md-1 text-center">{{ trans('translate.quantity') }}</th>
			<th class="small">RETAIL</th>
			<th class="small">RESELL</th>
			<th class="small">WHOLESALE</th>
			<th class="small">BULK</th>
			<th class="small">FACTORY</th>
			<th class="small">{{ trans('translate.action') }}</th>
			<!-- <th class="small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th> -->
		</tr>
	</thead>

	<tbody>

	@foreach ($products as $crt => $v)

		<tr>
			<td>
				{{ $crt+1 }}
			</td>

			<td>
				{{ $v->name }}
			</td>			

			<!-- <td>
				{{ $v->code }}
			</td> -->
			<td class="text-center">
				@if ( $v->quantity < 1)
					<label class="label label-overdue">{{ $v->quantity }}</label>
				@else
					<label class="label label-paid">{{ $v->quantity }}</label>
				@endif			
			</td>						
			<td>
				{{ $currency->position == 1 ? $currency->name : '' }} {{ $v->retail }} {{ $currency->position == 2 ? $currency->name : '' }} 
			</td>
			<td>
				{{ $currency->position == 1 ? $currency->name : '' }} {{ $v->reseller }} {{ $currency->position == 2 ? $currency->name : '' }} 
			</td>
			<td>
				{{ $currency->position == 1 ? $currency->name : '' }} {{ $v->wholesale }} {{ $currency->position == 2 ? $currency->name : '' }} 
			</td>
			<td>
				{{ $currency->position == 1 ? $currency->name : '' }} {{ $v->bulk }} {{ $currency->position == 2 ? $currency->name : '' }} 
			</td>
			<td>
				{{ $currency->position == 1 ? $currency->name : '' }} {{ $v->factory }} {{ $currency->position == 2 ? $currency->name : '' }} 
			</td>



			<td>
				<div class="dropdown">
					<button class="btn solso-pdf dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						{{ trans('translate.quick_actions') }}
						<span class="caret"></span>
					</button>
				
					<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation">
							<button style="width:100%; margin-bottom:2px;"  role="menuitem" tabindex="-1" href="#" class="btn btn-success solsoShowModal" 
							data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('product/' . $v->id . '/quantity') }}" data-modal-title="{{ trans('translate.change_quantity') }}">
								{{ trans('translate.change_quantity') }}
							</button>
						</li>
<!-- -->

						<li>
							<button style="width:100%; margin-bottom:2px;" type="button" class="btn btn-info solsoShowModal" data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('product/' . $v->id) }}" data-modal-title="{{ trans('translate.show_product') }}">
								<i class="fa fa-eye"></i> {{ trans('translate.show') }}
							</button>

						</li>
						<li>
							<button style="width:100%; margin-bottom:2px;"type="button" class="btn btn-primary solsoShowModal" data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('product/' . $v->id . '/edit') }}" data-modal-title="{{ trans('translate.edit_product') }}">
								<i class="fa fa-edit"></i> {{ trans('translate.edit') }}
							</button>

						</li>

						<li>
							<button style="width:100%; margin-bottom:2px;"type="button" class="btn btn-danger solsoConfirm" data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('product/' . $v->id) }}">
								<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
							</button>
						</li>	

<!-- -->
					</ul>
				</div>
			</td>			
			
			<!-- <td>
				<button type="button" class="btn btn-info solsoShowModal"
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('product/' . $v->id) }}" data-modal-title="{{ trans('translate.show_product') }}">
					<i class="fa fa-eye"></i> {{ trans('translate.show') }}
				</button>
			</td>

			<td>
				<button type="button" class="btn btn-primary solsoShowModal"
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('product/' . $v->id . '/edit') }}" data-modal-title="{{ trans('translate.edit_product') }}">
					<i class="fa fa-edit"></i> {{ trans('translate.edit') }}
				</button>
			</td>

			<td>
				<button type="button" class="btn btn-danger solsoConfirm"
				data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('product/' . $v->id) }}">
					<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
				</button>
			</td> -->
		</tr>

	@endforeach

	</tbody>
</table>