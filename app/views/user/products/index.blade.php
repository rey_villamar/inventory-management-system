@section('content')

	<div class="col-md-12">
		<h1>
			<i class="fa fa-list"></i> {{ trans('translate.products') }}
		</h1>

		<button type="button" class="btn btn-primary solsoShowModal"
		data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('product/create') }}" data-modal-title="{{ trans('translate.create_new_product') }}">
			<i class="fa fa-user-plus"></i> {{ trans('translate.create_new_product') }}
		</button>
	</div>	

	<div class="col-md-12 top40">
		<h3>{{ trans('translate.products') }}</h3>

		<div id="ajaxTable" class="table-responsive">
			@include('user.products.table')	
		</div>	
	</div>
	
@stop