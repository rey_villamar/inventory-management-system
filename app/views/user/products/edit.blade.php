{{ Form::open(array('url' => 'product/' . $product->id, 'role' => 'form', 'method' => 'PUT', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}

	<div class="col-md-12">
		<div class="form-group">
			<label for="name">{{ trans('translate.name') }}</label>
			<input type="text" name="name" class="form-control required" autocomplete="off" value="{{ isset($inputs['name']) ? $inputs['name'] : $product->name }}">
			
			<?php echo $errors->first('name', '<p class="error">:messages</p>');?>
		</div>		

		<div class="form-group">
			<label for="quantity">{{ trans('translate.quantity') }}</label>
			<input type="text" name="quantity" class="form-control" autocomplete="off" value="{{ isset($inputs['quantity']) ? $inputs['quantity'] : $product->quantity }}">
			
			<?php echo $errors->first('quantity', '<p class="error">:messages</p>');?>
		</div>	

		<div class="form-group">
			<label for="quantity">Retail Price</label>
			<input type="text" name="retail" class="form-control" autocomplete="off" value="{{ isset($inputs['retail']) ? $inputs['retail'] : $product->retail }}">

			<?php echo $errors->first('retail', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<label for="quantity">Reseller Price</label>
			<input type="text" name="resell" class="form-control" autocomplete="off" value="{{ isset($inputs['reseller']) ? $inputs['reseller'] : $product->reseller }}">

			<?php echo $errors->first('resell', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<label for="quantity">Wholesale Price (3pcs)</label>
			<input type="text" name="wholesale" class="form-control" autocomplete="off" value="{{ isset($inputs['wholesale']) ? $inputs['wholesale'] : $product->wholesale }}">

			<?php echo $errors->first('wholesale', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<label for="quantity">Bulk Price (30pcs)</label>
			<input type="text" name="bulk" class="form-control" autocomplete="off" value="{{ isset($inputs['bulk']) ? $inputs['bulk'] : $product->bulk }}">

			<?php echo $errors->first('bulk', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<label for="quantity">Factory Price (50pcs)</label>
			<input type="text" name="factory" class="form-control" autocomplete="off" value="{{ isset($inputs['factory']) ? $inputs['factory'] : $product->factory }}">

			<?php echo $errors->first('factory', '<p class="error">:messages</p>');?>
		</div>	
		
		<!-- <div class="form-group">
			<label for="code">{{ trans('translate.code') }}</label>
			<input type="text" name="code" class="form-control" autocomplete="off" value="{{ isset($inputs['code']) ? $inputs['code'] : $product->code }}">
			
			<?php echo $errors->first('code', '<p class="error">:messages</p>');?>
		</div> -->		

		<!-- <div class="form-group">
			<label for="price">{{ trans('translate.price') }}</label>
			<input type="text" name="price" class="form-control" autocomplete="off" value="{{ isset($inputs['price']) ? $inputs['price'] : $product->price }}">
			
			<?php echo $errors->first('price', '<p class="error">:messages</p>');?>
		</div>	 -->		
		
		<div class="form-group">
			<label for="description">{{ trans('translate.description') }}</label>
			<textarea name="description" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['description']) ? $inputs['description'] : $product->description }}</textarea>
		</div>	
	</div>
	
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}	