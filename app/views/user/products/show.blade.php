<div class="col-md-12">
	<h1>{{ $product->name }}</h1>
	<hr>
</div>

<div class="col-md-12">
	<table class="table table-striped">
	<tbody>
		<tr>
			<td class="col-md-4">{{ trans('translate.quantity') }}</td>
			<td class="col-md-8">{{ $product->quantity }}</td>
		</tr>		
		
		<tr>
			<td>Retail Price</td>
			<td>{{ $currency->position == 1 ? $currency->name : '' }} {{ $product->retail }} {{ $currency->position == 2 ? $currency->name : '' }} </td>
			<!-- <td class="col-md-4">{{ trans('translate.code') }}</td>
			<td class="col-md-8">{{ $product->code }}</td> -->
		</tr>

		<tr>
			<td>Reseller Price</td>
			<td>{{ $currency->position == 1 ? $currency->name : '' }} {{ $product->reseller }} {{ $currency->position == 2 ? $currency->name : '' }} </td>
			<!-- <td>{{ trans('translate.price') }}</td>
			<td>{{ $currency->position == 1 ? $currency->name : '' }} {{ $product->price }} {{ $currency->position == 2 ? $currency->name : '' }} </td> -->
		</tr>

		<tr>
			<td>Wholesale Price (3pcs)</td>
			<td>{{ $currency->position == 1 ? $currency->name : '' }} {{ $product->wholesale }} {{ $currency->position == 2 ? $currency->name : '' }}</td>
		</tr>

		<tr>
			<td>Bulk Price (30pcs)</td>
			<td>{{ $currency->position == 1 ? $currency->name : '' }} {{ $product->bulk }} {{ $currency->position == 2 ? $currency->name : '' }} </td>
		</tr>

		<tr>
			<td>Factory Price (50pcs)</td>
			<td>{{ $currency->position == 1 ? $currency->name : '' }} {{ $product->factory }} {{ $currency->position == 2 ? $currency->name : '' }} </td>
		</tr>		
		
		<!-- <tr>
			<td colspan="2">
				<h4>{{ trans('translate.description') }}</h4>
				
				{{ $product->description }}
			</td>
		</tr> -->
	</tbody>
	</table>
</div>
<div class="col-md-12">
	<table class="table table-striped">
	<tbody>
		<tr>
			<td colspan="2">
				<h4>{{ trans('translate.description') }}</h4>
				
				{{ $product->description }}
			</td>
		</tr>
	</tbody>
	</table>
</div>	