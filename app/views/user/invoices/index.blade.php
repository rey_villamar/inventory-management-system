@section('content')

	<div class="col-md-12">
		@if ($currencies == 0)
			<div role="alert" class="alert alert-warning">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_currencies') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }}</a>
			</div>
		@endif		
		
		@if ($taxes == 0)
			<div role="alert" class="alert alert-warning">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_taxes') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('admin/settings') }}">{{ trans('translate.settings') }}</a>
			</div>
		@endif		

		@if ($products == 0)
			<div role="alert" class="alert alert-warning">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_products') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('product') }}">{{ trans('translate.products') }}</a>
			</div>
		@endif	
		
		<h1>
			<i class="fa fa-list"></i> {{ trans('translate.invoices') }}
		</h1>
		
		@if (!$userIsClient && $currencies != 0 && $taxes != 0 && $products != 0)
			<button type="button" class="btn btn-primary solsoShowModal" 
			data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/create') }}" data-modal-title="{{ trans('translate.create_new_invoice') }}">
				<i class="fa fa-plus"></i> {{ trans('translate.create_new_invoice') }}
			</button>
		@endif
	</div>	

	<div class="col-md-12 top40">
		<h3>{{ trans('translate.invoices') }}</h3>

		<div id="ajaxTable" class="table-responsive">
			@include('user.invoices.table')	
		</div>	
	</div>
	
	@include('user.invoices.addons.email')
	
@stop