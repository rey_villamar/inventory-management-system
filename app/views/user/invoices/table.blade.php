<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}">
	<thead>
		<tr>
			<th>{{ trans('translate.crt') }}.</th>
			<th class="col-md-1">{{ trans('translate.invoice') }}</th>
			<th class="col-md-2">
				@if ( ! $userIsClient )
					{{ trans('translate.client') }}
				@else
					{{ trans('translate.from') }}
				@endif
			</th>
			<th class="col-md-1 text-right">{{ trans('translate.amount') }}</th>
			<!-- <th class="col-md-1 text-right">{{ trans('translate.paid') }}</th>
			<th class="col-md-1 text-right">{{ trans('translate.balance') }}</th>			 -->
			<th class="col-md-1 text-center">{{ trans('translate.due_date') }}</th>

			<th class="small">{{ trans('translate.status') }}</th>
			<th class="col-md-1 text-center">Mode of Payment</th>
			<!-- <th class="small">{{ trans('translate.state') }}</th> -->
			<!-- sched-->
			<!-- <th class="xs-small">{{ trans('translate.action') }}</th> -->
			
			@if ( ! $userIsClient )
			<!-- email-->
				<!-- <th class="xs-small">{{ trans('translate.action') }}</th> -->
				<!-- quick-->
				<th class="small">{{ trans('translate.action') }}</th>
				<!-- show-->
				<!-- <th class="small">{{ trans('translate.action') }}</th> -->
				<!-- edit-->
				<!-- <th class="small">{{ trans('translate.action') }}</th> -->
			@endif
			<!-- delete-->
			<th class="small">{{ trans('translate.action') }}</th>
		</tr>
	</thead>

	<tbody>
	@foreach ($invoices as $crt => $v)
		<tr>
			<td>
				{{ $crt+1 }}
			</td>

			<td>
				{{ $v->number }}
			</td>

			<td>
				@if ( ! $userIsClient )
					{{ $v->name }}
				@else
					{{ $owner->name }}
				@endif	
			</td>	

			<td class="text-right">
				{{ Solso::currencyPosition($v->currency, $v->position, $v->amount) }}
			</td>	
			
		<!-- 	<td class="text-right">
				{{ Solso::currencyPosition($v->currency, $v->position, $v->paid) }}
			</td>	 -->					
			
			<!-- <td class="text-right">
				{{ $v->position == 1 ? $v->currency : '' }} 
				
				@if ( $v->status == 'paid' )
					0
				@else
					- {{ Invoice::calculatePaid($v->amount, $v->paid) }}
				@endif
				
				{{ $v->position == 2 ? $v->currency : '' }}
			</td>	 -->		
			
			<td class="text-center">
				{{ $v->due_date }}
			</td>	
			

			<td>
				<label class="label label-{{ str_replace(' ', '-', $v->status) }}">
					{{ trans('translate.' . str_replace(' ', '_', $v->status)) }}
				</label>
			</td>
			<td>
				<select class="modeOfPayment" data-id="{{ $v->id }}" onchange="changeModeOfPayment(this)">
		
				<option value="0" SELECTED="TRUE">Choose An Option</option>
				@foreach ($mode_of_payment as $a => $b)
					
					<option value="{{ $b->id }}">{{ $b->name }} </option>
				
				@endforeach

				</select>	
			</td>						
			
			<!-- <td>
				@if ($v->state == 0)
					<label class="label label-overdue">{{ trans('translate.unseen') }}</label>
				@else
					<label class="label label-paid">{{ trans('translate.seen') }}</label>
				@endif
			</td>		 -->	
			
			<!-- @if ( ! $userIsClient )
				<td>
					@if ( $v->scheduleID )
						<button type="button" class="btn btn-default solsoShowModal" 
						data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('schedule/' . $v->scheduleID . '/edit/from-invoice') }}" data-modal-title="{{ trans('translate.edit_schedule') }}">						
							<i class="fa fa-edit"></i> <i class="fa fa-clock-o"></i>
						</button>						
					@else
						<button type="button" class="btn btn-default solsoShowModal" 
						data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('schedule/create/' . $v->id) }}" data-modal-title="{{ trans('translate.create_new_schedule') }}">						
								<i class="fa fa-plus"></i> <i class="fa fa-clock-o"></i>
						</button>						
					@endif	
				</td>

				<td>		
					<button type="button" class="btn btn-default solsoConfirm" 
					data-toggle="modal" data-target="#solsoSendEmail" data-href="{{ URL::to('email/invoice/' . $v->id . '/pdf') }}">
						<i class="fa fa-envelope"></i>
					</button>
				</td>	
			@endif -->
			
			<td>
				<div class="dropdown">
					<button class="btn solso-pdf dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						{{ trans('translate.quick_actions') }}
						<span class="caret"></span>
					</button>
				
					<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
						@if ( ! $userIsClient )
							<li role="presentation">
								<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id . '/status') }}" data-modal-title="{{ trans('translate.change_status') }}">
									{{ trans('translate.change_status') }}
								</a>
							</li>
							<li role="presentation">
								

							<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id) }}" data-modal-title="{{ trans('translate.show_invoice') }}">
									{{ trans('translate.show') }}
							</a>

							</li>
							<li role="presentation">
								

							<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id . '/edit') }}" data-modal-title="{{ trans('translate.show_invoice') }}">
									{{ trans('translate.edit') }}
							</a>

							</li>

							<!-- <li role="presentation">
								<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id . '/duedate') }}" data-modal-title="{{ trans('translate.change_due_date') }}">
									{{ trans('translate.change_due_date') }}
								</a>							
							</li>
							
							<li role="presentation">
								<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id . '/add-payment') }}" data-modal-title="{{ trans('translate.add_payment') }}">
									{{ trans('translate.add_payment') }}
								</a>
							</li> -->
							<!-- <li class="divider"></li> -->
						@endif
						
						<!-- <li role="presentation">
							<a role="menuitem" tabindex="-1" href="{{ URL::to('export/invoice/' . $v->id . '/pdf') }}">
								{{ trans('translate.export_pdf') }}
							</a>
						</li>
						
						<li role="presentation">
							<a role="menuitem" tabindex="-1" href="{{ URL::to('export/invoice/' . $v->id . '/excel') }}">
								{{ trans('translate.export_excel') }}
							</a>
						</li> -->
					</ul>
				</div>
			</td>			
			
			<!-- <td>
				<button type="button" class="btn btn-info solsoShowModal" 
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id) }}" data-modal-title="{{ trans('translate.show_invoice') }}">
					<i class="fa fa-eye"></i> {{ trans('translate.show') }}
				</button>
			</td>	 -->	
			@if ( ! $userIsClient )
				<!-- <td>		
					<button type="button" class="btn btn-primary solsoShowModal" 
					data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id . '/edit') }}" data-modal-title="{{ trans('translate.show_invoice') }}">
						<i class="fa fa-edit"></i> {{ trans('translate.edit') }}
					</button>
				</td> -->
				<td>		
					<button type="button" class="btn btn-danger solsoConfirm" 
					data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('invoice/' . $v->id) }}">
						<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
					</button>		
				</td>
			@endif

		</tr>
		
	@endforeach
	
	</tbody>
</table>	

<!DOCTYPE html>

<!DOCTYPE html>
<html>

<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        changeModeOfPayment = function(me){
	        var invoice = $(me);
	        var invoiceID = invoice.attr("data-id");
	        var selected = invoice.val();
	        $.ajax({ 
	            type: 'POST', 
	            url: '<?php echo URL::to('client'); ?>', 
	            data: { invoiceID: invoiceID, selected: selected}, 
	            dataType: 'json',
	            success: function (data) { 
	               console.log("SUCCESS "+data);
	            }
	        });	        
        }
    });
</script>




