<table>
	<tr>
		<td colspan="6">
			<h2>
				{{ trans('translate.invoice') }} {{ $invoiceCode }} {{ $invoice->number }}
			</h2>
		</td>
	</tr>
	
	<tr>
		<td>
			<h3>{{ trans('translate.bill_from') }}:</h3>
		</td>

		<td>
			<h3>{{ trans('translate.bill_to') }}:</h3>
		</td>
	</tr>
	
	<tr>
		<td>
			<h4>{{ $owner->name }}</h4>
		</td>

		<td>
			<h4>{{ $invoice->name }}</h4>
		</td>
	</tr>	
	
	<tr>
		<!-- <td>
			{{ $owner->country }} - {{ $owner->state }} - {{ $owner->city }}
		</td>
 -->
 		<td>
 			{{ $owner->address}}
 		</td>
		<!-- <td>
			{{ $invoice->country }} - {{ $invoice->state }} - {{ $invoice->city }}
		</td> -->
		<td>
			{{ $invoice->email}}
		</td>
	</tr>
	
	<tr>
		<!-- <td>
			{{ $owner->address }} - {{ $owner->zip }}
		</td> -->
		<td>
			{{ $owner->state }}
		</td>

	<!-- 	<td>
			{{ $invoice->address }} - {{ $invoice->zip }}
		</td> -->
		<td>
			{{ $invoice->contact }}
		</td>
	</tr>	
	
	<tr>
		<!-- <td>
			{{ trans('translate.bank') }}: {{ $owner->bank }}
		</td> -->

		<!-- <td>
			{{ trans('translate.bank') }}: {{ $invoice->bank }}
		</td> -->
		<td>
			{{ $owner->contact }}
		</td>
		<td>
			{{ $invoice->address }}
		</td>
	</tr>	
	
	<!-- <tr>
		<td>
			{{ trans('translate.bank_account') }}: {{ $owner->bank_account }}
		</td>

		<td>
			{{ trans('translate.bank_account') }}: {{ $invoice->bank_account }}
		</td>
	</tr> -->	
</table>

<table>
	<tr>
		<th>{{ trans('translate.crt') }}.</th>
		<th>{{ trans('translate.product') }}</th>
		<th>{{ trans('translate.price') }}</th>
		<th>{{ trans('translate.quantity') }}</th>
		<!-- <th>{{ trans('translate.tax_rate') }}</th>
		<th>{{ trans('translate.discount') }}</th> -->
		<th>{{ trans('translate.amount') }}</th>
	</tr>	

	<?php $subTotalItems 	= 0;?>
	<?php $taxItems 		= 0;?>
	<?php $discountItems	= 0;?>
	<?php $invoiceDiscount	= 0;?>
	
	@foreach ($products as $crt => $product)
		<tr>
			<td align="left">
				{{ $crt + 1 }}
			</td>
			
			<td align="left">
				{{ $product->name }}
			</td>	
			
			<td align="center">
				{{ Solso::currencyPosition($invoice->currency, $invoice->position, $product->price) }}
			</td>	

			<td align="center">
				{{ $product->quantity }}
			</td>					
			
			<!-- <td align="center">
				{{ $product->tax }} %
			</td>					
			
			<td align="center">
				@if ($product->discount == 0)
					-
				@else
					{{ Solso::currencyPosition($invoice->currency, $invoice->position, $product->discount_value) }}
				@endif
			</td> -->
			
			<td align="right">
				{{ Solso::currencyPosition($invoice->currency, $invoice->position, $product->amount) }}
			</td>					
		</tr>
		
		<tr>
			<td colspan="7">
				<h3> Remarks </h3>
			</td>

		</tr>
		<tr>
			<td>{{ $product->description }}</td>
		</tr>
		
		<?php $subTotalItems 	+= $product->quantity * $product->price;?>
		<?php $taxItems 		+= ($product->quantity * $product->price) * ($product->tax / 100);?>							
		<?php $discountItems 	+= $product->discount_value;?>				
	@endforeach

	<tr>
		<td colspan="7"></td>
	</tr>
	
	<tr>
		<td colspan="4"></td>
		
		<td colspan="2">
			{{ trans('translate.subtotal') }}
		</td>	

		<td align="right">
			{{ Solso::currencyPosition($invoice->currency, $invoice->position, $subTotalItems) }}
		</td>
	</tr>

	<tr>
		<td colspan="4"></td>
		
		<td colspan="2">
			<!-- {{ trans('translate.tax') }} -->
			Shipping Fee
		</td>	

		<td align="right">
			<!-- {{ Solso::currencyPosition($invoice->currency, $invoice->position, $taxItems) }} -->
			{{ $invoice->currency }} {{ Solso::currencyPosition($product->shipping_fee, $invoice->position, "") }}
		</td>
	</tr>						
	
<!-- 	@if ($discountItems != 0)
	<tr>
		<td colspan="4"></td>
		
		<td colspan="2">
			{{ trans('translate.products_discount') }}
		</td>	

		<td align="right">
			{{ Solso::currencyPosition($invoice->currency, $invoice->position, $discountItems) }}
		</td>
	</tr>	
	@endif						
	
	@if ($invoice->discount != 0)
	<tr>
		<td colspan="4"></td>
		
		<td colspan="2">
			{{ trans('translate.invoice_discount') }}
		</td>	

		<td align="right">
			{{ Solso::currencyPosition($invoice->currency, $invoice->position, $invoice->discount) }}
		</td>
	</tr>	
	@endif -->
	
	<tr>
		<td colspan="4"></td>
		
		<td colspan="2">
			<b>{{ trans('translate.total') }}</b>
		</td>	

		<td align="right">
			<b>{{ Solso::currencyPosition($invoice->currency, $invoice->position, $invoice->amount + $product->shipping_fee) }}</b>
		</td>
	</tr>						
				
</table>	