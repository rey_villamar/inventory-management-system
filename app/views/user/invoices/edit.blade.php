{{ Form::open(array('url' => 'invoice/' . Request::segment(2), 'role' => 'form', 'method' => 'PUT', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}
		
	<div class="form-group col-md-12">
		<label for="client_id">{{ trans('translate.client') }}</label>
		<select name="client_id" class="form-control required select2">
			<option selected value="{{ Input::old('client_id') ? Input::old('client_id') : $invoice->client_id }}"> {{ Input::old('client_id') ? Input::old('client_id') : $invoice->name }} </option>	
			<option value="">{{ trans('translate.choose') }}</option>
			
			@foreach ($clients as $c)
				<option value="{{ $c->id }}"> {{ $c->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('client_id', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-3">
		<label for="number">{{ trans('translate.invoice_number') }}</label>
		<div class="input-group">
			<span class="input-group-addon solso-pre">{{ $invoiceCode }}</span>		
			<input type="text" name="number" class="form-control required" autocomplete="off" value="{{ Input::old('number') ? Input::old('number') : $invoice->number }}">
		</div>
		<?php echo $errors->first('estimate', '<p class="error">:messages</p>');?>
	</div>
	
	<div class="form-group col-md-3">
		<label for="start_date">{{ trans('translate.start_date') }}</label>
		<input type="text" name="start_date" class="form-control datepicker required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd"	value="{{ Input::old('start_date') ? Input::old('start_date') : $invoice->start_date }}">

		<?php echo $errors->first('start_date', '<p class="error">:messages</p>');?>
	</div>

	<div class="form-group col-md-3">
		<label for="due_date">{{ trans('translate.due_date') }}</label>
		<input type="text" name="due_date" class="form-control datepicker required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ Input::old('due_date') ? Input::old('due_date') : $invoice->due_date }}">

		<?php echo $errors->first('due_date', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-3">
		<label for="currency">{{ trans('translate.currency') }}</label>
		<select name="currency_id" class="form-control required solsoCurrencyEvent">
			<option selected value="{{ Input::old('currency_id') ? Input::old('currency_id') : $invoice->currency_id }}"> {{ Input::old('currency_id') ? Input::old('currency_id') : $invoice->currency }} </option>	
			<option value="">{{ trans('translate.choose') }}</option>
			
			@foreach ($currencies as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach					
			
		</select>
		
		<?php echo $errors->first('currency_id', '<p class="error">:messages</p>');?>
	</div>		
	<div class="clearfix"></div>	
	
	
	<div class="table-responsive col-md-12">
		<table class="table">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="td-product">{{ trans('translate.product') }}</th>
				<th class="td-info" style="width:18%">TYPE OF PRICE</th>
				<th class="td-info">{{ trans('translate.price') }}</th>
				<th class="td-info">{{ trans('translate.quantity') }}</th>
				<!-- <th class="td-info">{{ trans('translate.tax_rate') }}</th>
				<th class="td-info">{{ trans('translate.discount') }}</th>
				<th class="td-info">{{ trans('translate.discount_type') }}</th> -->
				<th class="td-info">{{ trans('translate.subtotal') }}</th>
				<th class="delete-button">{{ trans('translate.action') }}</th>
			</tr>	
		</thead>
			
		<tbody class="solsoParent">	
				@foreach ($invoiceProducts as $crt => $p)
				
				<tr {{ $crt == 0 ? 'class="solsoChild"' : '' }}>
					<td class="crt">
						{{ $crt + 1 }}
					</td>
						
					<td>
						<select name="products[]" id="products" class="form-control required solsoSelect2 solsoCloneSelect2">
							<option selected value="{{ Input::old('products[]') ? Input::old('products[]') : $p->product_id }}">
								{{ Input::old('products[]') ? Input::old('products[]') :  substr($p->name, 0, 100) }} {{ strlen($p->name) > 100 ? '..' : '' }}
							</option>
							<option value="">{{ trans('translate.choose') }}</option>
							
							@foreach ($products as $v)
								<option value="{{ $v->id }}"> {{ substr($v->name, 0, 100) }} {{ strlen($v->name) > 100 ? '..' : '' }} </option>
							@endforeach			

						</select>				
					</td>
					<td>
					<form method="post">
						<select name="priceType" class="form-control" id="priceType">
							<option value="" selected>choose</option>			
							<option name="retail" value="retail">Retail</option>
							<option name="reseller" value="reseller">Reseller</option>
							<option name="wholesale" value="wholesale">Wholesale</option>
							<option name="bulk" value="bulk" >Bulk</option>
							<option name="factory" value="factory">Factory</option>
						</select>
					</form>
					</td>
					
					<td>
						<input type="text" name="price[]" id="price" class="form-control required solsoEvent" autocomplete="off" >
						<!-- value="{{ $p->price }}" -->
					</td>

					<td>
						<input type="text" name="qty[]" id="qty" class="form-control required solsoEvent" autocomplete="off" value="{{ $p->quantity }}">
					</td>
					
					<!-- <td>
						<select name="taxes[]" class="form-control required solsoEvent">
							<option selected value="{{ Input::old('taxes[]') ? Input::old('taxes[]') : $p->tax }}"> {{ Input::old('taxes[]') ? Input::old('taxes[]') : $p->tax }} %</option>
							<option value="">{{ trans('translate.choose') }}</option>
							
							@foreach ($taxes as $v)
								<option value="{{ $v->value }}"> {{ $v->value }} %</option>
							@endforeach			
							
						</select>					
					</td> -->
					
					<!-- <td class="no-right-padding">
						<input type="text" name="discount[]" class="form-control" autocomplete="off" value="{{ $p->discount }}">
					</td> -->	
				
					<!-- <td class="no-left-padding">
						<select name="discountType[]" class="form-control solsoEvent">
						
							@if ($p->discount_type == 0)
								<option value="">{{ trans('translate.choose') }}</option>
							@else
							<option selected value="{{ Input::old('discountType[]') ? Input::old('discountType[]') : $p->discount_type }}">
								{{ Input::old('discountType[]') ? Input::old('discountType[]') : $p->discount_type == 1 ? trans('translate.amount') : '%' }} 
							</option>
							@endif	
							
							<option value="1">{{ trans('translate.amount') }}</option>
							<option value="2">%</option>
						</select>
					</td> -->

					<td>
						<h4 class="pull-right">
							<span class="solsoSubTotal">{{ $p->amount }}</span>
							<span class="solsoCurrency">{{ $invoice->currency }}</span>
						</h4>	
					</td>						
					
					<td>		
						<button type="button" class="btn btn-danger removeClone">
							<i class="fa fa-minus"></i>
						</button>		
					</td>						
				</tr>
					
				@endforeach			
			
			</tbody>
		
			<tfoot>
				<tr>
					<td colspan="5">
						<!-- <div class="col-md-12"> -->
						<div class="form-inline">
							<label for="end" class="show" >Shipping Fee</label>
							<input  type="text" name="shippingFee" id="shippingFee" class="form-control" autocomplete="off" value="{{ Input::old('shippingFee') ? Input::old('shippingFee') : $invoice->shipping_fee }}">
							<!-- <input type="text" name="invoiceDiscount" class="form-control" autocomplete="off" value="{{ $invoice->discount }}">
							
							<select name="invoiceDiscountType" class="form-control solsoEvent">
							
								@if ($invoice->type == 0)
									<option value="">choose</option>
								@else							
								<option selected value="{{ Input::old('invoiceDiscountType[]') ? Input::old('invoiceDiscountType[]') : $invoice->type }}">
									{{ Input::old('invoiceDiscountType[]') ? Input::old('invoiceDiscountType[]') : $invoice->type == 1 ? trans('translate.amount') : '%' }} 
								</option>
								@endif
								
								<option value="1">{{ trans('translate.amount') }}</option>
								<option value="2">%</option>
							</select>	 -->						
						<!-- </div>		 -->
						</div>				
					</td>
						
						
					<td colspan="2">
						<!-- <h3 class="pull-right top10">{{ trans('translate.total') }}</h3> -->
						<h4 class="pull-right top10">Subtotal:  </h4>
						<h4 class="pull-right top10">Shipping Fee:  </h4>
						<h3 class="pull-right top10">{{ trans('translate.total') }}</h3>
					</td>
					
					<td colspan="2">
						<h3 class="top10">
							<span class="solsoTotal">{{ $invoice->amount }}</span>
							<span class="solsoCurrency">{{ $invoice->currency }}</span>
						</h3>
						<h3 class="top10">
							<span id="shippingFeeValue"> {{ $invoice->shipping_fee }} </span>
							<span class="solsoCurrency">{{ $invoice->currency }}</span>
						</h3>
						<h3 class="top10">
							<span id="total"> {{ $invoice->shipping_fee + $invoice->amount }} </span>
							<span class="solsoCurrency">{{ $invoice->currency }}</span>
						</h3>
					</td>
				</tr>
				<tr>

				</tr>	
			</tfoot>
		</table>
	</div>		
	
	<div class="form-group top20 text-center">
		<button type="button" class="btn btn-primary btn-lg" id="createClone">
			<i class="fa fa-plus"></i> {{ trans('translate.add_new_product') }}
		</button>
	</div>	
	
	<div class="form-group col-md-12">
		<!-- <label for="description"> {{ trans('translate.description') }} </label> -->
		<label for="description"> Remarks </label>
<!-- 		<textarea name="description" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['description']) ? $inputs['description'] : $invoice->description }}</textarea> -->
		<textarea name="terms" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['terms']) ? $inputs['terms'] : $invoice->terms }}</textarea>
	</div>	

	<!-- <div class="form-group col-md-12">
		<label for="terms"> {{ trans('translate.terms_conditions') }} </label>
		<textarea name="terms" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['terms']) ? $inputs['terms'] : $invoice->terms }}</textarea>
	</div>	 -->
	
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.date_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	<script type="text/javascript">
		// $("#shippingFee").change(function(){
  //  		var shipFee= Number($("#shippingFee").val());
  //  		var subtotal=Number($(".solsoTotal").html());
  //  		var total= shipFee + subtotal ;
  //  		$("#shippingFeeValue").html(shipFee);
  //  		$("#total").html(total);
  	$("#priceType").change(function(){
    	var products = $("#products").val();
    	var priceType = $(this).val();
    	$.ajax({
			url: "<?php echo URL::to('get-price'); ?>",
			type: 'POST',
			data: 'products='+products+'&priceType='+priceType+'',
			dataType: "json",
			success:function(data){
				$('#price').val(data);				
			}

    	});
    	return false;   	
	});

	$("#shippingFee, #qty, #price, #priceType, #products").change(function(){
   		var shipFee= Number($("#shippingFee").val());
   		var subtotal=Number($(".solsoTotal").html());
   		var total= shipFee + subtotal ;
   		$("#shippingFeeValue").html(shipFee);
   		$("#total").html(total);
   });




	</script>
	
{{ Form::close() }}	

