{{ Form::open(array('url' => 'invoice/add-payment/' . $invoice->id, 'role' => 'form', 'method' => 'POST', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}

	<div class="form-group col-md-4">		
		<label for="amount">{{ trans('translate.amount_paid') }}</label>
		<input type="text" name="amount" class="form-control required" autocomplete="off" data-parsley-type="number">
			
		<?php echo $errors->first('amount', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-4">
		<label for="start">{{ trans('translate.start_date') }}</label>
		<input type="text" name="start" class="form-control required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd">

		<?php echo $errors->first('start', '<p class="error">:messages</p>');?>
	</div>
	
	<div class="form-group col-md-4">	
		<label for="payment">{{ trans('translate.payment_method') }}</label>
		<select name="payment" class="form-control required">
			<option value="" selected>choose</option>
			
			@foreach ($payments as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach					
			
		</select>
		
		<?php echo $errors->first('payment', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}