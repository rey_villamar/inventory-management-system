{{ Form::open(array('url' => 'invoice', 'role' => 'form', 'class' => 'solsoForm')) }}

	<div class="form-group col-md-12">

		<label for="client_id">{{ trans('translate.client') }}</label></br>
		<input type="checkbox" name="cbox" id="cbox"> CHOOSE ( not required)
		</br>
		</br>
		<select id="client_id" disabled="disabled" name="client_id" class="form-control required solsoSelect2">
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($clients as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('client_id', '<p class="error">:messages</p>');?>
	</div>
	<div class="form-group col-md-12" id=>

		<!-- <label for="client_id">{{ trans('translate.client') }}</label></br> -->
		<input type="checkbox" name="newForm" id="newForm"> New order Form.
		</br>
		</br>
		<!-- {{ Form::open(array('url' => 'client', 'role' => 'form', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }} -->
		<div id="newInputForm" class="form-group col-md-12">

						<div class="form-group">
							<label>Name: </label>
							<input id="clientName" name="clientName" class="form-control" type="text" value="REY">
						</div>
						<div class="form-group">
							<label>Delivery Address: </label>
							<input id="clientAddress" name="clientAddress" class="form-control" type="text" value="78 C Del Rosario St. Marulas Valenzuela City">
						</div>
						<div class="form-group">
							<label >{{ trans('translate.email') }}</label>
							<input id="clientEmail" type="email" name="clientEmail" class="form-control" autocomplete="off" value="reyvillamar@gmail.com">
							
		
						</div>	
						<div class="form-group">
							<label>Contact number: </label>
							<input id="clientContact" name="clientContact" class="form-control" type="text" value="234234523">
						</div>
						<div class="form-group">
							<button id="addClient" class="btn btn-primary btn-lg" type="button"><i class="fa fa-plus"></i> Add New Client</button>
						</div>

			
		</div>
	<!-- 	{{ Form::close() }} -->
			
	</div>
	
	<div class="form-group col-md-3">
		<label for="number">{{ trans('translate.invoice_number') }}</label>
		<div class="input-group">
			<span class="input-group-addon solso-pre">{{ $invoiceCode }}</span>
			<input type="text" name="number" class="form-control required no-line" autocomplete="off" value="{{ $invoiceNumber ? $invoiceNumber : Input::old('number') }}">
		</div>
		
		<?php echo $errors->first('number', '<p class="error">:messages</p>');?>
	</div>		

	<div class="form-group col-md-3">
		<div class="form-group">
			<label for="start_date">{{ trans('translate.start_date') }}</label>
			<input type="text" name="start_date" class="form-control datepicker required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd"	value="{{ isset($inputs['start_date']) ? $inputs['start_date'] : '' }}">

			<?php echo $errors->first('start_date', '<p class="error">:messages</p>');?>
		</div>
	</div>

	<div class="form-group col-md-3">
		<div class="form-group">
			<label for="due_date">{{ trans('translate.due_date') }}</label>
			<input type="text" name="due_date" class="form-control datepicker required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ isset($inputs['due_date']) ? $inputs['due_date'] : '' }}">

			<?php echo $errors->first('due_date', '<p class="error">:messages</p>');?>
		</div>
	</div>			
		
	<div class="form-group col-md-3">
		<label for="currency">{{ trans('translate.currency') }}</label>
		<select name="currency_id" class="form-control required solsoCurrencyEvent" >
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($currencies as $v)
				<!-- <option value="{{ $v->id }}" selected > {{ $v->name }} </option> -->
				<option value="1"  > {{ $v->name }} </option>
			@endforeach					
			
		</select>
		
		<?php echo $errors->first('currency_id', '<p class="error">:messages</p>');?>
	</div>		
	<div class="clearfix"></div>	
	
	<div class="table-responsive col-md-12">
		<table class="table">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="td-product">{{ trans('translate.product') }}</th>
				<th class="td-info" style="width:18%">TYPE OF PRICE</th>
				<th class="td-info">{{ trans('translate.price') }}</th>
				<th class="td-info">{{ trans('translate.quantity') }}</th>
				<!-- <th class="td-info">{{ trans('translate.tax_rate') }}</th>
				<th class="td-info">{{ trans('translate.discount') }}</th>
				<th class="td-info">{{ trans('translate.type') }}</th> -->
				<th class="td-info">{{ trans('translate.subtotal') }}</th>
				<th class="delete-button">{{ trans('translate.action') }}</th>
			</tr>	
		</thead>
			
		<tbody class="solsoParent">	
			<tr class="solsoChild">
				<td class="crt">1</td>
				
				<td>
					<select name="products[]" id="products" class="form-control required solsoSelect2 solsoCloneSelect2">
						<option value="" selected id="sel">{{ trans('translate.choose') }}</option>
						
						@foreach ($products as $v)
							<option value="{{ $v->id }}"> {{ substr($v->name, 0, 100) }} {{ strlen($v->name) > 100 ? '...' : '' }} </option>
						@endforeach			
						
					</select>				
				</td>
				<td>
					<form method="post">
					<select name="priceType" class="form-control" id="priceType">
						<option value="" selected>choose</option>			
						<option name="retail" value="retail">Retail</option>
						<option name="reseller" value="reseller">Reseller</option>
						<option name="wholesale" value="wholesale">Wholesale</option>
						<option name="bulk" value="bulk" >Bulk</option>
						<option name="factory" value="factory">Factory</option>
					</select>
					</form>
					<?php
							
					 ?>
					
					<!-- <input type="text" name="typeprice[]" class="form-control required solsoEvent" autocomplete="off"> -->
				</td>
				<td>
					
					
					<input type="text" name="price[]" id="price" class="form-control required solsoEvent" autocomplete="off">
				</td>

				<td>
					<input type="text" id="qty" name="qty[]" class="form-control required solsoEvent" autocomplete="off">
				</td>				
				
				<!-- <td>
					<select name="taxes[]" class="form-control required solsoEvent">
						<option value="" selected>{{ trans('translate.choose') }}</option>
						
						@foreach ($taxes as $v)
							<option value="{{ $v->value }}"> {{ $v->value }} %</option>
						@endforeach			
						
					</select>					
				</td> -->
				
				<!-- <td>
					<input type="text" name="discount[]" class="form-control" autocomplete="off">
				</td>	 -->
			
			<!-- 	<td>
					<select name="discountType[]" class="form-control solsoEvent">
						<option value="" selected>{{ trans('translate.choose') }}</option>
						<option value="1">{{ trans('translate.amount') }}</option>
						<option value="2">%</option>
					</select>
				</td> -->

				<td>
					<h4 class="pull-right">
						<span class="solsoSubTotal">0.00</span>
					</h4>	
				</td>
				
				<td>
					<button type="button" class="btn btn-danger disabled removeClone">
						<i class="fa fa-minus"></i>
					</button>
				</td>					
			</tr>
		</tbody>
		
		<tfoot>
			<tr>
				<td colspan="5">
					<!-- <div class="form-inline">
						<label for="end" class="show">{{ trans('translate.invoice_discount') }}</label>
						<input type="text" name="invoiceDiscount" class="form-control" autocomplete="off">
						
						<select name="invoiceDiscountType" class="form-control solsoEvent">
							<option value="" selected>{{ trans('translate.choose') }}</option>
							<option value="1">{{ trans('translate.amount') }}</option>
							<option value="2">%</option>
						</select>							
					</div>	 -->	
					<div class="form-inline">
								<label for="end">Shipping Fee</label>
								<input  type="text" name="shippingFee" id="shippingFee" class="form-control" autocomplete="off">
					</div>				
				</td>
				
				<!-- <td colspan="2">
					<h3 class="pull-right top10">{{ trans('translate.total') }}</h3>
				</td> -->
				<td colspan="2">
					<h4 class="pull-right top10">Subtotal:  </h4>
					<h4 class="pull-right top10">Shipping Fee:  </h4>
					<h3 class="pull-right top10">{{ trans('translate.total') }}</h3>
					
				</td>
				
				<!-- <td colspan="2">
					<h3 class="top10">
						<span class="solsoTotal">0.00</span>
						<span class="solsoCurrency"></span>
					</h3>
				</td> -->
				<td colspan="2">
					<h4 class="top10">
						<span class="solsoTotal" id="sub">0.00</span>
						<span class="solsoCurrency"></span>
					</h4>
					<h4 class="top10">
						<span id="shippingFeeValue">0.00</span>
						<span class="solsoCurrency"></span>

					</h4>
					<h3 class="top10">
						<span  class="solsoTotal" id="total">0.00</span>
						<span class="solsoCurrency"></span>
					</h3>
				</td>
			</tr>
		</tfoot>
		</table>
	</div>		
	
	<div class="form-group top20 text-center">
		<button type="button" class="btn btn-primary btn-lg" id="createClone"><i class="fa fa-plus"></i> {{ trans('translate.add_new_product') }}</button>
	</div>	

	<div class="form-group col-md-12">
		<label for="description"> Remarks </label>
		<!-- <textarea name="description" class="form-control" rows="7" autocomplete="off"></textarea>
	</div>	 -->

	<!-- <div class="form-group col-md-12">
		<label for="terms"> {{ trans('translate.terms_conditions') }} </label> -->
		<textarea name="terms" class="form-control" rows="7" autocomplete="off"></textarea>
	</div>	
		
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" id="btnsave"
		data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_saved') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
		
{{ Form::close() }}

<script type="text/javascript">
	var ckbox = $('#cbox');
	var ckbox2 = $('#newForm');
	$('#newInputForm').hide();
    $('input').on('click',function () {
        if (ckbox.is(':checked')) {
            $('#client_id').removeAttr("disabled");
        } else {
           
             $('#client_id').attr("disabled","disabled");
        }
       
    });
    $('input').on('click',function () {
        if (ckbox2.is(':checked')) {
           $('#newInputForm').show();
        } else {
                     
           $('#newInputForm').hide();
        }
    });

    $("#priceType").change(function(){
    	var products = $("#products").val();
    	var priceType = $(this).val();
    	$.ajax({
			url: "<?php echo URL::to('get-price'); ?>",
			type: 'POST',
			data: 'products='+products+'&priceType='+priceType+'',
			dataType: "json",
			success:function(data){
				$('#price').val(data);				
			}

    	});
    	return false;   	
	});

	$("#shippingFee").change(function(){
   		var shipFee= Number($("#shippingFee").val());
   		var subtotal=Number($(".solsoTotal").html());
   		var total= shipFee + subtotal ;
   		$("#shippingFeeValue").html(shipFee);
   		$("#total").html(total);
   });
   
   $("#addClient").click(function(){
	  console.log("Add Client") ;
	  var clientName = $("#clientName").val();
	  var clientAddress = $("#clientAddress").val();
	  var clientEmail = $("#clientEmail").val();
	
	  var clientContact = $("#clientContact").val();


		$.ajax({ 
			type: 'POST', 
			url: '<?php echo URL::to('add-client'); ?>', 
			data: { name: clientName, address: clientAddress,email_c:clientEmail,contact:clientContact,description:"",email:"temp@gmail.com",_token:"9J9KvctnAqcOgbj1ZadE5Owvt8DO7scTlceq77Yf"}, 
			dataType: 'json',
			success: function (data) { 
			 	$("#cbox").click();
				$("#client_id").append('<option selected value="'+data+'"> '+clientName+' </option>')
				$("#client_id").val(data);
				$("#newForm").click();
				// console.log(data);
			}
		});
			       // $.ajax({ 
			       //      type: 'POST', 
			       //      url: '<?php echo URL::to('insert-client'); ?>', 
			       //      data: {}, 
			       //      dataType: 'json',
			       //      success: function (data) { 
				      //      $("#cbox").click();
				      //       $("#client_id").append('<option selected value="'+data+'"> '+$("#clientName").html()+' </option>')
				      //        $("#client_id").val(data);
				      //       $("#newForm").click();
			       //         console.log("SUCCESS "+data);
			       //      }
			       //  });
			        
			        
			         
			               
			               
	  return false;
   });

	// $("#btnsave").click(function(){
	// 	var shipFee= $("#shippingFee").val();
	// 	$.ajax({
	// 		url: "<?php echo URL::to('save-shipping-fee') ?>",
	// 		type: "POST",
	// 		data: 'shipping_fee='+shipFee+'',
	// 		dataType: "json",

	// 		success:function(data){

	// 		}


	// 	});
	// 	return false;

	// });



	
</script>