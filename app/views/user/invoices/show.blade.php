<div class="col-md-12">
	<h1>{{ trans('translate.invoice') }} {{ $invoiceCode }} {{ $invoice->number }}</h1>
	<hr>
</div>

<div class="col-md-6">
	<h3 class="noTop">{{ trans('translate.bill_from') }}:</h3>
	
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<td>
					<h4>{{ $owner->name }}</h4>
				</td>
			</tr>
			
			<tr>
				<td>
					{{ $owner->address }} - {{ $owner->zip }}
					
				</td>
			</tr>			
			
			<tr>
				<td>
					 {{ $owner->state }}
				</td>
			</tr>			
			
			<tr>
				<td>
				 {{ $owner->contact }}
				</td>
			</tr>			
			
		
		</table>
	</div>	
</div>

<div class="col-md-6">
	<h3 class="noTop">{{ trans('translate.bill_to') }}:</h3>
	
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<td>
					<h4>{{ $invoice->name }}</h4>
				</td>
			</tr>
			
			<tr>
				<td>
					{{ $invoice->email }}
				</td>
			</tr>	
			<tr>
				<td>
					{{ $invoice->contact }}
				</td>
			</tr>		
			
			<tr>
				<td>
					{{ $invoice->address }} 
				</td>
			</tr>			
			
			
		</table>
	</div>	
</div>

<div class="table-responsive col-md-12">
	<table class="table">
		<thead>
			<tr>
				<th class="col-md-3">{{ trans('translate.status') }}</th>
				<th class="col-md-3 text-center">{{ trans('translate.start_date') }}</th>
				<th class="col-md-3 text-center">{{ trans('translate.expiry_date') }}</th>
				<th class="col-md-3 text-center">{{ trans('translate.created_at') }}</th>
			</tr>
		<thead>
		
		<tbody>
			<tr>
				<td>
					<span class="label label-{{ str_replace(' ', '-', $invoice->status) }}">
						{{ trans('translate.' . str_replace(' ', '_', $invoice->status)) }}
					</span>	
				</td>
				
				<td class="text-center">
					{{ $invoice->start_date }}
				</td>
				
				<td class="text-center">
					{{ $invoice->due_date }}
				</td>
				
				<td class="text-center">
					{{ $invoice->created_at }}
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="td-product">{{ trans('translate.product') }}</th>
				<th class="td-info text-center">{{ trans('translate.price') }}</th>
				<th class="td-info text-center">{{ trans('translate.quantity') }}</th>
				<!-- <th class="td-info text-center">{{ trans('translate.tax_rate') }}</th>
				<th class="td-info text-center">{{ trans('translate.discount') }}</th> -->
				<th class="td-info text-center">{{ trans('translate.amount') }}</th>
			</tr>	
		</thead>
		
		<tbody>
			<?php $subTotalItems 	= 0;?>
			<?php $taxItems 		= 0;?>
			<?php $discountItems	= 0;?>
			<?php $invoiceDiscount	= 0;?>
			
			@foreach ($products as $crt => $product)
				<tr>
					<td>
						{{ $crt + 1 }}
					</td>
					
					<td>
						{{ $product->name }}
					</td>	
					
					<td class="text-center">
						{{ Solso::currencyPosition($invoice->currency, $invoice->position, $product->price) }}
					</td>	

					<td class="text-center">
						{{ $product->quantity }}
					</td>					
					
					<!-- <td class="text-center">
						{{ $product->tax }} %
					</td>					
					
					<td class="text-center">
						@if ($product->discount == 0)
							-
						@else
							{{ Solso::currencyPosition($invoice->currency, $invoice->position, $product->discount_value) }}
						@endif
					</td> -->
					
					<td class="text-center">
						{{ Solso::currencyPosition($invoice->currency, $invoice->position, $product->amount) }}
					</td>					
				</tr>
				
				<!-- <tr>
					<td colspan="7">
						<h4>
							{{ trans('translate.description') }}
							REMARKS
						</h4>
						
						{{ $product->description }}
					</td>
				</tr> -->
				
				<?php $subTotalItems 	+= $product->quantity * $product->price;?>
				<?php $taxItems 		+= ($product->quantity * $product->price) * ($product->tax / 100);?>							
				<?php $discountItems 	+= $product->discount_value;?>				
			@endforeach
		</tbody>
		
		<tfoot>
			<tr>
				<td colspan="4">
											
				</td>
				
				<td colspan="3">
					<table class="table">
						<tr>
							<td class="col-md-6">
								{{ trans('translate.subtotal') }}
							</td>	

							<td class="col-md-6" id="subTotal">
								{{ Solso::currencyPosition($invoice->currency, $invoice->position, $subTotalItems) }}
							</td>
						</tr>

						<!-- <tr>
							<td class="col-md-6">
								{{ trans('translate.tax') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($invoice->currency, $invoice->position, $taxItems) }}
							</td>
						</tr> -->	
						<tr>
							<td class="col-md-6">Shipping Fee</td>
							<td class="col-md-6" id="shippingFee">{{$invoice->currency }}{{ $product->shipping_fee }}</td>
						</tr>						
						
						<!-- @if ($discountItems != 0)
						<tr>
							<td class="col-md-6">
								{{ trans('translate.products_discount') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($invoice->currency, $invoice->position, $discountItems) }}
							</td>
						</tr>	
						@endif	 -->					
						
						<!-- @if ($invoice->discount != 0)
						<tr>
							<td class="col-md-6">
								{{ trans('translate.invoice_discount') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($invoice->currency, $invoice->position, $invoice->discount) }}
							</td>
						</tr>	
						@endif -->
						
						<tr>
							<td class="col-md-6">
								<h4>{{ trans('translate.total') }}</h4>
							</td>	

							<td class="col-md-6">
								<h4 id="invoiceTotal">
									<!-- {{ Solso::currencyPosition($invoice->currency, $invoice->position, $invoice->amount) }} -->
									{{ $invoice->currency }}{{$product->amount + $product->shipping_fee}}
								</h4>
							</td>
						</tr>						
					</table>
				</td>	
			</tr>				
		</tfoot>
	</table>
</div>

<!-- <div class="col-md-12">
	<h3>{{ trans('translate.list_of_payments') }}</h3>
	
	<div class="table-responsive">
		<table class="table table-striped">			
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="col-md-4">{{ trans('translate.amount_paid') }}</th>
				<th class="col-md-4">{{ trans('translate.date') }}</th>
				<th class="col-md-4">{{ trans('translate.payment_method') }}</th>
			</tr>				
		</thead>

		<tbody>	
			@if ($invoicePayments)
				@foreach($invoicePayments as $crt => $p)
					<tr>
						<td>
							{{ $crt + 1 }}
						</td>				
				
						<td>
							{{ $p->payment_amount }} {{ $invoice->currency }}
						</td>

						<td>
							{{ $p->payment_date }}
						</td>						
				
						<td>
							{{ $p->name }}
						</td>				
					</tr>
				@endforeach
			@else
				<tr>
					<td colspan="4">
						{{ trans('translate.no_data_available') }}
					</td>				
				</tr>
			@endif
		</tbody>	
		</table>
	</div>
</div> -->

<div class="col-md-12 bottom20">
	<a href="{{ URL::to('export/invoice/' . $invoice->invoiceID . '/pdf') }}" class="btn solso-pdf">
		{{ trans('translate.export_pdf') }}
	</a>

	<a href="{{ URL::to('export/invoice/' . $invoice->invoiceID . '/excel') }}" class="btn solso-excel">
		{{ trans('translate.export_excel') }}
	</a>
</div>

<div class="col-md-12 bottom20">
	<table class="table table-striped">
		<tr>
					<td colspan="7">
						<h4>
							<!-- {{ trans('translate.description') }} -->
							REMARKS
						</h4>
						<hr>
						
						{{ $invoice->terms }}
					</td>
				</tr>

	</table>			
</div>
<?php
	var_dump($invoice);

 ?>
<script type="text/javascript">
		
</script>


				