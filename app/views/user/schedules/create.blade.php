{{ Form::open(array('url' => 'schedule', 'role' => 'form', 'class' => 'solsoForm')) }}

	<div class="form-group col-md-4">
		<label for="invoice">{{ trans('translate.invoice_number') }}</label>
		<select name="invoice" class="form-control required select2">
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($invoices as $v)
				<option value="{{ $v->id }}"> {{ $v->number }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('invoice', '<p class="error">:messages</p>');?>
	</div>		
	
	<div class="form-group col-md-4">
		<label for="end">{{ trans('translate.every') }} -> {{ trans('translate.from_every_month') }}</label>
		<select name="startDate" class="form-control required">
			<option selected value="">{{ trans('translate.choose') }}</option>
			
			@for($i=1; $i<32; $i++)
				<option value="{{ $i }}">{{ $i }}</option>
			@endfor
		</select>

		<?php echo $errors->first('startDate', '<p class="error">:messages</p>');?>
	</div>	

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_saved') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}