{{ Form::open(array('url' => 'schedule/' . $schedule->scheduleID, 'role' => 'form', 'method' => 'PUT', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}

	<div class="form-group col-md-4">
		<label for="end">{{ trans('translate.every') }} --- {{ trans('translate.from_every_month') }}</label>
		<select name="startDate" class="form-control required">
			<option selected value="{{ $schedule->start_date }}">{{ $schedule->start_date }}</option>
			<option value="">{{ trans('translate.choose') }}</option>
			
			@for($i=1; $i<32; $i++)
				<option value="{{ $i }}">{{ $i }}</option>
			@endfor
		</select>

		<?php echo $errors->first('startDate', '<p class="error">:messages</p>');?>
	</div>			
	<div class="clearfix"></div>

	<div class="form-group col-md-12">
		<input type="hidden" name="action" value="{{ $action }}">
		<button type="button" class="btn btn-success solsoSave"
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>	
	
{{ Form::close() }}									
