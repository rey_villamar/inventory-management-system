@section('content')

	<div class="col-md-12">
		@if ($invoices == 0)
			<div role="alert" class="alert alert-warning">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_invoices') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('invoice') }}">{{ trans('translate.invoices') }}</a>
			</div>
		@endif		
	
		<h1>
			<i class="fa fa-list"></i> {{ trans('translate.schedules') }}
		</h1>
		
		@if ($invoices != 0)
		<button type="button" class="btn btn-primary solsoShowModal"  
			data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('schedule/create') }}" data-modal-title="{{ trans('translate.create_new_schedule') }}">
				<i class="fa fa-plus"></i> {{ trans('translate.create_new_schedule') }}
			</button>
		@endif
	</div>	

	<div class="col-md-12 top40">
		<h3>{{ trans('translate.schedules') }}</h3>
		
		<div id="ajaxTable" class="table-responsive">
			@include('user.schedules.table')	
		</div>
	</div>

	@include('user.invoices.addons.email')
	
@stop