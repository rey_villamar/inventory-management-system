<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}">
	<thead>
		<tr>
			<th>{{ trans('translate.crt') }}.</th>
			<th class="col-md-1">{{ trans('translate.invoice') }}</th>
			<th>{{ trans('translate.client') }}</th>
			<th class="col-md-1 text-right">{{ trans('translate.amount') }}</th>
			<th class="col-md-2 text-center">{{ trans('translate.schedule') }}</th>
			<th class="xs-small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
		</tr>
	</thead>
	
	<tbody>
	@foreach ($schedules as $crt => $v)
		<tr>
			<td>
				{{ $crt+1 }}
			</td>
			
			<td>
				{{ $v->number }}
			</td>

			<td>
				{{ $v->name }}
			</td>	

			<td class="text-right">
				{{ Solso::currencyPosition($v->currency, $v->position, $v->amount) }}
			</td>					
			
			<td class="text-center">
				{{ trans('translate.every') }} {{ $v->scheduleDate }} {{ trans('translate.from_every_month') }}
			</td>			
			
			<td>		
				<button type="button" class="btn btn-default solsoConfirm" 
				data-toggle="modal" data-target="#solsoSendEmail" data-href="{{ URL::to('email/invoice/' . $v->id . '/pdf') }}">
					<i class="fa fa-envelope"></i>
				</button>
			</td>	
			
			<td>
				<button type="button" class="btn btn-info solsoShowModal" 
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('invoice/' . $v->id) }}" data-modal-title="{{ trans('translate.show_invoice') }}">
					<i class="fa fa-eye"></i> {{ trans('translate.show_invoice') }}
				</button>
			</td>				

			<td>		
				<button type="button" class="btn btn-primary solsoShowModal" 
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('schedule/' . $v->scheduleID . '/edit') }}" data-modal-title="{{ trans('translate.edit_schedule') }}">
					<i class="fa fa-edit"></i> {{ trans('translate.edit') }}
				</button>
			</td>
			<td>		
				<button type="button" class="btn btn-danger solsoConfirm" 
				data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('schedule/' . $v->scheduleID) }}">
					<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
				</button>
			</td>
		</tr>
		
	@endforeach
	
	</tbody>
</table>	