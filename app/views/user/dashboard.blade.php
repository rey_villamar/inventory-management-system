@section('content')
	
	<div class="col-md-12">
		<h1>
			<i class="fa fa-home"></i> {{ trans('translate.dashboard') }}
		</h1>
	</div>
	

	<div class="col-sm-12 col-md-6">
		<div class="widget widget-stats bg-purple">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-file-pdf-o fa-fw"></i></div>
			<div class="stats-title">{{ trans('translate.invoices') }}</div>
			<div class="stats-number">{{ $totalInvoices }}</div>
			<hr>
			<div class="stats-desc">{{ trans('translate.number_of_invoices') }}</div>
		</div> 
	</div> 		

	<div class="col-sm-12 col-md-6">
		<div class="widget widget-stats bg-grey">
			<div class="stats-icon stats-icon-lg"><i class="fa fa-money fa-fw"></i></div>
			<div class="stats-title">{{ trans('translate.amount') }}</div>
			<div class="stats-number">{{ $amount }}</div>
			<hr>
			<div class="stats-desc">{{ trans('translate.total_value_of_amounts') }}</div>
		</div> 
	</div> 		
	
	<div class="col-md-12">
		<h3>{{ trans('translate.last') }} 5 {{ trans('translate.estimates') }}</h3>
		
		@include('user.estimates.table')	
	</div>	
	
	<div class="col-md-12">
		<h3>{{ trans('translate.last') }} 5 {{ trans('translate.invoices') }}</h3>
		
		@include('user.invoices.table')		
	</div>	
	
@stop	 