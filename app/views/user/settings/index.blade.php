@section('content')

	<div class="col-md-12">
		<h1>
			<i class="fa fa-cogs"></i> {{ trans('translate.settings') }}
		</h1>
	</div>		
	
	<div class="col-md-12">
		<ul id="solsoTabs" class="nav nav-tabs" role="tablist">

			@if ( $userIsClient )
				<li class="active"><a href="#tabClient" role="tab" data-toggle="tab">{{ trans('translate.client') }}</a></li>
				<li><a href="#tabLanguages" role="tab" data-toggle="tab">{{ trans('translate.languages') }}</a></li>
			@else
				<li class="active"><a href="#tabLanguages" role="tab" data-toggle="tab">{{ trans('translate.languages') }}</a></li>	
			@endif
			
			<li><a href="#tabAccount" role="tab" data-toggle="tab">{{ trans('translate.account') }}</a></li>
			<li><a href="#tabPassword" role="tab" data-toggle="tab">{{ trans('translate.password') }}</a></li>
		</ul>
		
		<div class="row tab-content">
			
			@if ( $userIsClient )
				<div class="tab-pane active" id="tabClient">
					@include('user.settings.client')
				</div>		

				<div class="tab-pane" id="tabLanguages">
					@include('user.settings.language')
				</div>					
			@else
				<div class="tab-pane active" id="tabLanguages">
					@include('user.settings.language')
				</div>					
			@endif
			
			<div class="tab-pane" id="tabAccount">
				@include('user.settings.account')
			</div>	

			<div class="tab-pane" id="tabPassword">
				@include('user.settings.password')
			</div>	
		</div>		
	</div>				
		
@stop