<div id="solso-sidebar" class="list-group">
	<a href="{{ URL::to('dashboard') }}" class="list-group-item <?php if ( Request::segment(1) == 'dashboard' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-home"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.dashboard') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.reports') }}
			</p>
		</div>	  
	</a>
	
	<a href="{{ URL::to('estimate') }}" class="list-group-item <?php if ( Request::segment(1) == 'estimate' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-file"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.estimates') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>	

	<a href="{{ URL::to('invoice') }}" class="list-group-item <?php if ( Request::segment(1) == 'invoice' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-file-pdf-o"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.invoices') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.view') }}
			</p>
		</div>	  
	</a>	
	
	<a href="{{ URL::to('message') }}" class="list-group-item <?php if ( Request::segment(1) == 'message' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-weixin"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.messages') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.create') }}  | {{ trans('translate.edit') }}  | {{ trans('translate.delete') }} 
			</p>
		</div>	  
	</a>	

	<a href="{{ URL::to('setting') }}" class="list-group-item <?php if ( Request::segment(1) == 'setting' ) { ?> active <?php } ?>">
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-cogs"></i></span>
			<h4 class="list-group-item-heading">{{ trans('translate.settings') }}</h4>
			<p class="list-group-item-text">
				{{ trans('translate.personal_data') }}
			</p>
		</div>	  
	</a>	
</div>
