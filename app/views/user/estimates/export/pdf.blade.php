<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="dompdf.view" content="XYZ,0,0,1" />
	
	<link rel="stylesheet" type="text/css" href="{{ url('public/css/invoice.css') }}">
</head>
<body>
	<div id="invoice">
		<table>
			<tr>
				<td class="col-md-6">
					@if (isset($logo->name))
						<img src="{{ URL::to('public/upload/' . $logo->name) }}" width="{{ $logo->width }}" height="{{ $logo->height }}">
					@endif
				</td>
				
				<td class="col-md-6">
					<h1>{{ trans('translate.estimate') }}</h1>
					
					<table class="border">
						<tr>
							<th class="col-md-6">{{ trans('translate.estimate') }}</th>
							<th class="col-md-6">{{ trans('translate.due_date') }}</th>
						</tr>

						<tr>						
							<td class="text-center">{{ $estimate->estimate }} {{ $estimate->reference }}</td>
							<td class="text-center">{{ $estimate->due_date }}</td>
						</tr>
					</table>					
				</td>
			</tr>
		</table>
		
		<table>
			<tr>
				<td class="col-md-6">
					<h3 class="text-left">{{ trans('translate.bill_from') }}</h3>
					<h4>{{ $owner->name }}</h4>
					
					<p class="details">{{ $owner->city }}, {{ $owner->state }}, {{ $owner->country }}</p>
					<p class="details">{{ $owner->address }}, {{ $owner->zip}}</p>
					<p class="details">{{ $owner->contact }}</p>
					<p class="details">{{ $owner->phone }}</p>
					<p class="details">{{ $owner->bank }}</p>
					<p class="details">{{ $owner->bank_account }}</p>
				</td>

				<td class="col-md-6">			
					<h3 class="text-left">{{ trans('translate.bill_to') }}</h3>
					<h4>{{ $estimate->name }}</h4>
					
					<p class="details">{{ $estimate->city }}, {{ $estimate->state }}, {{ $estimate->country }}</p>
					<p class="details">{{ $estimate->address }}, {{ $estimate->zip}}</p>
					<p class="details">{{ $estimate->contact }}</p>
					<p class="details">{{ $estimate->phone }}</p>
					<p class="details">{{ $estimate->bank }}</p>
					<p class="details">{{ $estimate->bank_account }}</p>
				</td>
			</tr>
		</table>
		
		<table class="table border table-striped">
			<thead>
				<tr>
					<th class="td-crt">{{ trans('translate.crt') }}.</th>
					<th class="td-product">{{ trans('translate.product') }}</th>
					<th class="td-qty">{{ trans('translate.price') }}</th>
					<th class="td-qty">{{ trans('translate.quantity') }}</th>
					<th class="td-qty">{{ trans('translate.tax_rate') }}</th>
					<th class="td-qty">{{ trans('translate.discount') }}</th>
					<th class="td-qty">{{ trans('translate.amount') }}</th>
				</tr>	
			</thead>
			
			<tbody>
				<?php $subTotalItems 	= 0;?>
				<?php $taxItems 		= 0;?>
				<?php $discountItems	= 0;?>
				<?php $invoiceDiscount	= 0;?>
				
				@foreach ($products as $crt => $product)
					<tr>
						<td>
							{{ $crt + 1 }}
						</td>
						
						<td>
							{{ $product->name }}
						</td>	
						
						<td class="text-center">
							{{ Solso::currencyPosition($estimate->currency, $estimate->position, $product->price) }}
						</td>	

						<td class="text-center">
							{{ $product->quantity }}
						</td>					
						
						<td class="text-center">
							{{ $product->tax }} %
						</td>					
						
						<td class="text-center">
							@if ($product->discount == 0)
								-
							@else
								{{ Solso::currencyPosition($estimate->currency, $estimate->position, $product->discount_value) }}
							@endif
						</td>
						
						<td class="text-center">
							{{ Solso::currencyPosition($estimate->currency, $estimate->position, $product->amount) }}
						</td>					
					</tr>
					
					<tr>
						<td colspan="7">
							{{ $product->description }}
						</td>
					</tr>
					
					<?php $subTotalItems 	+= $product->quantity * $product->price;?>
					<?php $taxItems 		+= ($product->quantity * $product->price) * ($product->tax / 100);?>							
					<?php $discountItems 	+= $product->discount_value;?>				
				@endforeach
			</tbody>
			
			<tfoot>
				<tr>
					<td colspan="4">
												
					</td>
					
					<td colspan="3">
						<table class="table">
							<tr>
								<td class="col-md-6">
									{{ trans('translate.subtotal') }}
								</td>	

								<td class="col-md-6">
									{{ Solso::currencyPosition($estimate->currency, $estimate->position, $subTotalItems) }}
								</td>
							</tr>

							<tr>
								<td class="col-md-6">
									{{ trans('translate.tax') }}
								</td>	

								<td class="col-md-6">
									{{ Solso::currencyPosition($estimate->currency, $estimate->position, $taxItems) }}
								</td>
							</tr>						
							
							@if ($discountItems != 0)
							<tr>
								<td class="col-md-6">
									{{ trans('translate.products_discount') }}
								</td>	

								<td class="col-md-6">
									{{ Solso::currencyPosition($estimate->currency, $estimate->position, $discountItems) }}
								</td>
							</tr>	
							@endif						
							
							@if ($estimate->discount != 0)
							<tr>
								<td class="col-md-6">
									{{ trans('translate.invoice_discount') }}
								</td>	

								<td class="col-md-6">
									{{ Solso::currencyPosition($estimate->currency, $estimate->position, $estimate->discount) }}
								</td>
							</tr>	
							@endif
							
							<tr>
								<td class="col-md-6">
									<h4>{{ trans('translate.total') }}</h4>
								</td>	

								<td class="col-md-6">
									<h4>{{ Solso::currencyPosition($estimate->currency, $estimate->position, $estimate->amount) }}</h4>
								</td>
							</tr>						
						</table>
					</td>	
				</tr>				
			</tfoot>
		</table>				
	</div>
</body>
</html>