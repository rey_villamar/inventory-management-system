{{ Form::open(array('url' => 'estimate', 'role' => 'form', 'class' => 'solsoForm')) }}

	<div class="form-group col-md-12">
		<label for="client_id">{{ trans('translate.client') }}</label>
		<select name="client_id" class="form-control required solsoSelect2">
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($clients as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('client_id', '<p class="error">:messages</p>');?>
	</div>
	
	<div class="form-group col-md-3">
		<label for="estimate"> {{ trans('translate.estimate') }} </label>
		<input type="text" name="estimate" class="form-control" autocomplete="off" value="{{ Input::old('estimate') }}">

		<?php echo $errors->first('estimate', '<p class="error">:messages</p>');?>
	</div>		

	<div class="form-group col-md-3">
		<label for="reference"> {{ trans('translate.reference') }} </label>
		<input type="text" name="reference" class="form-control" autocomplete="off" value="{{ Input::old('reference') }}">

		<?php echo $errors->first('reference', '<p class="error">:messages</p>');?>
	</div>			

	<div class="form-group col-md-2">
		<div class="form-group">
			<label for="start_date">{{ trans('translate.start_date') }}</label>
			<input type="text" name="start_date" class="form-control datepicker required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd"	value="{{ isset($inputs['start_date']) ? $inputs['start_date'] : '' }}">

			<?php echo $errors->first('start_date', '<p class="error">:messages</p>');?>
		</div>
	</div>

	<div class="form-group col-md-2">
		<div class="form-group">
			<label for="due_date">{{ trans('translate.expiry_date') }}</label>
			<input type="text" name="due_date" class="form-control datepicker required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ isset($inputs['due_date']) ? $inputs['due_date'] : '' }}">

			<?php echo $errors->first('due_date', '<p class="error">:messages</p>');?>
		</div>
	</div>			
		
	<div class="form-group col-md-2">
		<label for="currency">{{ trans('translate.currency') }}</label>
		<select name="currency_id" class="form-control required solsoCurrencyEvent">
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($currencies as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach					
			
		</select>
		
		<?php echo $errors->first('currency_id', '<p class="error">:messages</p>');?>
	</div>		
	<div class="clearfix"></div>	
	
	
	<div class="table-responsive col-md-12">
		<table class="table">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="td-product">{{ trans('translate.product') }}</th>
				<th class="td-info">{{ trans('translate.price') }}</th>
				<th class="td-info">{{ trans('translate.quantity') }}</th>
				<th class="td-info">{{ trans('translate.tax_rate') }}</th>
				<th class="td-info">{{ trans('translate.discount') }}</th>
				<th class="td-info">{{ trans('translate.discount_type') }}</th>
				<th class="td-info">{{ trans('translate.subtotal') }}</th>
				<th class="delete-button">{{ trans('translate.action') }}</th>
			</tr>	
		</thead>
			
		<tbody class="solsoParent">	
			<tr class="solsoChild">
				<td class="crt">1</td>
				
				<td>
					<select name="products[]" class="form-control required solsoSelect2 solsoCloneSelect2">
						<option value="" selected>{{ trans('translate.choose') }}</option>
						
						@foreach ($products as $v)
							<option value="{{ $v->id }}"> {{ substr($v->name, 0, 100) }} {{ strlen($v->name) > 100 ? '...' : '' }} </option>
						@endforeach			
						
					</select>				
				</td>
				
				<td>
					<input type="text" name="price[]" class="form-control required solsoEvent" autocomplete="off">
				</td>

				<td>
					<input type="text" name="qty[]" class="form-control required solsoEvent" autocomplete="off">
				</td>				
				
				<td>
					<select name="taxes[]" class="form-control required solsoEvent">
						<option value="" selected>{{ trans('translate.choose') }}</option>
						
						@foreach ($taxes as $v)
							<option value="{{ $v->value }}"> {{ $v->value }} %</option>
						@endforeach			
						
					</select>					
				</td>
				
				<td>
					<input type="text" name="discount[]" class="form-control" autocomplete="off">
				</td>	
			
				<td>
					<select name="discountType[]" class="form-control solsoEvent">
						<option value="" selected>{{ trans('translate.choose') }}</option>
						<option value="1">{{ trans('translate.amount') }}</option>
						<option value="2">%</option>
					</select>
				</td>

				<td>
					<h4 class="pull-right">
						<span class="solsoSubTotal">0.00</span>
					</h4>	
				</td>
				
				<td>
					<button type="button" class="btn btn-danger disabled removeClone">
						<i class="fa fa-minus"></i>
					</button>
				</td>					
			</tr>
		</tbody>
		
		<tfoot>
			<tr>
				<td colspan="5">
					<div class="form-inline">
						<label for="end" class="show">{{ trans('translate.estimate_discount') }}</label>
						<input type="text" name="invoiceDiscount" class="form-control" autocomplete="off">
						
						<select name="invoiceDiscountType" class="form-control solsoEvent">
							<option value="" selected>{{ trans('translate.choose') }}</option>
							<option value="1">{{ trans('translate.amount') }}</option>
							<option value="2">%</option>
						</select>							
					</div>						
				</td>
				
				<td colspan="2">
					<h3 class="pull-right top10">{{ trans('translate.total') }}</h3>
				</td>
				
				<td colspan="2">
					<h3 class="top10">
						<span class="solsoTotal">0.00</span>
						<span class="solsoCurrency"></span>
					</h3>
				</td>
			</tr>
		</tfoot>
		</table>
	</div>		
	
	<div class="form-group top20 text-center">
		<button type="button" class="btn btn-primary btn-lg" id="createClone">
			<i class="fa fa-plus"></i> {{ trans('translate.add_new_product') }}
		</button>
	</div>	

	<div class="form-group col-md-12">
		<label for="description"> {{ trans('translate.description') }} </label>
		<textarea name="description" class="form-control" rows="7" autocomplete="off"></textarea>
	</div>	

	<div class="form-group col-md-12">
		<label for="terms"> {{ trans('translate.terms_conditions') }} </label>
		<textarea name="terms" class="form-control" rows="7" autocomplete="off"></textarea>
	</div>	
		
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_saved') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
		
{{ Form::close() }}