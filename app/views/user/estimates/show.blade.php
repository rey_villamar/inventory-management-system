<div class="col-md-12">
	<h1>{{ trans('translate.estimate') }} {{ $estimate->estimate }} {{ trans('translate.reference') }}  {{ $estimate->reference }}</h1>
	<hr>
</div>

<div class="col-md-6">
	<h3 class="noTop">{{ trans('translate.bill_from') }}:</h3>
	
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<td>
					<h4>{{ $owner->name }}</h4>
				</td>
			</tr>
			
			<tr>
				<td>
					{{ $owner->country }} - {{ $owner->state }} - {{ $owner->city }}
				</td>
			</tr>			
			
			<tr>
				<td>
					{{ $owner->address }} - {{ $owner->zip }}
				</td>
			</tr>			
			
			<tr>
				<td>
					{{ trans('translate.bank') }}: {{ $owner->bank }}
				</td>
			</tr>			
			
			<tr>
				<td>
					{{ trans('translate.bank_account') }}: {{ $owner->bank_account }}
				</td>
			</tr>
		</table>
	</div>	
</div>

<div class="col-md-6">
	<h3 class="noTop">{{ trans('translate.bill_to') }}:</h3>
	
	<div class="table-responsive">
		<table class="table table-striped">
			<tr>
				<td>
					<h4>{{ $estimate->name }}</h4>
				</td>
			</tr>
			
			<tr>
				<td>
					{{ $estimate->country }} - {{ $estimate->state }} - {{ $estimate->city }}
				</td>
			</tr>			
			
			<tr>
				<td>
					{{ $estimate->address }} - {{ $estimate->zip }}
				</td>
			</tr>			
			
			<tr>
				<td>
					{{ trans('translate.bank') }}: {{ $estimate->bank }}
				</td>
			</tr>			
			
			<tr>
				<td>
					{{ trans('translate.bank_account') }}: {{ $estimate->bank_account }}
				</td>
			</tr>
		</table>
	</div>	
</div>

<div class="table-responsive col-md-12">
	<table class="table">
		<thead>
			<tr>
				<th class="col-md-3">{{ trans('translate.status') }}</th>
				<th class="col-md-3 text-center">{{ trans('translate.start_date') }}</th>
				<th class="col-md-3 text-center">{{ trans('translate.expiry_date') }}</th>
				<th class="col-md-3 text-center">{{ trans('translate.created_at') }}</th>
			</tr>
		<thead>
		
		<tbody>
			<tr>
				<td>
					@if ($estimate->status != 2)
						<label class="label-overdue">{{ trans('translate.unapproved') }}</label>
					@endif
					
					@if ($estimate->status == 2)
						<label class="label-paid">{{ trans('translate.approved') }}</label>
					@endif		
				</td>
				
				<td class="text-center">
					{{ $estimate->start_date }}
				</td>
				
				<td class="text-center">
					{{ $estimate->due_date }}
				</td>
				
				<td class="text-center">
					{{ $estimate->created_at }}
				</td>
			</tr>
		</tbody>
	</table>
	
	<table class="table table-striped">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="td-product">{{ trans('translate.product') }}</th>
				<th class="td-info text-center">{{ trans('translate.price') }}</th>
				<th class="td-info text-center">{{ trans('translate.quantity') }}</th>
				<th class="td-info text-center">{{ trans('translate.tax_rate') }}</th>
				<th class="td-info text-center">{{ trans('translate.discount') }}</th>
				<th class="td-info text-center">{{ trans('translate.amount') }}</th>
			</tr>	
		</thead>
		
		<tbody>
			<?php $subTotalItems 	= 0;?>
			<?php $taxItems 		= 0;?>
			<?php $discountItems	= 0;?>
			<?php $invoiceDiscount	= 0;?>
			
			@foreach ($products as $crt => $product)
				<tr>
					<td>
						{{ $crt + 1 }}
					</td>
					
					<td>
						{{ $product->name }}
					</td>	
					
					<td class="text-center">
						{{ Solso::currencyPosition($estimate->currency, $estimate->position, $product->price) }}
					</td>	

					<td class="text-center">
						{{ $product->quantity }}
					</td>					
					
					<td class="text-center">
						{{ $product->tax }} %
					</td>					
					
					<td class="text-center">
						@if ($product->discount == 0)
							-
						@else
							{{ Solso::currencyPosition($estimate->currency, $estimate->position, $product->discount_value) }}
						@endif
					</td>
					
					<td class="text-center">
						{{ Solso::currencyPosition($estimate->currency, $estimate->position, $product->amount) }}
					</td>					
				</tr>
				
				<tr>
					<td colspan="7">
						<h4>{{ trans('translate.description') }}</h4>
						
						{{ $product->description }}
					</td>
				</tr>
				
				<?php $subTotalItems 	+= $product->quantity * $product->price;?>
				<?php $taxItems 		+= ($product->quantity * $product->price) * ($product->tax / 100);?>							
				<?php $discountItems 	+= $product->discount_value;?>				
			@endforeach
		</tbody>
		
		<tfoot>
			<tr>
				<td colspan="4">
											
				</td>
				
				<td colspan="3">
					<table class="table">
						<tr>
							<td class="col-md-6">
								{{ trans('translate.subtotal') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($estimate->currency, $estimate->position, $subTotalItems) }}
							</td>
						</tr>

						<tr>
							<td class="col-md-6">
								{{ trans('translate.tax') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($estimate->currency, $estimate->position, $taxItems) }}
							</td>
						</tr>						
						
						@if ($discountItems != 0)
						<tr>
							<td class="col-md-6">
								{{ trans('translate.products_discount') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($estimate->currency, $estimate->position, $discountItems) }}
							</td>
						</tr>	
						@endif						
						
						@if ($estimate->discount != 0)
						<tr>
							<td class="col-md-6">
								{{ trans('translate.estimate_discount') }}
							</td>	

							<td class="col-md-6">
								{{ Solso::currencyPosition($estimate->currency, $estimate->position, $estimate->discount) }}
							</td>
						</tr>	
						@endif
						
						<tr>
							<td class="col-md-6">
								<h4>{{ trans('translate.total') }}</h4>
							</td>	

							<td class="col-md-6">
								<h4>{{ Solso::currencyPosition($estimate->currency, $estimate->position, $estimate->amount) }}</h4>
							</td>
						</tr>						
					</table>
				</td>	
			</tr>				
		</tfoot>
	</table>
</div>

<div class="col-md-12">
	@if ( $userIsClient )
		@if ($estimate->status != 2)

			{{ Form::open(array('url' => 'estimate/' . $estimate->estimateID . '/approve/', 'role' => 'form', 'method' => 'POST', 'class' => 'solsoForm' )) }}
				<button type="button" class="btn btn-success solsoSave bottom20"
				data-message-title="Update notification" data-message-error="Validation error messages" data-message-success="Data was updated" data-message-warning="">
					<i class="fa fa-check"></i> {{ trans('translate.approve') }}
				</button>
			{{ Form::close() }}	
			
		@endif
	@endif
</div>