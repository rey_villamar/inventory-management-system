{{ Form::open(array('url' => 'estimate/' . Request::segment(2), 'role' => 'form', 'method' => 'PUT', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}
		
	<div class="form-group col-md-12">
		<label for="client_id">{{ trans('translate.client') }}</label>
		<select name="client_id" class="form-control required select2">
			<option selected value="{{ Input::old('client_id') ? Input::old('client_id') : $estimate->client_id }}"> {{ Input::old('client_id') ? Input::old('client_id') : $estimate->name }} </option>	
			<option value="">{{ trans('translate.choose') }}</option>
			
			@foreach ($clients as $c)
				<option value="{{ $c->id }}"> {{ $c->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('client_id', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-3">
		<label for="estimate">{{ trans('translate.estimate') }}</label>
		<input type="text" name="estimate" class="form-control required" autocomplete="off" value="{{ Input::old('estimate') ? Input::old('estimate') : $estimate->estimate }}">
		
		<?php echo $errors->first('estimate', '<p class="error">:messages</p>');?>
	</div>
	
	<div class="form-group col-md-3">
		<label for="reference">{{ trans('translate.reference') }}</label>
		<input type="text" name="reference" class="form-control required" autocomplete="off" value="{{ Input::old('reference') ? Input::old('reference') : $estimate->reference }}">
		
		<?php echo $errors->first('reference', '<p class="error">:messages</p>');?>
	</div>

	<div class="form-group col-md-2">
		<label for="start_date">{{ trans('translate.start_date') }}</label>
		<input type="text" name="start_date" class="form-control datepicker required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd"	value="{{ Input::old('start_date') ? Input::old('start_date') : $estimate->start_date }}">

		<?php echo $errors->first('start_date', '<p class="error">:messages</p>');?>
	</div>

	<div class="form-group col-md-2">
		<label for="due_date">{{ trans('translate.expiry_date') }}</label>
		<input type="text" name="due_date" class="form-control datepicker required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ Input::old('due_date') ? Input::old('due_date') : $estimate->due_date }}">

		<?php echo $errors->first('due_date', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-2">
		<label for="currency">{{ trans('translate.currency') }}</label>
		<select name="currency_id" class="form-control required solsoCurrencyEvent">
			<option selected value="{{ Input::old('currency_id') ? Input::old('currency_id') : $estimate->currency_id }}"> {{ Input::old('currency_id') ? Input::old('currency_id') : $estimate->currency }} </option>	
			<option value="">{{ trans('translate.choose') }}</option>
			
			@foreach ($currencies as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach					
			
		</select>
		
		<?php echo $errors->first('currency_id', '<p class="error">:messages</p>');?>
	</div>		
	<div class="clearfix"></div>	
	
	
<div class="table-responsive col-md-12">
		<table class="table">
		<thead>
			<tr>
				<th>{{ trans('translate.crt') }}.</th>
				<th class="td-product">{{ trans('translate.product') }}</th>
				<th class="td-info">{{ trans('translate.price') }}</th>
				<th class="td-info">{{ trans('translate.quantity') }}</th>
				<th class="td-info">{{ trans('translate.tax_rate') }}</th>
				<th class="td-info">{{ trans('translate.discount') }}</th>
				<th class="td-info">{{ trans('translate.discount_type') }}</th>
				<th class="td-info">{{ trans('translate.subtotal') }}</th>
				<th class="delete-button">{{ trans('translate.action') }}</th>
			</tr>	
		</thead>
			
		<tbody class="solsoParent">	
				@foreach ($estimateProducts as $crt => $p)
				
				<tr {{ $crt == 0 ? 'class="solsoChild"' : '' }}>
					<td class="crt">
						{{ $crt + 1 }}
					</td>
						
					<td>
						<select name="products[]" class="form-control required solsoSelect2 solsoCloneSelect2">
							<option selected value="{{ Input::old('products[]') ? Input::old('products[]') : $p->product_id }}">
								{{ Input::old('products[]') ? Input::old('products[]') :  substr($p->name, 0, 100) }} {{ strlen($p->name) > 100 ? '..' : '' }}
							</option>
							<option value="">{{ trans('translate.choose') }}</option>
							
							@foreach ($products as $v)
								<option value="{{ $v->id }}"> {{ substr($v->name, 0, 100) }} {{ strlen($v->name) > 100 ? '..' : '' }} </option>
							@endforeach			

						</select>				
					</td>
					
					<td>
						<input type="text" name="price[]" class="form-control required solsoEvent" autocomplete="off" value="{{ $p->price }}">
					</td>

					<td>
						<input type="text" name="qty[]" class="form-control required solsoEvent" autocomplete="off" value="{{ $p->quantity }}">
					</td>
					
					<td>
						<select name="taxes[]" class="form-control required solsoEvent">
							<option selected value="{{ Input::old('taxes[]') ? Input::old('taxes[]') : $p->tax }}"> {{ Input::old('taxes[]') ? Input::old('taxes[]') : $p->tax }} %</option>
							<option value="">{{ trans('translate.choose') }}</option>
							
							@foreach ($taxes as $v)
								<option value="{{ $v->value }}"> {{ $v->value }} %</option>
							@endforeach			
							
						</select>					
					</td>
					
					<td class="no-right-padding">
						<input type="text" name="discount[]" class="form-control" autocomplete="off" value="{{ $p->discount }}">
					</td>	
				
					<td class="no-left-padding">
						<select name="discountType[]" class="form-control solsoEvent">
						
							@if ($p->discount_type == 0)
								<option value="">{{ trans('translate.choose') }}</option>
							@else
							<option selected value="{{ Input::old('discountType[]') ? Input::old('discountType[]') : $p->discount_type }}">
								{{ Input::old('discountType[]') ? Input::old('discountType[]') : $p->discount_type == 1 ? trans('translate.amount') : '%' }} 
							</option>
							@endif	
							
							<option value="1">{{ trans('translate.amount') }}</option>
							<option value="2">%</option>
						</select>
					</td>

					<td>
						<h4 class="pull-right">
							<span class="solsoSubTotal">{{ $p->amount }}</span>
							<span class="solsoCurrency">{{ $estimate->currency }}</span>
						</h4>	
					</td>						
					
					<td>		
						<button type="button" class="btn btn-danger removeClone">
							<i class="fa fa-minus"></i>
						</button>		
					</td>						
				</tr>
					
				@endforeach			
			
			</tbody>
		
			<tfoot>
				<tr>
					<td colspan="5">
						<div class="col-md-12 form-inline">
							<label for="end" class="show">{{ trans('translate.estimate_discount') }}</label>
							<input type="text" name="invoiceDiscount" class="form-control" autocomplete="off" value="{{ $estimate->discount }}">
							
							<select name="invoiceDiscountType" class="form-control solsoEvent">
							
								@if ($estimate->type == 0)
									<option value="">choose</option>
								@else							
								<option selected value="{{ Input::old('invoiceDiscountType[]') ? Input::old('invoiceDiscountType[]') : $estimate->type }}">
									{{ Input::old('invoiceDiscountType[]') ? Input::old('invoiceDiscountType[]') : $estimate->type == 1 ? trans('translate.amount') : '%' }} 
								</option>
								@endif
								
								<option value="1">{{ trans('translate.amount') }}</option>
								<option value="2">%</option>
							</select>							
						</div>						
					</td>
					
					<td colspan="2">
						<h3 class="pull-right top10">{{ trans('translate.total') }}</h3>
					</td>
					
					<td colspan="2">
						<h3 class="top10">
							<span class="solsoTotal">{{ $estimate->amount }}</span>
							<span class="solsoCurrency">{{ $estimate->currency }}</span>
						</h3>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>		
	
	<div class="form-group top20 text-center">
		<button type="button" class="btn btn-primary btn-lg" id="createClone">
			<i class="fa fa-plus"></i> {{ trans('translate.add_new_product') }}
		</button>
	</div>	
	
	<div class="form-group col-md-12">
		<label for="description"> {{ trans('translate.description') }} </label>
		<textarea name="description" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['description']) ? $inputs['description'] : $estimate->description }}</textarea>
	</div>	

	<div class="form-group col-md-12">
		<label for="terms"> {{ trans('translate.terms_conditions') }} </label>
		<textarea name="terms" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['terms']) ? $inputs['terms'] : $estimate->terms }}</textarea>
	</div>	
	
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.date_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}	
