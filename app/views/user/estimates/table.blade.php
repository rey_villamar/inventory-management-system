<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}">
	<thead>
		<tr>
			<th>{{ trans('translate.crt') }}.</th>
			<th class="col-md-1">{{ trans('translate.estimate') }}</th>
			<th>
				@if ( ! $userIsClient )
					{{ trans('translate.client') }}
				@else
					{{ trans('translate.from') }}
				@endif
			</th>
			<th class="col-md-1 text-right">{{ trans('translate.amount') }}</th>
			<th class="col-md-1 text-center">{{ trans('translate.expiry_date') }}</th>
			<th class="small">{{ trans('translate.status') }}</th>
			<th class="small">{{ trans('translate.state') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
			
			@if ( ! $userIsClient )
				<th class="small">{{ trans('translate.action') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
				<th class="small">{{ trans('translate.action') }}</th>
			@endif
			
			@if ( $userIsClient )
				<th class="small">{{ trans('translate.action') }}</th>
			@endif
		</tr>
	</thead>
	
	<tbody>
	@foreach ($estimates as $crt => $v)
		<tr>
			<td>
				{{ $crt+1 }}
			</td>

			<td>
				{{ $v->estimate }}
			</td>

			<td>
				@if ( ! $userIsClient )
					{{ $v->name }}
				@else
					{{ $owner->name }}
				@endif	
			</td>	

			<td class="text-right">
				{{ Solso::currencyPosition($v->currency, $v->position, $v->amount) }}
			</td>	
			
			<td class="text-center">
				{{ $v->due_date }}
			</td>	

			<td>
				@if ($v->status != 2)
					<label class="label label-overdue">{{ trans('translate.unapproved') }}</label>
				@endif
				
				@if ($v->status == 2)
					<label class="label label-paid">{{ trans('translate.approved') }}</label>
				@endif
			</td>						
			
			<td>
				@if ($v->status == 0)
					<label class="label label-overdue">{{ trans('translate.unseen') }}</label>
				@else
					<label class="label label-paid">{{ trans('translate.seen') }}</label>
				@endif
			</td>			
			
			@if ( ! $userIsClient )
				<td>		
					<button type="button" class="btn btn-default solsoConfirm" 
					data-toggle="modal" data-target="#solsoSendEmail" data-href="{{ URL::to('email/estimate/' . $v->id . '/pdf') }}">
						<i class="fa fa-envelope"></i>
					</button>
				</td>			
			@endif	
			
			<td>
				<div class="dropdown">
					<button class="btn solso-pdf dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
						{{ trans('translate.quick_actions') }}
						<span class="caret"></span>
					</button>
				
					<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
						<li role="presentation">
							<a role="menuitem" tabindex="-1" href="{{ URL::to('export/estimate/' . $v->id . '/pdf') }}">
								{{ trans('translate.export_pdf') }}
							</a>
						</li>
						
						<li role="presentation">
							<a role="menuitem" tabindex="-1" href="{{ URL::to('export/estimate/' . $v->id . '/excel') }}">
								{{ trans('translate.export_excel') }}
							</a>
						</li>
					</ul>
				</div>
			</td>			
			
			@if ( ! $userIsClient )
				<td>
					<button type="button" class="btn btn-info solsoShowModal" 
					data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('estimate/' . $v->id) }}" data-modal-title="{{ trans('translate.show_estimate') }}">
						<i class="fa fa-eye"></i> {{ trans('translate.show') }}
					</button>
				</td>		

				<td>		
					<button type="button" class="btn btn-primary solsoShowModal" 
					data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('estimate/' . $v->id . '/edit') }}" data-modal-title="{{ trans('translate.show_estimate') }}">
						<i class="fa fa-edit"></i> {{ trans('translate.edit') }}
					</button>
				</td>

				<td>		
					<button type="button" class="btn btn-danger solsoConfirm" 
					data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('estimate/' . $v->id) }}">
						<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
					</button>		
				</td>
			@endif
			
			@if ( $userIsClient )
				<td>
					<button type="button" class="btn btn-info solsoShowModal" 
					data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('estimate/' . $v->id) }}" data-modal-title="{{ trans('translate.show_estimate') }}">
						<i class="fa fa-eye"></i> {{ trans('translate.show') }}
					</button>			
				</td>
			@endif	
		</tr>
		
	@endforeach
	
	</tbody>
</table>	