@section('content')

	<div class="col-md-12 ">
		<h1><i class="fa fa-line-chart"></i> {{ trans('translate.reports') }}</h1>
	</div>	

	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-bar-chart"></i> {{ trans('translate.clients_and_products') }} -> {{ trans('translate.this_month') }}
				</h2>
			</div>
			
			<div class="panel-body">
				<canvas id="reportClientsProducts" height="120" class="col-md-12"></canvas>
			</div>
		</div>	
	</div>	

	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-bar-chart"></i> {{ trans('translate.clients_and_products') }} -> {{ trans('translate.this_year') }}
				</h2>
			</div>
			
			<div class="panel-body">
				<canvas id="reportClientsProducts2" height="120" class="col-md-12"></canvas>
			</div>
		</div>	
	</div>
	<div class="clearfix"></div>
	
	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-bar-chart"></i> {{ trans('translate.estimates_and_invoices') }} -> {{ trans('translate.this_month') }}
				</h2>
			</div>
			
			<div class="panel-body">
				<canvas id="reporEstimatesInvoices" height="120" class="col-md-12"></canvas>
			</div>
		</div>	
	</div>	

	<div class="col-xs-12 col-sm-12 col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h2 class="panel-title">
					<i class="fa fa-bar-chart"></i> {{ trans('translate.estimates_and_invoices') }} -> {{ trans('translate.this_year') }}
				</h2>
			</div>
			
			<div class="panel-body">
				<canvas id="reporEstimatesInvoices2" height="120" class="col-md-12"></canvas>
			</div>
		</div>	
	</div>

	<div class = "container">
		<table class="table table-bordered">
		    <thead>
		      <tr>
		        <th>Month</th>
		        <th>Total Products</th>
		        <th>Total Sales</th>
		      </tr>
		    </thead>
		    <tbody>
		      <?php foreach ($reportMonthlySales as $key => $value): ?>
					<tr>
						<td><?php echo $value->monthname; ?></td>
						<td><?php echo count($value->total_amount); ?></td>
						<td><?php echo $value->total_amount; ?></td>
					</tr>
				<?php endforeach;?>
		     
		    </tbody>
		</table>
	</div>
@stop