@section('content')

	<div class="col-md-12">
		@if ($projects == 0)
			<div role="alert" class="alert alert-warning">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_projects') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('project') }}">{{ trans('translate.projects') }}</a>
			</div>
		@endif
		
		<h1>
			<i class="fa fa-list"></i> {{ trans('translate.tasks') }}
		</h1>
	
		@if ($projects != 0)
			<button type="button" class="btn btn-primary solsoShowModal"
			data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('task/create') }}" data-modal-title="{{ trans('translate.create_new_task') }}">
				<i class="fa fa-plus"></i> {{ trans('translate.create_new_task') }}
			</button>
		@endif
	</div>	

	<div class="col-md-12 top40">
		<h3>{{ trans('translate.tasks') }}</h3>

		<div id="ajaxTable" class="table-responsive">
			@include('user.tasks.table')	
		</div>	
	</div>
	
@stop