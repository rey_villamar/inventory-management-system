<div class="col-md-12">
	<h2>
		{{ $task->name }}
	</h2>
	<hr>
	
	<div class="table-responsive">
		<table class="table table-striped">
			<tbody>
				<tr>
					<td class="col-md-2">{{ trans('translate.project') }}</th>
					<td class="col-md-10">{{ $task->project }}</td>
				</tr>
				
				<tr>
					<td>{{ trans('translate.start_date') }}</td>
					<td>{{ $task->start_date }}</td>
				</tr>

				<tr>
					<td>{{ trans('translate.due_date') }}</td>
					<td>{{ $task->due_date }}</td>
				</tr>				
				
				<tr>
					<td>{{ trans('translate.estimated_hours') }}</td>
					<td>{{ $task->hours }}</td>
				</tr>	

				<tr>
					<td>{{ trans('translate.completed_hours') }}</td>
					<td>{{ $task->completed }}</td>
				</tr>

				<tr>
					<td>{{ trans('translate.progress') }}</td>
					<td>{{ $task->progress }} % </td>
				</tr>
				
				<tr>
					<td colspan="2">
						<h4>{{ trans('translate.description') }}</h4>
						
						{{ $task->description }}
					</td>
				</tr>
			<tbody>
		</table>
	</div>
</div>