{{ Form::open(array('url' => 'task', 'role' => 'form')) }}

	<div class="form-group col-md-6">
		<label for="project_id">{{ trans('translate.projects') }}</label>
		<select name="project_id" class="form-control required select2">
			<option value="" selected>{{ trans('translate.choose') }}</option>
			
			@foreach ($projects as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('project_id', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-6">
		<label for="name">{{ trans('translate.task_name') }}</label>
		<input type="text" name="name" class="form-control required" autocomplete="off" value="{{ Input::old('name') }}">

		<?php echo $errors->first('name', '<p class="error">:messages</p>');?>
	</div>	
	<div class="clearfix"></div>
		
	<div class="form-group col-md-4">
		<div class="form-group">
			<label for="start_date">{{ trans('translate.start_date') }}</label>
			<input type="text" name="start_date" class="form-control datepicker required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ isset($inputs['start_date']) ? $inputs['start_date'] : '' }}">

			<?php echo $errors->first('start_date', '<p class="error">:messages</p>');?>
		</div>
	</div>

	<div class="form-group col-md-4">
		<div class="form-group">
			<label for="due_date">{{ trans('translate.end_date_deadline') }}</label>
			<input type="text" name="due_date" class="form-control datepicker required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ isset($inputs['due_date']) ? $inputs['due_date'] : '' }}">

			<?php echo $errors->first('due_date', '<p class="error">:messages</p>');?>
		</div>
	</div>		

	<div class="form-group col-md-4">
		<div class="form-group">
			<label for="hours">{{ trans('translate.estimated_hours') }}</label>
			<input type="text" name="hours" class="form-control required" autocomplete="off" value="{{ isset($inputs['hours']) ? $inputs['hours'] : '' }}">

			<?php echo $errors->first('hours', '<p class="error">:messages</p>');?>
		</div>
	</div>	
	<div class="clearfix"></div>	
	
	<div class="col-md-12">
		<div class="form-group">
			<label for="description">{{ trans('translate.description') }}</label>
			<textarea name="description" class="form-control solsoEditor" rows="7" autocomplete="off">{{ isset($inputs['description']) ? $inputs['description'] : '' }}</textarea>
		</div>	
	</div>	

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_saved') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}