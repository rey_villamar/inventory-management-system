<table class="table solsoTable" data-alert="{{ isset($alert) ? $alert : false }}">
	<thead>
		<tr>
			<th>{{ trans('translate.crt') }}.</th>
			<th>{{ trans('translate.task') }}</th>
			<th>{{ trans('translate.project') }}</th>
			<th class="col-md-1">{{ trans('translate.estimated') }}</th>
			<th class="col-md-1">{{ trans('translate.completed') }}</th>
			<th class="small">{{ trans('translate.progress') }}</th>
			<th class="small">{{ trans('translate.start_date') }}</th>
			<th class="small">{{ trans('translate.due_date') }}</th>
			
			@if (Auth::user()->role_id == 1)
				<th class="small">{{ trans('translate.action') }}</th>
			@endif
			
			<th class="small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
			<th class="small">{{ trans('translate.action') }}</th>
		</tr>
	</thead>
	
	<tbody>
	
	@foreach ($tasks as $crt => $v)
	
		<tr>
			<td>
				{{ $crt+1 }}
			</td>

			<td>
				{{ $v->name }}
			</td>
			
			<td>
				{{ $v->project }}
			</td>					
		
			<td>
				{{ $v->hours }} {{ trans('translate.hours') }}
			</td>			
			
			<td>
				@if (Auth::user()->role_id == 1)
					<form>
						<div class="input-group">
							
							<input type="text" name="completed" class="form-control required xs-small" autocomplete="off" value="{{ $v->completed }}" data-parsley-errors-container=".completedError">
							<span class="input-group-addon" id="sizing-addon1">{{ trans('translate.hours') }}</span>
							<span class="input-group-btn">
								<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
								<button type="submit" class="btn btn-success solsoAjax" 
									data-href="{{ URL::to('task/' . $v->id . '/completed') }}" data-method="post"  
									data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
									<i class="fa fa-save"></i> {{ trans('translate.save') }}
								</button>
							</span>	
						</div>
						
						<div class="completedError"></div>
					</form>
				@else			
					{{ $v->completed }} {{ trans('translate.hours') }}
				@endif
			</td>			
			
			<td>
				@if (Auth::user()->role_id == 1)
					<form>
						<div class="input-group">
							
							<input type="text" name="progress" class="form-control required xs-small" autocomplete="off" value="{{ $v->progress }}" data-parsley-errors-container=".progressError">
							<span class="input-group-addon" id="sizing-addon1">%</span>
							<span class="input-group-btn">
								<input type="hidden" name="solsoStatus" value="{{ isset($alert) ? $alert : 'false'; }}">
								<button type="submit" class="btn btn-success solsoAjax" 
									data-href="{{ URL::to('task/' . $v->id . '/progress') }}" data-method="post"  
									data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
									<i class="fa fa-save"></i> {{ trans('translate.save') }}
								</button>
							</span>	
						</div>
						
						<div class="progressError"></div>
					</form>
				@else			
					{{ $v->progress }} %
				@endif
			</td>					
							
			<td>
				{{ $v->start_date }}
			</td>	

			<td>
				{{ $v->due_date }}
			</td>		
			
			@if (Auth::user()->role_id == 1)
				<td>
					<div class="dropdown">
						<button class="btn solso-pdf dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
							Quick Actions
							<span class="caret"></span>
						</button>
					
						<ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu1">
							<li role="presentation">
								<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('task/' . $v->id . '/duedate') }}" data-modal-title="{{ trans('translate.change_deadline') }}">
									{{ trans('translate.change_deadline') }}
								</a>							
							</li>	

							<li role="presentation">
								<a role="menuitem" tabindex="-1" href="#" class="solsoShowModal" 
								data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('task/' . $v->id . '/hours') }}" data-modal-title="{{ trans('translate.change_estimated_hours') }}">
									{{ trans('translate.change_estimated_hours') }}
								</a>							
							</li>
						</ul>
					</div>
				</td>			
			@endif
			
			<td>
				<button type="button" class="btn btn-info solsoShowModal" 
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('task/' . $v->id) }}" data-modal-title="{{ trans('translate.show_task') }}">
					<i class="fa fa-eye"></i> {{ trans('translate.show') }}
				</button>
			</td>		

			<td>		
				<button type="button" class="btn btn-primary solsoShowModal" 
				data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('task/' . $v->id . '/edit') }}" data-modal-title="{{ trans('translate.edit_task') }}">
					<i class="fa fa-edit"></i> {{ trans('translate.edit') }}
				</button>
			</td>

			<td>		
				<button type="button" class="btn btn-danger solsoConfirm" 
				data-toggle="modal" data-target="#solsoDeleteModal" data-href="{{ URL::to('task/' . $v->id) }}">
					<i class="fa fa-trash"></i> {{ trans('translate.delete') }}
				</button>		
			</td>
		</tr>
		
	@endforeach
	
	</tbody>
</table>