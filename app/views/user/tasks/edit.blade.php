{{ Form::open(array('url' => 'task/' . Request::segment(2), 'role' => 'form', 'method' => 'PUT')) }}

	<div class="form-group col-md-6">
		<label for="project_id">{{ trans('translate.projects') }}</label>
		<select name="project_id" class="form-control required select2">
			<option selected value="{{ Input::old('project_id') ? Input::old('project_id') : $task->projectID }}"> {{ Input::old('project_id') ? Input::old('project_id') : $task->project }} </option>
			<option value="">{{ trans('translate.choose') }}</option>
			
			@foreach ($projects as $v)
				<option value="{{ $v->id }}"> {{ $v->name }} </option>
			@endforeach			
			
		</select>
		
		<?php echo $errors->first('project_id', '<p class="error">:messages</p>');?>
	</div>	
	
	<div class="form-group col-md-6">
		<label for="name">{{ trans('translate.task_name') }}</label>
		<input type="text" name="name" class="form-control required" autocomplete="off" value="{{ Input::old('code') ? Input::old('code') : $task->name }}">

		<?php echo $errors->first('name', '<p class="error">:messages</p>');?>
	</div>	
	<div class="clearfix"></div>
		
	<div class="form-group col-md-4">
		<div class="form-group">
			<label for="start_date">{{ trans('translate.start_date') }}</label>
			<input type="text" name="start_date" class="form-control datepicker required" id="dp1" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ Input::old('start_date') ? Input::old('start_date') : $task->start_date }}">

			<?php echo $errors->first('start_date', '<p class="error">:messages</p>');?>
		</div>
	</div>

	<div class="form-group col-md-4">
		<div class="form-group">
			<label for="due_date">{{ trans('translate.end_date_deadline') }}</label>
			<input type="text" name="due_date" class="form-control datepicker required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd" value="{{ Input::old('due_date') ? Input::old('due_date') : $task->due_date }}">

			<?php echo $errors->first('due_date', '<p class="error">:messages</p>');?>
		</div>
	</div>		

	<div class="form-group col-md-4">
		<div class="form-group">
			<label for="hours">{{ trans('translate.estimated_hours') }}</label>
			<input type="text" name="hours" class="form-control required" autocomplete="off" value="{{ Input::old('hours') ? Input::old('hours') : $task->hours }}">

			<?php echo $errors->first('hours', '<p class="error">:messages</p>');?>
		</div>
	</div>	
	<div class="clearfix"></div>	
	
	<div class="col-md-12">
		<div class="form-group">
			<label for="description">{{ trans('translate.description') }}</label>
			<textarea name="description" class="form-control solsoEditor" rows="7" autocomplete="off">{{ Input::old('description') ? Input::old('description') : $task->description }}</textarea>
		</div>	
	</div>	

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" data-message-title="Create notification" data-message-error="Validation error messages" data-message-success="Data was saved">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}