{{ Form::open(array('url' => 'task/' . $task->id . '/duedate', 'role' => 'form', 'method' => 'POST', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}

	<div class="form-group col-md-4">
		<label for="endDate">{{ trans('translate.end_date_deadline') }}</label>
		<input type="text" name="endDate" class="form-control required" id="dp2" autocomplete="off" data-date-format="yyyy-mm-dd">

		<?php echo $errors->first('endDate', '<p class="error">:messages</p>');?>
	</div>

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_updated') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}
