<div class="col-md-12">
	<h1>{{ $client->name }}</h1>
	<hr>
</div>

<div class="col-md-6">
	<table class="table table-striped">
	<tbody>
		<!-- <tr>
			<td class="col-md-4">{{ trans('translate.country') }}</td>
			<td class="col-md-8">{{ $client->country }}</td>
		</tr>

		<tr>
			<td>{{ trans('translate.state') }}</td>
			<td>{{ $client->state }}</td>
		</tr>		
		
		<tr>
			<td>{{ trans('translate.city') }}</td>
			<td>{{ $client->city }}</td>
		</tr>		
		
		<tr>
			<td>{{ trans('translate.zip_code') }}</td>
			<td>{{ $client->zip }}</td>
		</tr>	 -->

		<tr>
			<td>{{ trans('translate.address') }}</td>
			<td>{{ $client->address }}</td>
		</tr>
		<tr>
			<td class="col-md-4">
				<!-- {{ trans('translate.contact') }} -->
				Contact Number
			</td>
			<td class="col-md-8">{{ $client->contact }}</td>
		</tr>

		<!-- <tr>
			<td>{{ trans('translate.phone') }}</td>
			<td>{{ $client->phone }}</td>
		</tr>	 -->	
		
		<tr>
			<td>{{ trans('translate.email') }}</td>
			<td>{{ $client->email }}</td>
		</tr>	
	</tbody>
	</table>
</div>

<div class="col-md-6">
	<table class="table table-striped">
	<tbody>
		<!-- <tr>
			<td class="col-md-4">
				{{ trans('translate.contact') }}
				Contact Number
			</td>
			<td class="col-md-8">{{ $client->contact }}</td>
		</tr> -->

		<!-- <tr>
			<td>{{ trans('translate.phone') }}</td>
			<td>{{ $client->phone }}</td>
		</tr>	 -->	
		
		<!-- <tr>
			<td>{{ trans('translate.email') }}</td>
			<td>{{ $client->email }}</td>
		</tr> -->		
		
		<!-- <tr>
			<td>{{ trans('translate.website') }}</td>
			<td>{{ $client->website }}</td>
		</tr>	

		<tr>
			<td>{{ trans('translate.bank') }} - {{ trans('translate.bank_account') }}</td>
			<td>{{ $client->bank }} - {{ $client->bank_account }}</td>
		</tr> -->
	</tbody>
	</table>
</div>

<div class="col-md-12">
	<table class="table table-striped">
	<tbody>
		<tr>
			<td colspan="2">
				<h4>{{ trans('translate.description') }}</h4>
				
				{{ $client->description }}
			</td>
		</tr>
	</tbody>
	</table>
</div>		