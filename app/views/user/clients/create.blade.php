{{ Form::open(array('url' => 'client', 'role' => 'form', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}
	
	<div class="col-md-12">
		<div class="form-group">
			<label for="name">{{ trans('translate.name') }}</label>
			<input type="text" name="name" class="form-control required" autocomplete="off" value="{{ isset($inputs['name']) ? $inputs['name'] : '' }}">

			<?php echo $errors->first('name', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<label for="email">{{ trans('translate.email') }}</label>
			<input type="email" name="email" class="form-control required" autocomplete="off" value="{{ isset($inputs['email']) ? $inputs['email'] : '' }}">
			
			<?php echo $errors->first('email', '<p class="error">:messages</p>');?>
		</div>		
		
		<div class="form-group">
			<label for="contact">
				<!-- {{ trans('translate.contact') }} -->
				Contact Number
			</label>
			<input type="text" name="contact" class="form-control required" autocomplete="off" value="{{ isset($inputs['contact']) ? $inputs['contact'] : '' }}">
			
			<?php echo $errors->first('contact', '<p class="error">:messages</p>');?>
		</div>		
		
		<!-- <div class="form-group">
			<label for="country">{{ trans('translate.country') }}</label>
			<input type="text" name="country" class="form-control" autocomplete="off" value="{{ isset($inputs['country']) ? $inputs['country'] : '' }}">

			<?php echo $errors->first('country', '<p class="error">:messages</p>');?>
		</div>		

		<div class="form-group">
			<label for="state">{{ trans('translate.state') }}</label>
			<input type="text" name="state" class="form-control" autocomplete="off" value="{{ isset($inputs['state']) ? $inputs['state'] : '' }}">
			
			<?php echo $errors->first('state', '<p class="error">:messages</p>');?>
		</div>			
		
		<div class="form-group">
			<label for="city">{{ trans('translate.city') }}</label>
			<input type="text" name="city" class="form-control" autocomplete="off" value="{{ isset($inputs['city']) ? $inputs['city'] : '' }}">
			
			<?php echo $errors->first('city', '<p class="error">:messages</p>');?>
		</div>

		<div class="form-group">
			<label for="zip">{{ trans('translate.zip_code') }}</label>
			<input type="text" name="zip" class="form-control" autocomplete="off" value="{{ isset($inputs['zip']) ? $inputs['zip'] : '' }}">
			
			<?php echo $errors->first('zip', '<p class="error">:messages</p>');?>
		</div>	 -->
		
		<div class="form-group">
			<label for="address">{{ trans('translate.address') }}</label>
			<input type="text" name="address" class="form-control" autocomplete="off" value="{{ isset($inputs['address']) ? $inputs['address'] : '' }}">
			
			<?php echo $errors->first('address', '<p class="error">:messages</p>');?>
		</div>				
		
	<!-- 	<div class="form-group">
			<label for="phone">{{ trans('translate.phone') }}</label>
			<input type="text" name="phone" class="form-control" autocomplete="off" value="{{ isset($inputs['phone']) ? $inputs['phone'] : '' }}">
			
			<?php echo $errors->first('phone', '<p class="error">:messages</p>');?>
		</div>	

		<div class="form-group">
			<label for="website">{{ trans('translate.website') }}</label>
			<span class="pull-right">http://www.domain.com</span>
			<input type="url" name="website" class="form-control" autocomplete="off" value="{{ isset($inputs['website']) ? $inputs['website'] : '' }}">
			
			<?php echo $errors->first('website', '<p class="error">:messages</p>');?>
		</div>	

		<div class="form-group">
			<label for="bank">{{ trans('translate.bank') }}</label>
			<input type="text" name="bank" class="form-control" autocomplete="off" value="{{ isset($inputs['bank']) ? $inputs['bank'] : '' }}">
		</div>	

		<div class="form-group">
			<label for="bank_account">{{ trans('translate.bank_account') }}</label>
			<input type="text" name="bank_account" class="form-control" autocomplete="off" value="{{ isset($inputs['bank_account']) ? $inputs['bank_account'] : '' }}">
		</div>				
 -->
		<div class="form-group">
			<label for="description">{{ trans('translate.description') }}</label>
			<textarea name="description" class="form-control" rows="7" autocomplete="off">{{ isset($inputs['description']) ? $inputs['description'] : '' }}</textarea>
		</div>	
	</div>	
	
	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave" 
		data-message-title="{{ trans('translate.create_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.data_was_saved') }}">
			<i class="fa fa-save"></i> {{ trans('translate.save') }}
		</button>
	</div>
	
{{ Form::close() }}