{{ Form::open(array('url' => 'client/' . $client->user_id . '/reset-password', 'role' => 'form', 'method' => 'POST', 'class' => 'solsoForm', 'data-alert' => isset($alert) ? $alert : false )) }}

	<div class="form-group col-md-4">
		<label for="new-password">{{ trans('translate.new_password') }}</label>
		<div class="input-group">
			<span class="input-group-addon"><i class="fa fa-lock"></i></span>
			<input type="password" name="new-password" class="form-control required" name="new-password" placeholder="new password" autocomplete="off" 
			data-parsley-minlength="6" data-parsley-errors-container=".passwordError">
		</div>

		<div class="passwordError"></div>
		<?php echo $errors->first('new-password', '<p class="error">:messages</p>');?>
	</div>

	<div class="form-group col-md-12">
		<button type="button" class="btn btn-success solsoSave"
		data-message-title="{{ trans('translate.update_notification') }}" data-message-error="{{ trans('translate.validation_error_messages') }}" data-message-success="{{ trans('translate.password_was_changed') }}">
			<i class="fa fa-save"></i> Save
		</button>
	</div>

{{ Form::close() }}		
