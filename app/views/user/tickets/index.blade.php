@section('content')

	<div class="col-md-12">
		@if ($projects == 0)
			<div role="alert" class="alert alert-warning">
				<strong>{{ trans('translate.message') }}: </strong> {{ trans('translate.no_projects') }}
				{{ trans('translate.go_to') }} <a href="{{ URL::to('project') }}">{{ trans('translate.projects') }}</a>
			</div>
		@endif
		
		<h1>
			<i class="fa fa-list"></i> {{ trans('translate.tickets') }}
		</h1>
		
		@if ($userIsClient && $projects != 0)
			<button type="button" class="btn btn-primary solsoShowModal"
			data-toggle="modal" data-target="#solsoCrudModal" data-href="{{ URL::to('ticket/create') }}" data-modal-title="{{ trans('translate.create_new_ticket') }}">
				<i class="fa fa-plus"></i> {{ trans('translate.create_new_ticket') }}
			</button>
		@endif
	</div>	

	<div class="col-md-12 top40">
		<h3>{{ trans('translate.tickets') }}</h3>

		<div id="ajaxTable" class="table-responsive">
			@include('user.tickets.table')	
		</div>	
	</div>
	
@stop