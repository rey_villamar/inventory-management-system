<?php

return array(
	
	/* === APPLICATION === */
	"app_name" => "Application Name - your desired name for this app",
	"error404" => "ERROR 404",
	/* === APPLICATION === */
	
	
	/* === LOGIN === */
	"log_in_to_your_account"	=> "Log in to Your Account",	
	"log_in" 					=> "Log in",		
	"forgot_password" 			=> "Forgot Password",
	"send_password" 			=> "Send password",
	/* === END LOGIN === */

	
	/* === USERS === */
	"users"	=> "Users",
	"user"	=> "User",
	
		/* === EMAILS === */
			"account_was_created"		=> "Account was created",
			"password_was_reset"		=> "Password was reset",
			"login_with_credentials"	=> "You can login with following credentials",
		/* === EMAILS === */	
	/* === END USERS === */	
	
	
	/* === ADMIN === */
		/* === LANGUAGES === */
		"language" 				=> "Language",
		"languages" 			=> "Languages",
		"new_language" 			=>  "New language",
		"create_new_language" 	=> "Create new language",
		"edit_language" 		=> "Edit Language",
		"default_language" 		=> "Default language",
		"short_name" 			=> "Short name",
		"translate" 			=> "Translate",
		"original_language" 	=> "Original language",
		"translate_language"	=> "Translate language",
		"translated_language"	=> "Translated language",
		"directory_exist" 		=> "short name already exists",
		/* === END LANGUAGES === */
		
		/* === SETTINGS === */
		"settings" 					=> "Settings",		
		"change_user" 				=> "Change user",
		"change_user_password" 		=> "Change user password",

			/* === EMAIL === */
			"email_address_for_emails" 	=> "Email address for emails",
			"no_receive_emails"			=> "You need to complete Email address for emails",
			/* === END EMAIL === */		
		/* === END SETTINGS === */
		
		/* === UPLOAD === */
		"logo" 		=> "Logo",
		"logo_size"	=>  "Logo size (pixels)",
		/* === END UPLOAD === */	

		/* === EMAILS FORMAT === */
		"basic_format_for_emails"				=> "Basic format for emails",
		"format_email_shortcode"				=> "You can use the following 'SHORTCODE' to pass some personal information through email.",
		"format_email_new_client" 				=> "When ADMIN create a new client, (automatically) the client will receive an email like this.",
		"explain_shortcode_client_name"			=> "the [client_email] shortcode will be replace with the client name in your Email.",
		"explain_shortcode_client_email"		=> "the [client_user] shortcode will be replace with the client email in your Email.",
		"explain_shortcode_client_password"		=> "the [client_password] shortcode will be replace with the client password in your Email.",
		
		"shortcode_mandatory"					=> "The shortcode are mandatory, otherwise the email will be send with empty information !!!",
		/* === END EMAILS EMAILS === */
		
		/* === NOTIFICATIONS === */
		"no_details" 	=> "You need to complete company details !",
		"go_to"			=> "go to ",
		/* === END NOTIFICATIONS === */
	/* === END ADMIN === */
	
	
	/* === DASHBOARD === */
	"dashboard" 				=> "Dashboard",
	"number_of_clients" 		=> "total number of clients",
	"number_of_projects" 		=> "total number of projects",
	"number_of_estimates"		=> "total number of estimates",
	"number_of_invoices" 		=> "total number of invoices",
	"number_of_tasks" 			=> "total number of tasks",
	"number_of_messages" 		=> "total number of messages",
	"number_of_tickets" 		=> "total number of tickets",
	"total_value_of_amounts"	=> "total value of amounts",
	"last" 						=> "Last",
	/* === END DASHBOARD === */	
	
	
	/* === SIDEBAR === */
	"access"		=> "access",
	"create"		=> "create",
	"edit"			=> "edit",
	"view"			=> "view",
	"delete"		=> "delete",
	"inbox" 		=> "inbox",
	"outbox" 		=> "outbox",
	"personal_data"	=> "personal data",
	/* === END SIDEBAR === */
	
	
	/* === CLIENTS === */
	"clients" 			=> "Clients",
	"client" 			=> "Client",
	"create_new_client" => "Create new client",
	"show_client" 		=> "Show client",
	"edit_client" 		=>  "Edit client",
	"company" 			=> "Company",	
	"name" 				=> "Name",
	"country" 			=> "Country",
	"region" 			=> "Region",
	"city" 				=> "City",
	"zip_code" 			=> "Zip code",
	"address" 			=> "Address",
	"contact" 			=> "Contact",
	"phone" 			=> "Phone",
	"website" 			=> "Website",
	"bank" 				=> "Bank",
	"bank_account"		=>  "Bank account",
	"description" 		=> "Description",
	
		/* === NOTIFICATIONS === */
			"no_clients"	=> "You don't have defined any client !",
		/* === END NOTIFICATIONS === */		
	/* === END CLIENTS === */	

	
	/* === INVITATION === */
	"send_invitation" 			=> "Send invitation",
	"invitation" 				=> "Invitation",
	"an_invitation_was_sent"	=> "An invitation was sent !",
	"invitation_was_sent" 		=> "invitation was sent",
	"no_invitation"				=> "You need to complete invitation text before you can send invitations to your clients !",
	/* === END INVITATION === */


	/* === PRODUCTS === */
	"products" 				=> "Products",
	"product" 				=> "Product",
	"add_new_product"		=> "Add new product",
	"new_product" 			=> "New product",
	"create_new_product"	=> "Create new product",
	"code" 					=> "Code",
	"quantity" 				=> "Quantity",
	"change_quantity" 		=> "Chaneg quantity",
	"qty" 					=> "Qty",
	"product_information" 	=> "Product information",
	"item" 					=> "ITEM",
	"price" 				=> "price",
	
		/* === NOTIFICATIONS === */
			"no_products"		=> "You don't have any products !",
			"out_of_stock"		=> "out of stock",
		/* === END NOTIFICATIONS === */	
	/* === END PRODUCTS === */	
	
	
	/* === ESTIMATES === */
	"estimates" 			=> "Estimates",
	"estimate" 				=> "Estimate",
	"create_new_estimate"	=> "Create new estimate",
	"show_estimate" 		=> "Show estimate",
	"edit_estimate" 		=> "Edit estimate",
	"reference" 			=> "Reference",
	"start_date"			=> "Start date",
	"expiry_date" 			=> "Expiry date",
	"approved" 				=> "approved",
	"unapproved" 			=> "unapproved",
	"seen" 					=> "seen",
	"unseen" 				=> "unseen",
	"terms_conditions" 		=> "Terms & Conditions",
	"estimate_discount" 	=> "Estimate discount",
	
		/* === EMAILS === */
		"new_estimate" 			=> "New estimate",
		"new_estimate_from" 	=> "New estimate from",	
		/* === END EMAILS === */
	/* === END ESTIMATES === */

	
	/* === INVOICES === */
	"invoices" 				=> "Invoices",
	"invoice" 				=> "Invoice",
	"create_new_invoice"	=> "Create new invoice",
	"edit_invoice" 			=> "Edit invoice",
	"show_invoice" 			=> "Show invoice",
	"bill_from" 			=> "BILL FROM",
	"bill_to" 				=> "BILL TO",
	"item" 					=> "Item",
	"unit_price" 			=> "Unit price",
	"date" 					=> "Date",
	"discount" 				=> "Discount",
	"discount_type" 		=> "Type",
	"total" 				=> "TOTAL",
	"subtotal" 				=> "Subtotal",
	"paid" 					=> "paid",
	"unpaid" 				=> "unpaid",
	"partially_paid" 		=> "partially paid",
	"overdue" 				=> "overdue",
	"cancelled" 			=> "cancelled",
	"invoice_number" 		=> "Invoice number",
	"list_of_payments"		=> "List of payments",
	"amount" 				=> "amount",
	"amount_paid" 			=> "amount paid",
	"payment_method" 		=> "payment method",
	"due_date" 				=> "Due date",
	"balance" 				=> "Balance",
	"number" 				=> "Number",
	"products_discount" 	=> "Products discount",
	"invoice_discount" 		=> "Invoice discount",
	"add_new_product" 		=> "Add new product",

		/* === ADDONS === */
		"end_due_date" 		=> "End date -> due date",
		"change_status" 	=> "Change status",
		"change_due_date" 	=> "Change due date",
		"add_payment" 		=> "Add payment",
		/* === END ADDONS === */
	
		/* === DIALOG && MESSAGES === */
		"mail_dialog" 		=> "Send email to client",
		"mail_dialog_01" 	=> "You are about to send pdf invoice via email to this user.",
		"mail_dialog_02" 	=> "Do you want to proceed ?",
		/* === DIALOG === */	
	
		/* === SETTINGS === */
		"invoice_start_number" 			=> "Invoice start number - put your number - 0 as start number",
		"invoice_code" 					=> "Invoice code",
		"invoice_personal_title" 		=> "Invoice personal text",
		"invoice_personal_description"	=> "this text will appear on bottom of page on every invoice, this text is not mandatory, use max 2 rows",
		"invoice_status"				=> "Invoice status",
		/* === END SETTINGS === */

		/* === EMAIL === */
			"new_invoice"		=> "New invoice",
			"new_invoice_from"	=> "New invoice from ... ",
		/* === EDN EMAIL === */
		
		/* === NOTIFICATIONS === */
			"no_invoices" 					=> "You didn't create any invoice !",
		/* === END NOTIFICATIONS === */			
	/* === END INVOICES === */
		

	/* === SCHEDULES === */
	"schedules" 			=> "Schedules",
	"schedule" 				=> "Schedule",
	"create_new_schedule" 	=> "Create new schedule",
	"edit_schedule" 		=> "Edit schedule",
	"cancelled" 			=> "cancelled",
	"every" 				=> "every",
	"from_every_month" 		=> "from every month",
	/* === END SCHEDULES === */
		
		
	/* === CURRENCIES === */
	"currencies"	=> "Currencies",
	"currency" 		=> "Currency",
		
		/* === SETTINGS === */
			"values" 			=> "Values",
			"price_position" 	=> "Price position",
			"default_currency"	=> "Default currency",
		/* === END SETTINGS === */
		
		/* === NOTIFICATIONS === */
			"no_currencies"	=> "You don't have defined any currency !",
		/* === END NOTIFICATIONS === */	
	/* === END CURRENCIES === */	
	
	
	/* === TAXES === */
	"tax" 		=> "Tax",
	"tax_rate"	=> "Tax rate",	
		/* === NOTIFICATIONS === */
			"no_taxes"	=> "You don't have defined any tax !",
		/* === END NOTIFICATIONS === */		
	/* === END TAXES === */
	
	
	/* === PAYMENTS === */
	"payments" => "Payments",
	
		/* === NOTIFICATIONS === */
			"no_payments" => "You don't have defined any payments !",	
		/* === END NOTIFICATIONS === */	
	/* === END PAYMENTS === */
		
	
	/* === REPORTS === */
	"reports" 					=> "Reports",
	"report" 					=> "Report",
	"clients_and_products" 		=> "Clients & Products",
	"estimates_and_invoices"	=> "Estimates & Invoices",
	"this_month" 				=> "this month",
	"this_year" 				=> "this year",
	/* === END RRPORTS === */	
	
	
	/* === MESSAGES === */
	"messages" 				=> "Messages",
	"message" 				=> "Message",
	"create_new_message"	=> "Create new message",
	"show_message" 			=> "Show message",
	"edit_message" 			=> "Edit message",
	"read" 					=> "read",
	"unread" 				=> "unread",
	"received_messages"		=> "Received messages",
	"sent_messages" 		=> "Sent messages",
	"reply_to_message"		=> "Reply to message",
	"deleted" 				=> "deleted",
	"histories" 			=> "Histories",
	"reply_to" 				=> "Reply to",
	/* === END MESSAGES === */
	
	
	/* === NOTIFICATIONS  === */
	"create_notification" 		=> "Create notification",
	"update_notification" 		=> "Update notification",
	"delete_notification" 		=> "Delete notification",
	"validation_error_messages" => "Validation error messages",
	"an_error_occurred" 		=> "An error occurred",
	"data_was_saved" 			=> "Data was saved",
	"data_was_updated" 			=> "Data was updated",
	"data_was_deleted" 			=> "Data was deleted",
	"message_was_sent" 			=> "Message was sent",
	"value_already_exist" 		=> "This value already exist",
	"name_is_not_set_yet" 		=> "name is not set yet",
	"old_password_not_match" 	=> "Old password doesn't match",
	"password_was_changed" 		=> "Password was changed",
	"no_data_available" 		=> "No data available",
	"signed_in_as" 				=> "Signed in as",
	"email_was_sent_to_client"	=> "E-mail was sent to client",
	"already_assigned" 			=> "already assigned",
	
		/* === DIALOG WINDOWS === */
		"delete_dialog" 			=> "Delete dialog",
		"procedure_is_irreversible"	=>  "You are about to delete this item this procedure is irreversible !",
		"want_to_proceed" 			=> "Do you want to proceed ?",
		/* === END DIALOG WINDOWS === */
	/* === MESSAGES && NOTIFICATIONS === */
	
		
	/* === ALERTS === */
	"alerts"		=> "Alerts",
	"alert_me_when"	=> "Alert me when",
	"is_lower_then"	=> "is lower then",
	
		/* === NOTIFICATION === */
		"no_alerts"	=> "You don't have defined alerts !",
		/* === END NOTIFICATION === */
	
		/* === PRODUCT === */
		"alert_text_product"	=> "Alert me when product quantity is lower then",
		"lower_stock"			=> "lower stock",
		/* === END PRODUCT === */
	/* === END ALERTS === */	
	
	/* === FORMS === */
	"email" 				=>  "E-mail",
	"repeat_email" 			=> "Repeat e-mail",
	"password" 				=> "Password",
	"create_new_account"	=> "Create new account",
	"account" 				=> "Account",
	"change_password"		=> "Change password",
	"old_password" 			=> "Old password",
	"new_password" 			=> "New password",
	"choose" 				=> "choose",
	"new_value" 			=>  "New value",
	"title" 				=> "Title",
	"content" 				=> "Content",
	/* === END FORMS === */	


	/* === UPLOAD FORM === */
	"upload_logo" 				=> "Upload logo",
	"allowed_file_extensions" 	=> "allowed file extensions",
	"message_logo" 				=> "the company don't have any logo defined !",
	/* === END UPLOAD FORM === */
	
	
	/* === TABLES === */
	"crt" 			=> "Crt",
	"action" 		=> "Action",
	"status" 		=> "Status",
	"state" 		=> "State",
	"created_at" 	=> "Created at",
	"from" 			=> "FROM",
	"sender" 		=> "Sender",
	/* === END TABLES === */
	
	
	/* === BUTTONS === */
	"save" 				=> "Save",
	"cancel" 			=> "Cancel",
	"close" 			=> "Close",
	"no" 				=> "NO",
	"yes" 				=> "YES",
	"left" 				=> "Left",
	"right" 			=> "Right",
	"ok" 				=> "OK",
	"edit" 				=> "Edit",
	"show" 				=> "Show",
	"update" 			=> "Update",
	"delete"			=> "Delete",
	"reply"				=> "Reply",
	"approve" 			=> "Approve",
	"ban" 				=> "Ban",
	"assign" 			=> "Assign",
	"remove_ban" 		=> "Remove Ban",
	"export_pdf" 		=> "Export PDF",
	"export_excel" 		=> "Export Excel",
	"email_to_client" 	=> "Email to Client",
	"sign_in" 			=> "Sign in",
	"logout" 			=> "logout",
	"send" 				=> "Send",
	"reset_password"	=> "Reset password",
	"quick_actions" 	=> "Quick Actions",
	"remove" 			=> "Remove",
	"replace" 			=> "Replace",
	/* === END BUTTONS === */
	
	
	/* === MONTHS === */
	"january" 	=> "January",
	"february" 	=> "February",
	"march" 	=> "March",
	"april" 	=> "April",
	"may" 		=> "May",
	"june" 		=> "June",
	"july" 		=> "July",
	"august" 	=> "August",
	"september"	=> "September",
	"cctober" 	=> "October",
	"november" 	=> "November",
	"december" 	=> "December",
	/* === END MONTHS === */

);
