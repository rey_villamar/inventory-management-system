<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$lang = new Language;
App::setLocale(isset($lang->defaultLanguage()->short) ? $lang->defaultLanguage()->short : 'en');


Route::get('/',						'WebsiteController@index');

Route::get('login',					'LoginController@index');
Route::post('auth',					'LoginController@auth');
Route::get('logout',				'LoginController@logout');
Route::get('forgot-password',		'LoginController@forgotPassword');
Route::post('reset',				'LoginController@reset');


Route::group(array('before' => 'auth'), function()
{
	Route::group(array('before' => 'admin'), function() 
	{
		Route::get('admin/settings',					'AdminController@settings');
		Route::post('admin/company',					'AdminController@company');
		Route::post('admin/image',						'AdminController@imageSizes');
		Route::post('admin/email',						'AdminController@receiveEmails');
				
		Route::post('language/translate',				'LanguageController@translate');
		
		Route::post('currency/default',					'CurrencyController@defaultCurrency');	
		Route::post('currency/position',				'CurrencyController@currencyPosition');
		
		Route::post('invoice/number',					'InvoiceController@storeInvoiceNumber');
		Route::post('invoice/code',						'InvoiceController@storeInvoiceCode');
		Route::post('invoice/text',						'InvoiceController@storeInvoiceText');
		
		Route::post('upload/upload-image',				'UploadController@uploadImage');
		Route::post('upload/image-size',				'UploadController@imageSize');		
		
		Route::get('client/{id}/send-invitation',		'ClientController@sendInvitation');
		Route::get('client/{id}/reset-password',		'ClientController@showResetPassword');
		Route::post('client/{id}/reset-password',		'ClientController@resetPassword');		
		Route::post('add-client',     					'ClientController@addClient');	
		
		Route::post('get-product-price',				'ProductController@getProductPrice');		
		Route::get('product/{id}/quantity',				'ProductController@showQuantity');		
		Route::post('product/{id}/quantity',			'ProductController@updateQuantity');		
		//
		Route::post('get-price',						'ProductController@getPrice');	
			
		Route::get('invoice/{id}/status',				'InvoiceController@showStatus');
		Route::post('invoice/{id}/status',				'InvoiceController@updateStatus');		
		Route::get('invoice/{id}/duedate',				'InvoiceController@showDueDate');
		Route::post('invoice/duedate/{id}',				'InvoiceController@updateDueDate');		
		Route::get('invoice/{id}/add-payment',			'InvoiceController@showPayment');
		Route::post('invoice/add-payment/{id}',			'InvoiceController@addPayment');
		Route::post('update-mode',						'InvoiceController@updateMode');	

		//
		Route::post('save-shipping-fee', 				'InvoiceController@saveShippingFee');
		
		Route::get('schedule/create/{id}',				'ScheduleController@showCreateFormInvoice');
		Route::get('schedule/{id}/edit/from-invoice',	'ScheduleController@edit');		
		
		Route::post('email/estimate/{id}/pdf',			'EmailController@sendEstimate');
		Route::post('email/invoice/{id}/pdf',			'EmailController@sendInvoice');
				
		Route::post('alert/products',					'AlertController@storeAlertProduct');			
				
		Route::resource('admin',						'AdminController');
		Route::resource('currency',						'CurrencyController');
		Route::resource('client',						'ClientController');
		Route::resource('invitation',					'InvitationSettingController');
		Route::resource('format-email',					'FormatEmailController');		
		Route::resource('language',						'LanguageController');		
		Route::resource('payment',						'PaymentController');
		Route::resource('product',						'ProductController');
		Route::resource('report',						'ReportController');	
		Route::resource('schedule',						'ScheduleController');	
		Route::resource('tax',							'TaxController');		
	});	

	Route::get('dashboard',								'DashboardController@index');	

	Route::post('estimate/{id}/approve',				'EstimateController@approve');
	
	Route::get('setting',								'SettingController@index');	
	Route::post('user/language',						'UserController@setDefaultLanguage');
	
	Route::get('message/{id}/sent',						'MessageController@show');
	Route::post('message/{id}/reply',					'MessageController@reply');	

	Route::get('export/estimate/{id}/pdf',				'ExportController@exportToPDF');
	Route::get('export/estimate/{id}/excel',			'ExportController@exportToExcel');	
	Route::get('export/invoice/{id}/pdf',				'ExportController@exportToPDF');
	Route::get('export/invoice/{id}/excel',				'ExportController@exportToExcel');
		
	Route::resource('estimate',							'EstimateController');	
	Route::resource('invoice',							'InvoiceController');	
	Route::resource('user',								'UserController');
	Route::resource('message',							'MessageController');	

});	


App::missing(function($exception)
{
	return Response::view('assets.messages.error-404', array(), 404);
});