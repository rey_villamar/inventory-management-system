<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="dompdf.view" content="XYZ,0,0,1" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo url('public/css/invoice.css'); ?>">
</head>
<body>
	<div id="invoice">
		<table>
			<tr>
				<td class="col-md-6">
					<?php if(isset($logo->name)): ?>
						<img src="<?php echo URL::to('public/upload/' . $logo->name); ?>" width="<?php echo $logo->width; ?>" height="<?php echo $logo->height; ?>">
					<?php endif; ?>
				</td>
				
				<td class="col-md-6">
					<h1><?php echo trans('translate.invoice'); ?></h1>
					
					<table class="border">
						<tr>
							<th class="col-md-6"><?php echo trans('translate.invoice'); ?></th>
							<th class="col-md-6"><?php echo trans('translate.due_date'); ?></th>
						</tr>

						<tr>						
							<td class="text-center"><?php echo $invoiceCode; ?> <?php echo $invoice->number; ?></td>
							<td class="text-center"><?php echo $invoice->due_date; ?></td>
						</tr>
					</table>					
				</td>
			</tr>
		</table>
		
		<table>
			<tr>
				<td class="col-md-6">
					<h3 class="text-left"><?php echo trans('translate.bill_from'); ?></h3>
					<h4><?php echo $owner->name; ?></h4>
					
					<p class="details"><?php echo $owner->city; ?>, <?php echo $owner->state; ?>, <?php echo $owner->country; ?></p>
					<p class="details"><?php echo $owner->address; ?>, <?php echo $owner->zip; ?></p>
					<p class="details"><?php echo $owner->contact; ?></p>
					<p class="details"><?php echo $owner->phone; ?></p>
					<p class="details"><?php echo $owner->bank; ?></p>
					<p class="details"><?php echo $owner->bank_account; ?></p>
				</td>

				<td class="col-md-6">			
					<h3 class="text-left"><?php echo trans('translate.bill_to'); ?></h3>
					<h4><?php echo $invoice->name; ?></h4>
					
					<p class="details"><?php echo $invoice->city; ?>, <?php echo $invoice->state; ?>, <?php echo $invoice->country; ?></p>
					<p class="details"><?php echo $invoice->address; ?>, <?php echo $invoice->zip; ?></p>
					<p class="details"><?php echo $invoice->contact; ?></p>
					<p class="details"><?php echo $invoice->phone; ?></p>
					<p class="details"><?php echo $invoice->bank; ?></p>
					<p class="details"><?php echo $invoice->bank_account; ?></p>
				</td>
			</tr>
		</table>
		
		<table class="table border table-striped">
			<thead>
				<tr>
					<th class="td-crt"><?php echo trans('translate.crt'); ?>.</th>
					<th class="td-product"><?php echo trans('translate.product'); ?></th>
					<th class="td-qty"><?php echo trans('translate.price'); ?></th>
					<th class="td-qty"><?php echo trans('translate.quantity'); ?></th>
					<th class="td-qty"><?php echo trans('translate.tax_rate'); ?></th>
					<th class="td-qty"><?php echo trans('translate.discount'); ?></th>
					<th class="td-qty"><?php echo trans('translate.amount'); ?></th>
				</tr>	
			</thead>
			
			<tbody>
				<?php $subTotalItems 	= 0;?>
				<?php $taxItems 		= 0;?>
				<?php $discountItems	= 0;?>
				<?php $invoiceDiscount	= 0;?>
				
				<?php foreach($products as $crt => $product): ?>
					<tr>
						<td>
							<?php echo $crt + 1; ?>

						</td>
						
						<td>
							<?php echo $product->name; ?>

						</td>	
						
						<td class="text-center">
							<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $product->price); ?>

						</td>	

						<td class="text-center">
							<?php echo $product->quantity; ?>

						</td>					
						
						<td class="text-center">
							<?php echo $product->tax; ?> %
						</td>					
						
						<td class="text-center">
							<?php if($product->discount == 0): ?>
								-
							<?php else: ?>
								<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $product->discount_value); ?>

							<?php endif; ?>
						</td>
						
						<td class="text-center">
							<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $product->amount); ?>

						</td>					
					</tr>
					
					<tr>
						<td colspan="7">
							<?php echo $product->description; ?>

						</td>
					</tr>
					
					<?php $subTotalItems 	+= $product->quantity * $product->price;?>
					<?php $taxItems 		+= ($product->quantity * $product->price) * ($product->tax / 100);?>							
					<?php $discountItems 	+= $product->discount_value;?>				
				<?php endforeach; ?>
			</tbody>
			
			<tfoot>
				<tr>
					<td colspan="4">
												
					</td>
					
					<td colspan="3">
						<table class="table">
							<tr>
								<td class="col-md-6">
									<?php echo trans('translate.subtotal'); ?>

								</td>	

								<td class="col-md-6">
									<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $subTotalItems); ?>

								</td>
							</tr>

							<tr>
								<td class="col-md-6">
									<?php echo trans('translate.tax'); ?>

								</td>	

								<td class="col-md-6">
									<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $taxItems); ?>

								</td>
							</tr>						
							
							<?php if($discountItems != 0): ?>
							<tr>
								<td class="col-md-6">
									<?php echo trans('translate.products_discount'); ?>

								</td>	

								<td class="col-md-6">
									<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $discountItems); ?>

								</td>
							</tr>	
							<?php endif; ?>						
							
							<?php if($invoice->discount != 0): ?>
							<tr>
								<td class="col-md-6">
									<?php echo trans('translate.invoice_discount'); ?>

								</td>	

								<td class="col-md-6">
									<?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $invoice->discount); ?>

								</td>
							</tr>	
							<?php endif; ?>
							
							<tr>
								<td class="col-md-6">
									<h4><?php echo trans('translate.total'); ?></h4>
								</td>	

								<td class="col-md-6">
									<h4><?php echo Solso::currencyPosition($invoice->currency, $invoice->position, $invoice->amount); ?></h4>
								</td>
							</tr>						
						</table>
					</td>	
				</tr>				
			</tfoot>
		</table>				
	</div>
	
	<?php if(isset($invoiceSettings->text)): ?>
		<div class="footer">
			<?php echo $invoiceSettings->text; ?>

		</div>	
	<?php endif; ?>	
	
</body>
</html>