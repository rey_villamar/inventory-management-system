<?php

class InvoiceController extends \BaseController {

	protected $layout;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->layout = 'user.index';
		
		if ($this->userInfo->role_id == 1)
		{
			$this->layout = 'admin.index';
		}
	}
	
	
	/* === VIEW === */
	public function index()
	{
		$invoice 			= new Invoice;
		$invoiceSettings	= InvoiceSetting::find(1);
		$mode_of_payment = $invoice->get_mode_of_payment();


		$data = array(
			'invoices' 		=> $invoice->getAll(),
			'products'		=> Product::where('status', 1)->count(),
			'currencies'	=> Currency::count(),
			'taxes'			=> Tax::count(),
			'owner'			=> Setting::find(1),
			'mode_of_payment' => $mode_of_payment
		);
		
		if ( Request::ajax() )
		{

			return $this->loadDataTable();
		}
		else
		{
			$this->layout->content = View::make('user.invoices.index', $data);
		}
	}

	public function create()
	{
		$invoiceSettings	= InvoiceSetting::find(1);
		
		$data = array(
			'clients'		=> Client::all(),
			'products'		=> Product::where('status', 1)->get(),
			'currencies'	=> Currency::all(),
			'taxes'			=> Tax::orderBy('value', 'asc')->get(),
			'invoiceCode'	=> isset($invoiceSettings->code) 	? $invoiceSettings->code 		: false,
			'invoiceNumber'	=> isset($invoiceSettings->number) 	? $invoiceSettings->number + 1 	: false
		);
		
		return View::make('user.invoices.create', $data);
	}

	public function show($id)
	{
		$invoice 			= new Invoice;
		$products			= new InvoiceProduct;
		$invoiceSettings	= InvoiceSetting::find(1);
		$payment			= new InvoicePayment;
		
		if ( Auth::user()->role_id == 5 )
		{
			$update = Invoice::find($id);
				
			if ($update->state == 0)
			{
				$update->state = 1;
				$update->save();
			}
		}

		$data = array(
			'owner'				=> Setting::find(1),
			'logo'				=> Image::find(1),
			'invoice'			=> $invoice->getOne($id),
			'products'			=> $products->getProducs($id),
			'invoiceCode'		=> isset($invoiceSettings->code) ? $invoiceSettings->code : false,
			'invoicePayments'	=> $payment->payments($id)
		);	

		return View::make('user.invoices.show', $data);
	}	
	
	public function edit($id)
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));
		}
		
		$invoice 			= new Invoice;
		$products			= new InvoiceProduct;
		$invoiceSettings	= InvoiceSetting::find(1);
		
		$data = array(
			'invoice'			=> $invoice->getOne($id),
			'invoiceProducts'	=> $products->getProducs($id),
			'clients'			=> Client::all(),
			'products'			=> Product::where('status', 1)->get(),
			'currencies'		=> Currency::all(),
			'taxes'				=> Tax::orderBy('value', 'asc')->get(),
			'invoiceCode'		=> isset($invoiceSettings->code) ? $invoiceSettings->code : false,
		);
		
		return View::make('user.invoices.edit', $data);
	}
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));	
		}		
		
		$rules = array(
			'client_id'		=> 'required',
			'number'		=> 'required|integer',
			'start_date'	=> 'required|date|date_format:"Y-m-d"',
			'due_date'		=> 'required|date|date_format:"Y-m-d"',
			'currency_id'	=> 'required'
		);

		$validator = Validator::make(Input::all(), $rules);	
		
		if ($validator->passes())
		{
			$invoiceSettings = InvoiceSetting::where('user_id', Auth::id())->first();

			if (isset($invoiceSettings->number))
			{
				$invoiceNumber 				= $invoiceSettings->number + 1;
				$invoiceSettings->number	= $invoiceNumber;
				$invoiceSettings->save();
			}			
			
			$store				= new Invoice;
			$store->user_id 	= Auth::id();
			$store->number		= isset($invoiceSettings->number) ? $invoiceNumber : Input::get('number');
			$store->status_id	= 2;
			$store->shipping_fee = Input::get('shippingFee');
			// $store->discount	= Input::get('invoiceDiscount') ? Input::get('invoiceDiscount') : 0;
			// $store->type    	= Input::get('invoiceDiscountType') ? Input::get('invoiceDiscountType') : 0;
			$store->amount		= $store->calculateInvoice(Input::get('qty'), Input::get('price'));		
			// $store->amount		= $store->calculateInvoice(Input::get('qty'), Input::get('price'), Input::get('taxes'), Input::get('discount'), Input::get('discountType'), Input::get('invoiceDiscount'), Input::get('invoiceDiscountType'));				
			$store->fill(Input::all());
			$store->save();				
			
			$products			= Input::get('products');
			
			foreach ($products as $k => $v)
			{
				$product 					= new InvoiceProduct;
				$product->user_id			= Auth::id();
				$product->invoice_id		= $store->id;
				$product->product_id		= $v;
				$product->quantity			= Input::get('qty')[$k];
				$product->price    			= Input::get('price')[$k];
				$product->shipping_fee		= Input::get('shippingFee');
				// $product->tax	    		= Input::get('taxes')[$k];
				// $product->discount    		= Input::get('discount')[$k] ? Input::get('discount')[$k] : 0;
				// $product->discount_type		= Input::get('discountType')[$k] ? Input::get('discountType')[$k] : 0;
				// $product->discount_value	= $store->calculateProductPrice(1, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				// $product->amount			= $store->calculateProductPrice(2, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);
				$product->discount_value	= $store->calculateProductPrice(1, Input::get('qty')[$k], Input::get('price')[$k]);	
				$product->amount			= $store->calculateProductPrice(2, Input::get('qty')[$k], Input::get('price')[$k]);		
				$product->save();		
				
				App::make('ProductController')->manageQuantity($v, Input::get('qty')[$k]);
			}			
			
			$invoice = new Invoice;
			$invoice->invoiceStatus();			
			
			App::make('EmailController')->sendInvoice($store->id);
		}
		else
		{
			$invoiceSettings	= InvoiceSetting::find(1);
			
			$data = array(
				'clients' 		=> Client::all(),
				'products'		=> Product::where('status', 1)->get(),
				'currencies'	=> Currency::all(),
				'taxes'			=> Tax::all(),
				'invoiceCode'	=> isset($invoiceSettings->code) 	? $invoiceSettings->code 		: false,
				'invoiceNumber'	=> isset($invoiceSettings->number) 	? $invoiceSettings->number + 1 	: false,				
				'errors' 		=> $validator->errors(),
				'inputs'		=> Input::all()	
			);
			
			return View::make('user.invoices.create', $data);
		}
		

		return $this->loadDataTable();
	}
	
	public function update($id)
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));	
		}
		
		$rules = array(
			'client_id'		=> 'required',
			'number'		=> 'required|integer',
			'start_date'	=> 'required|date|date_format:"Y-m-d"',
			'due_date'		=> 'required|date|date_format:"Y-m-d"',
			'currency_id'	=> 'required'
		);	
		
		$validator = Validator::make(Input::all(), $rules);	
		
		if ($validator->passes())
		{
			$invoice	= new Invoice;
			
			$delete		= InvoiceProduct::where('invoice_id', $id)->where('user_id', Auth::id());
			$delete->delete();			
			
			$update 			= Invoice::where('id', $id)->where('user_id', Auth::id())->first();
			$update->status_id	= 2;
			$update->discount	= Input::get('invoiceDiscount') ? Input::get('invoiceDiscount') : 0;
			$update->type    	= Input::get('invoiceDiscountType') ? Input::get('invoiceDiscountType') : 0;
			$update->amount		= $invoice->calculateInvoice(Input::get('qty'), Input::get('price'), Input::get('taxes'), Input::get('discount'), Input::get('discountType'), Input::get('invoiceDiscount'), Input::get('invoiceDiscountType'));				
			$update->fill(Input::all());
			$update->save();				
			
			$products			= Input::get('products');
			
			foreach ($products as $k => $v)
			{
				$product 					= new InvoiceProduct;
				$product->user_id			= Auth::id();
				$product->invoice_id		= $id;
				$product->product_id		= $v;
				$product->quantity			= Input::get('qty')[$k];
				$product->price    			= Input::get('price')[$k];
				$product->shipping_fee		= Input::get('shippingFee');
				// $product->tax	    		= Input::get('taxes')[$k];
				// $product->discount    		= Input::get('discount')[$k] ? Input::get('discount')[$k] : 0;
				// $product->discount_type		= Input::get('discountType')[$k] ? Input::get('discountType')[$k] : 0;
				// $product->discount_value	= $invoice->calculateProductPrice(1, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				// $product->amount			= $invoice->calculateProductPrice(2, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				$product->amount			= $invoice->calculateProductPrice(2, Input::get('qty')[$k], Input::get('price')[$k]);	
				$product->save();		
			}			
			
			$invoice = new Invoice;
			$invoice->invoiceStatus();			
			
			App::make('EmailController')->sendInvoice($id);	
		}
		else
		{
			$data = array(
				'clients' 		=> Client::all(),
				'products'		=> Product::where('status', 1)->get(),
				'currencies'	=> Currency::all(),
				'taxes'			=> Tax::all(),
				'invoiceCode'	=> isset($invoiceSettings->code) 	? $invoiceSettings->code 		: false,
				'invoiceNumber'	=> isset($invoiceSettings->number) 	? $invoiceSettings->number + 1 	: false,		
				'errors' 		=> $validator->errors(),
				'inputs'		=> Input::all()	
			);
			
			return View::make('user.invoices.edit', $data);
		}	
		
		return $this->loadDataTable();
	}

	public function destroy($id)
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));	
		}
		
		$delete = Invoice::where('id', $id)->where('user_id', Auth::id())->first();
		$delete->delete();
		
		$products = InvoiceProduct::where('invoice_id', $id)->where('user_id', Auth::id());
		$products->delete();		
		
		return $this->loadDataTable();
	}
	/* === END C.R.U.D. === */
	
	
	/* === PRIVATE === */
	public function loadDataTable()
	{

		$invoice = new Invoice;
		
		$data = array(
			'invoices' 	=> $invoice->getAll(),
			'owner'		=> Setting::find(1),
			'alert'		=> 1
		);
	
		return View::make('user.invoices.table', $data);		
	}
	/* === END PRIVATE === */
	
	
	/* === ADDONS === */
	public function showStatus($id)
	{
		$data = array(
			'invoice' 	=> Invoice::where('id', $id)->select('id')->first(),
			'status'	=> InvoiceStatus::all()
		);
		
		return View::make('user.invoices.addons.status', $data);
	}
	
	public function updateStatus($id)
	{
		$rules = array(
			'status' => 'required',
		);	

		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{	
			$update 			= Invoice::where('id', $id)->first();
			$update->status_id 	= Input::get('status');
			$update->date_paid  = date('Y-m-d');
			$update->save();
		}
		else
		{
			$data = array(
				'invoice' 	=> Invoice::where('id', $id)->select('id')->first(),
				'status'	=> InvoiceStatus::all(),
				'errors'	=> $validator->errors(),
			);
			
			return View::make('user.invoices.addons.status', $data);
		}

		$invoice = new Invoice;
		
		$data = array(
			'invoices' => $invoice->getAll(),
			'alert'	=> 1
		);
		
		return View::make('user.invoices.table', $data);
	}	
	
	public function showDueDate($id)
	{
		$data = array(
			'invoice' 	=> Invoice::where('id', $id)->select('id')->first()
		);
		
		return View::make('user.invoices.addons.duedate', $data);
	}
	
	public function updateDueDate($id)
	{
		$rules = array(
			'endDate' => 'required',
		);	

		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{	
			$update 			= Invoice::where('id', $id)->first();
			$update->status_id	= 2;
			$update->due_date	= Input::get('endDate');
			$update->save();
		}
		else
		{
			$data = array(
				'invoice' 	=> Invoice::where('id', $id)->select('id')->first(),
				'errors'	=> $validator->errors()
			);
			
			return View::make('user.invoices.addons.duedate', $data);
		}

		$invoice = new Invoice;
		
		$data = array(
			'invoices' => $invoice->getAll(),
			'alert'	=> 1
		);
		
		return View::make('user.invoices.table', $data);
	}	
	
	public function showPayment($id)
	{
		$data = array(
			'invoice' 	=> Invoice::where('id', $id)->select('id')->first(),
			'payments'	=> Payment::all()
		);
		
		return View::make('user.invoices.addons.payment', $data);
	}
	
	public function addPayment($id)
	{
		$rules = array(
			'amount'	=> 'required|numeric',
			'start'		=> 'required|date|date_format:"Y-m-d"',
			'payment'	=> 'required|numeric',
		);

		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{	
			$store 					= new InvoicePayment;
			$store->user_id			= Auth::id();
			$store->invoice_id		= $id;
			$store->payment_id		= Input::get('payment');
			$store->payment_date	= Input::get('start');
			$store->payment_amount	= Input::get('amount');
			$store->save();				
			
			$store->balance($id);
		}
		else
		{
			$data = array(
				'invoice' 	=> Invoice::where('id', $id)->select('id')->first(),
				'payments'	=> Payment::all(),
				'errors'	=> $validator->errors(),
			);
			
			return View::make('user.invoices.addons.payment', $data);
		}

		$invoice = new Invoice;
		
		$data = array(
			'invoices' => $invoice->getAll(),
			'alert'	=> 1
		);
		
		return View::make('user.invoices.table', $data);
	}	
	
	
	
	public function updateMode()
	{
		$invoiceID = Input::get('invoiceID');
		$selected = Input::get('selected');
		$invoice = new Invoice;
		$data = $invoice->updateMode($invoiceID,$selected);
		return $data;
	}	
	
	
	
	
	/* === ADDONS === */
	
	
	/* === OTHERS === */
	public function storeInvoiceNumber()
	{
		$rules = array(
			'number' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$update = InvoiceSetting::where('user_id', Auth::id())->first();
			
			if ( $update )
			{
				$update->number		= Input::get('number');
				$update->save();
			}
			else
			{
				$store 				= new InvoiceSetting;
				$store->user_id		= Auth::id();
				$store->number		= Input::get('number');
				$store->save();
			}
		}
		else
		{		
			return $this->returnInvoiceSettings(false);
		}
		
		return $this->returnInvoiceSettings(true);				
	}
	
	public function storeInvoiceCode()
	{
		$rules = array(
			'code' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$update = InvoiceSetting::where('user_id', Auth::id())->first();
			
			if ( $update )
			{
				$update->code		= Input::get('code');
				$update->save();
			}
			else
			{
				$store 				= new InvoiceSetting;
				$store->user_id		= Auth::id();
				$store->code		= Input::get('code');
				$store->save();
			}
		}
		else
		{		
			return $this->returnInvoiceSettings(false);
		}
		
		return $this->returnInvoiceSettings(true);	
	}
	
	public function storeInvoiceText()
	{
		$rules = array(
			'description' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$update = InvoiceSetting::where('user_id', Auth::id())->first();
			
			if ( $update )
			{
				$update->text		= Input::get('description');
				$update->save();
			}
			else
			{
				$store 				= new InvoiceSetting;
				$store->user_id		= Auth::id();
				$store->text		= Input::get('description');
				$store->save();
			}
		}
		else
		{		
			return $this->returnInvoiceSettings(false);	
		}
		
		return $this->returnInvoiceSettings(true);			
	}
	
	private function returnInvoiceSettings($state)
	{
		if ($state)
		{
			$data = array(
				'invoiceSettings' 	=> InvoiceSetting::where('user_id', Auth::id())->first(),
				'invoiceStatus'		=> InvoiceStatus::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
				'alert'				=> 1
			);
		}
		else
		{
			$data = array(
				'invoiceSettings' 	=> InvoiceSetting::where('user_id', Auth::id())->first(),
				'errors' 			=> $validator->errors(),
				'inputs'			=> Input::all(),
			);			
		}
		
		return View::make('admin.settings.invoice', $data);		
	}
	/* === END OTHERS === */

	public function saveShippingFee(){
		 $shippingFee = Input::get("shippingFee");
		 $invoice = new Invoice;
		 $invoice->saveShippingFee($shippingFee);
		 // echo $shippingFee;


	}
	
}