<?php

class LoginController extends \BaseController {

	protected $layout = 'login.index';
	
	
	/* === VIEW === */
	public function index()
	{
		$this->layout->content = View::make('login.login');
	}
	
	public function forgotPassword()
	{
		$seo = array(
			'title'			=> '',
			'keywords'		=> '',
			'description'	=> ''
		);
		View::share('seo', $seo);
		
		$this->layout->content = View::make('login.password');
	}	
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function auth()
	{

		// echo Hash::make(Input::get('password'));
		//  exit();
		$rules = array(
			'email'		=> 'required|email',
			'password'	=> 'required|min:5',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);
	
		if ($validator->passes())
		{
			if ( Auth::attempt(Input::only('email','password'), $remember = false) )
			{
				if ( Auth::user()->status == 1 )
				{
					if ( Auth::user()->role_id == 1 )
					{
						return Redirect::to('admin');
					}
					else
					{
						return Redirect::to('dashboard');
					}
				}
				elseif ( Auth::user()->status == 2 )
				{
					return Redirect::to('login')->with('error', 'Your account was banned !');
				}
				else
				{
					return Redirect::to('login')->with('warning', 'Your account was not approved yet !');
				}
			}
			else
			{
				return Redirect::to('login')->with('warning', 'Your username/password combination was incorrect !');
			}	
		}
		else
		{
			return Redirect::to('login')->with('warning', "Validation Error Messages ")->withErrors($validator);
		}
	}

	public function auth2(){
		return Redirect::to('dashboard');
	}

	public function reset()
	{
		$rules = array(
			'sendEmail' => 'required|email',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{
			if ( User::where('email', Input::get('sendEmail'))->count() == 1 )
			{
				$pass 				= str_random(10);
	
				$update				= User::where('email', Input::get('sendEmail'))->first();
				$update->password 	= Hash::make($pass);
				$update->save();
				
				$fromEmail	= User::where('id', 1)->first()->email;	
				$toEmail	= Input::get('sendEmail');;
				$title		= "Reset password";
				$subject	= "Reset password";
				
				$values = array(
					'user'		=> Input::get('sendEmail'),
					'password' 	=> $pass
				);	
				
				Mail::send('assets.emails.reset', $values, function($message) use ($fromEmail, $toEmail, $title, $subject)
				{
					$message->from($fromEmail, $title);

					$message->to($toEmail)->subject($subject);

				});					
			}
			else
			{
				return Redirect::to('forgot-password')->with('warning', 'This email is not registered !');
			}
		}
		else
		{
			return Redirect::to('forgot-password')->with('warning', "Validation Error Messages ")->withErrors($validator);
		}

		return Redirect::to('login')->with('message', 'The password was reset !');
	}	
	
	public function logout()
	{
		Auth::logout();
		
		return Redirect::to('');
	}
	/* === END C.R.U.D. === */
}