<?php

class WebsiteController extends \BaseController {

	protected $layout = 'login.index';

	
	/* === VIEW === */
	public function index()
	{
		$this->layout->content = View::make('login.login');
	}	

	public function showTicket($number)
	{
		$data = array(
			'ticket' => Ticket::where('ticket_random', $number)->first()
		);			
		
		$this->layout->content = View::make('website.tickets.show', $data);
	}
	/* === END VIEW === */
	
}