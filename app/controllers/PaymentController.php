<?php

class PaymentController extends \BaseController {

	protected $layout = 'index';
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		$rules = array(
			'value' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$payment = Payment::where('user_id', Auth::id())->where('name', Input::get('value'))->first();
			
			if(!$payment)
			{
				$store				= new Payment;
				$store->user_id		= Auth::id();
				$store->name		= Input::get('value');
				$store->save();	
			}
			else
			{
				$data = array(
					'payments'	=> Payment::where('user_id', Auth::id())->get(),
					'errors'	=> $validator->errors(),
					'inputs'	=> Input::all(),
					'alert'		=> 2
				);
				
				return View::make('admin.settings.payment', $data);		
			}
		}
		else
		{		
			$data = array(
				'payments'	=> Payment::where('user_id', Auth::id())->get(),
				'errors'	=> $validator->errors(),
				'inputs'	=> Input::all(),
				'alert'		=> 3
			);
			
			return View::make('admin.settings.payment', $data);	
		}
		
		$data = array(
			'payments'	=> Payment::where('user_id', Auth::id())->get(),
			'alert'		=> 1
		);
		
		return View::make('admin.settings.payment', $data);		
	}
	
	public function update($id)
	{
		$rules = array(
			'payment' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$payment = Payment::where('user_id', Auth::id())->where('name', Input::get('payment'))->first();
			
			if(!$payment)
			{			
				$update			= Payment::where('id', $id)->where('user_id', Auth::id())->first();
				$update->name	= Input::get('payment');
				$update->save();
			}
			else
			{
				$data = array(
					'payments'		=> Payment::where('user_id', Auth::id())->get(),
					'errors'		=> $validator->errors(),
					'inputs'		=> Input::all(),
					'alert'			=> 2
				);
				
				return View::make('admin.settings.payment', $data);		
			}				
		}
		else
		{		
			$data = array(
				'payments'		=> Payment::where('user_id', Auth::id())->get(),
				'errors'		=> $validator->errors(),
				'inputs'		=> Input::all()
			);
			
			return View::make('admin.settings.payment', $data);	
		}
		
		$data = array(
			'payments'		=> Payment::where('user_id', Auth::id())->get(),
			'alert'			=> 1
		);
		
		return View::make('admin.settings.payment', $data);			
	}

	public function destroy($id)
	{
		$delete = Payment::where('id', $id)->where('user_id', Auth::id())->first();
		$delete->delete();
		
		$data = array(
			'payments' => Payment::where('user_id', Auth::id())->get(),
		);
		
		return View::make('admin.settings.payment', $data);			
	}	
	/* === END C.R.U.D. === */

}