<?php

class AdminController extends \BaseController {

	protected $layout = 'admin.index';
	
	
	/* === VIEW === */
	public function index()
	{
		$invoice 	= new Invoice;
		$report 	= new Report;
		
		$data = array(
			'amount'			=> Invoice::sum('amount'),
			'invoiceStatus'		=> InvoiceStatus::all(),
			'invoiceChart'		=> $invoice->invoiceChart(),
			'tax'				=> Tax::count(),
			'currencies'		=> Currency::count(),
			'payments'			=> Payment::count(),
			'invitation'		=> InvitationSetting::find(1),
			'owner'				=> Setting::find(1),
			'logo'				=> Image::find(1),
			
			'totalClients'		=> Client::count(),
			'totalProducts'		=> Product::count(),
			'totalInvoices'		=> Invoice::count(),
			'lastClients'		=> Client::orderBy('id', 'desc')->take(5)->get(),
			'lastProducts'		=> Product::orderBy('id', 'desc')->take(5)->get(),
			'lastInvoices'		=> array_slice($invoice->getAll(), -5)
		);

		View::share('label1', 			$report->showMonths());
		View::share('reportClients', 	$report->showYearReport('clients', 'created_at'));
		View::share('reportProducts', 	$report->showYearReport('products', 'created_at'));
		View::share('reportEstimates', 	$report->showYearReport('estimates', 'start_date'));
		View::share('reportInvoices', 	$report->showYearReport('invoices', 'start_date'));

		$this->layout->content = View::make('admin.dashboard', $data);
	}
	
	public function settings()
	{
		$language = new Language;
		
		$data = array(
			'company' 			=> Setting::find(1),
			'logo'				=> Image::where('user_id', Auth::id())->first(),
			'taxes' 			=> Tax::where('user_id', Auth::id())->orderBy('value', 'asc')->get(),
			'invoiceSettings'	=> InvoiceSetting::where('user_id', Auth::id())->first(),
			'invoiceStatus'		=> InvoiceStatus::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
			'currencies'		=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
			'payments'			=> Payment::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),		
			'invitation'		=> InvitationSetting::find(1),
			'languages'			=> Language::all(),
			'defaultLanguage'	=> $language->defaultLanguage(),
			'emails'			=> FormatEmail::find(1),
			'alerts'			=> Alert::find(1)
		);

		$this->layout->content = View::make('admin.settings.index', $data);
	}	
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function update($id)
	{
		$update = User::find($id);
		
		if ( Input::get('action') == 'email' )
		{
			$rules = array(
				'email'     	=> 'required|email',
				'repeat-email'	=> 'required|same:email',
			);	
			
			$validator = Validator::make(array_map('trim', Input::all()), $rules);	
			
			if ($validator->passes())
			{
				$update->email	= Input::get('email');
			}
			else
			{
				$data = array(
					'errors'	=> $validator->errors(),
					'alert'		=> 3
				);
				
				return View::make('admin.settings.account', $data);	
			}
		}
		
		if ( Input::get('action') == 'password' )
		{
			$rules = array(
				'old-password'	=> 'required|min:6',
				'new-password'	=> 'required|min:6',
			);	
			
			$validator = Validator::make(array_map('trim', Input::all()), $rules);	
			
			if ($validator->passes())
			{
				if ( Hash::check(Input::get('old-password'), $update->password) )
				{
					$update->password = Hash::make(Input::get('new-password'));
				}
				else
				{
					$data = array(
						'errors'	=> $validator->errors(),
						'alert'		=> 2
					);
					
					return View::make('admin.settings.password', $data);	
				}
			}
			else
			{	
				$data = array(
					'errors'	=> $validator->errors(),
					'alert'		=> 3
				);
				
				return View::make('admin.settings.password', $data);	
			}
		}
		
		$update->save();
		
		$data = array(
			'alert'	=> 1
		);		
		
		return View::make('admin.settings.account', $data);
	}
	/* === END C.R.U.D. === */	
	
	
	/* === OTHERS === */
	public function company()
	{
		$rules = array(
			'name'     	=> 'required',
			// 'country'	=> 'required',
			'state'		=> 'required',
			// 'city'		=> 'required',
			// 'zip'		=> 'required',
			'address'	=> 'required',
			'contact'	=> 'required',
			// 'phone'		=> 'required',
			// 'email'		=> 'required|email',
			// 'website'	=> 'url',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$update = Setting::find(1);
			$update->fill(Input::all());
			$update->save();			
		}
		else
		{
			$data = array(
				'company' 	=> Setting::where('id', 1)->first(),
				'errors' 	=> $validator->errors(),
				'inputs'	=> Input::all(),
				'alert'		=> 3
			);
			
			return View::make('admin.settings.company', $data);
		}	
		
		$data = array(
			'company' 	=> Setting::where('id', 1)->first(),
			'alert'		=> 1
		);
		
		return View::make('admin.settings.company', $data);	
	}
	
	public function receiveEmails()
	{
		$rules = array(
			'email' => 'required|email'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{		
			$update					= Setting::where('id', 1)->first();
			$update->receive_emails	= Input::get('email');
			$update->save();
		}
		else
		{
			$data = array(
				'company' 	=> Setting::where('id', 1)->first(),
				'errors' 	=> $validator->errors(),
				'alert'		=> 3
			);
			
			return View::make('admin.settings.email', $data);
		}	
		
		$data = array(
			'company' 	=> Setting::where('id', 1)->first(),
			'alert'		=> 1
		);	
		
		return View::make('admin.settings.email', $data);	
	}	
	/* === END OTHERS === */	
	
}