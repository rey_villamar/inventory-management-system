<?php

class ReportController extends \BaseController {

	protected $layout = 'admin.index';
	
	
	/* === VIEW === */
	public function index()
	{
		$report = new Report;
		// var_dump($report->showMonthlySales());
		// exit();
		View::share('reportMonthlySales', $report->showMonthlySales());	
		View::share('label1', 	$report->showDays());
		View::share('label2', 	$report->showMonths());
		View::share('reportClients', 	$report->showMonthReport('clients', 'created_at'));
		View::share('reportClients2', 	$report->showYearReport('clients', 'created_at'));
		View::share('reportProducts', 	$report->showMonthReport('products', 'created_at'));
		View::share('reportProducts2', 	$report->showYearReport('products', 'created_at'));
		View::share('reportEstimates', 	$report->showMonthReport('estimates', 'start_date'));
		View::share('reportEstimates2', $report->showYearReport('estimates', 'start_date'));
		View::share('reportInvoices', 	$report->showMonthReport('invoices', 'start_date'));			
		View::share('reportInvoices2', 	$report->showYearReport('invoices', 'start_date'));	

				
		$this->layout->content = View::make('user.reports.index');
	}
	/* === END VIEW === */

}