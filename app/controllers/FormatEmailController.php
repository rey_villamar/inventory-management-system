<?php

class FormatEmailController extends \BaseController {

	protected $layout = 'index';

	
	/* === C.R.U.D. === */
	public function store()
	{
		$update = FormatEmail::find(1);
		
		if ( $update )
		{
			$update->fill(Input::all());
			$update->save();
		}
		else
		{
			$store = new FormatEmail;
			$store->fill(Input::all());
			$store->save();	
		}
		
		return $this->loadDataTable();	
	}
	
	public function update($id)
	{
		$update = FormatEmail::find(1);
		$update->fill(Input::all());
		$update->save();	

		return $this->loadDataTable();	
	}
	/* === END C.R.U.D. === */

	
	/* === PRIVATE === */
	public function loadDataTable()
	{
		$data = array(
			'emails'	=> FormatEmail::find(1),
			'alert'		=> 1
		);
		
		return View::make('admin.settings.emails-format', $data);			
	}
	/* === END PRIVATE === */
}