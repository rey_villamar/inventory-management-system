<?php

class SettingController extends \BaseController {

	protected $layout = 'user.index';
	
	
	/* === VIEW === */
	public function index()
	{
		$language = new Language;

		$data = array(
			'client'			=> Client::where('user_id', Auth::id())->first(),
			'languages'			=> Language::all(),
			'defaultLanguage'	=> $language->defaultLanguage()
		);

		$this->layout->content = View::make('user.settings.index', $data);
	}
	/* === END VIEW === */

}