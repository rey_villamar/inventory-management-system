<?php

class ClientController extends \BaseController {

	protected $layout;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->layout = 'user.index';
		
		if ($this->userInfo->role_id == 1)
		{
			$this->layout = 'admin.index';
		}
	}

	
	/* === VIEW === */
	public function index()
	{
		$client = new Client;
		
		$data = array(
			'clients'	=> $client->getAll()
		);

		if (Request::ajax())
		{
			return $this->loadDataTable();
		}
		else
		{			
			$this->layout->content = View::make('user.clients.index', $data);
		}	
	}

	public function create()
	{
		return View::make('user.clients.create');
	}
	
	public function inserClient(){
		
		$client = new Client;
		$data = $client->insertClient();
		
		return $data;
	}

	public function show($id)
	{
		$data = array(
			'client' => Client::find($id),
		);
		
		return View::make('user.clients.show', $data);
	}	
	
	public function edit($id)
	{
		$data = array(
			'client' => Client::find($id),
		);
		
		return View::make('user.clients.edit', $data);
	}
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		$rules = array(
			'name'     	=> 'required',
			'email'		=> 'required|email|unique:users',
			'contact'	=> 'required',
			// 'website'	=> 'url',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{
			$password 			= str_random(10);
			
			$store				= new User;
			$store->role_id		= 5;
			$store->parent_id	= Auth::id();
			$store->name		= Input::get('contact');
			$store->email		= Input::get('email');
			$store->password	= Hash::make($password);
			$store->save();	
			
			$client				= new Client;
			$client->user_id	= $store->id;
			$client->fill(Input::all());
			$client->save();	


			// send email to Client
			$fromEmail		= Setting::find(1)->receive_emails;	
			$toEmail		= Input::get('email');
			$subject		= trans('translate.account_was_created');
			$formatEmail	= FormatEmail::find(1);

			if (isset($formatEmail->client))
			{
				$content = str_replace('[client_name]', Input::get('name'), $formatEmail->client);
				$content = str_replace('[client_email]', Input::get('email'), $content);
				$content = str_replace('[client_password]', $password, $content);
			
				$values = array(
					'content'	=> $content
				);
				
				Mail::send('assets.emails.index', $values, function($message) use ($fromEmail, $toEmail, $subject)
				{
					$message->from($fromEmail, trans('translate.app_name'));
					$message->to($toEmail)->subject($subject);
				});
			}
		}
		else
		{
			$data = array(
				'errors' 	=> $validator->errors(),
				'inputs'	=> Input::all()
			);
			
			return View::make('user.clients.create', $data);
		}	
		
		return $this->loadDataTable();
	}
	
	public function update($id)
	{
		$rules = array(
			'name'     	=> 'required',
			'email'		=> 'required|email',
			'contact'	=> 'required',
			// 'website'	=> 'url',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{	
			if ($this->userIsClient )
			{
				$user = User::find(Auth::id());
				$user->name = Input::get('contact');
				$user->save();
			}
	
			$update	= Client::find($id);
			$update->fill(Input::all());
			$update->save();
			
			$user		= User::find($update->user_id);
			$user->name	= Input::get('contact');
			$user->save();
		}
		else
		{
			$data = array(
				'client' 	=> Client::find($id),
				'inputs'	=> Input::all(),
				'errors' 	=> $validator->errors()
			);
			
			if ( ! $this->userIsClient )
			{
				return View::make('user.clients.edit', $data);
			}				
			
			if ( $this->userIsClient )
			{
				return View::make('user.settings.client', $data);
			}	
		}
		
		if ( ! $this->userIsClient )
		{			
			return $this->loadDataTable();
		}				
		
		if ( $this->userIsClient )
		{			
			$data = array(
				'client' 	=> Client::find($id),
				'alert'		=> 1
			);
			
			return View::make('user.settings.client', $data);	
		}	
	}

	public function destroy($id)
	{
		$user = User::find($id);
		$user->delete();
		
		$delete = Client::where('user_id', $id)->first();
		$delete->delete();
		
		return $this->loadDataTable();
	}
	/* === END C.R.U.D. === */


	/* === PRIVATE ===  */
	public function loadDataTable()
	{
		$client = new Client;
		
		$data = array(
			'clients' 	=> $client->getAll(),
			'alert'		=> 1
		);
		
		return View::make('user.clients.table', $data);		
	}	
	/* === END PRIVATE ===  */
	
	
	/* === ADDONS === */
	public function showResetPassword($id)
	{
		$data = array(
			'client' => Client::where('user_id', $id)->first(),
		);
		
		return View::make('user.clients.addons.reset-password', $data);
	}
	
	
	
	public function resetPassword($id)
	{
		$rules = array(
			'new-password' => 'required|min:6',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{
			$update = User::find($id);
			$update->password = Hash::make(Input::get('new-password'));
			$update->save();
			
			$fromEmail		= Setting::find(1)->receive_emails;	
			$toEmail		= $update->email;
			$subject		= trans('translate.password_was_reset');
			$formatEmail	= FormatEmail::find(1);			
			
			if (isset($formatEmail->client))
			{
				$content = str_replace('[client_name]', Client::where('user_id', $id)->first()->name, $formatEmail->client);
				$content = str_replace('[client_email]', $update->email, $content);
				$content = str_replace('[client_password]', Input::get('new-password'), $content);				

				$values = array(
					'content' => $content
				);				

				Mail::send('assets.emails.index', $values, function($message) use ($fromEmail, $toEmail, $subject)
				{
					$message->from($fromEmail, trans('translate.app_name'));
					$message->to($toEmail)->subject($subject);
				});				
			}			
		}
		else
		{	
			$data = array(
				'client' 	=> Client::where('user_id', $id)->first(),
				'errors'	=> $validator->errors()
			);
			
			return View::make('user.clients.addons.reset-password', $data);
		}		
		
		return $this->loadDataTable();
	}
	/* === ADDONS === */
	
	
	/* === OTHERS === */
	public function sendInvitation($id)
	{
		$store				= new Invitation;
		$store->user_id		= Auth::id();
		$store->client_id	= $id;
		$store->status		= 1;
		$store->save();		
	
		$text = InvitationSetting::find(1);
		
		$data = array(
			'title'		=> $text->title,
			'content' 	=> $text->content
		);
		
		$contactEmail = Client::where('id', $id)->first()->email;
		
		Mail::send('assets.emails.invitation', $data, function($message) use ($contactEmail)
		{
			$message->from(Auth::user()->email, trans('translate.app_name'));
			$message->to($contactEmail)->subject(trans('translate.invitation'));
		});		

		return $this->loadDataTable();	
	}
	/* === END OTHERS === */

	public function addClient(){

		$password 			= str_random(10);
			
		$store				= new User;
		$store->role_id		= 5;
		$store->parent_id	= Auth::id();
		$store->name		= Input::get('name');
		$store->email		= Input::get('email_c');
		$store->password	= Hash::make($password);
		$store->save();	
			
		$client = new Client;
		$user_id = $store->id;
		$name 	 = Input::get('name'); 
		$address = Input::get('address');
		$email   = Input::get('email_c');
		$contact = Input::get('contact');
		$description = Input::get('description');
		$data = $client->addClient($name,$address,$email,$contact,$description,$user_id);

		return $data;

	}
}