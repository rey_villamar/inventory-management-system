<?php

class EstimateController extends \BaseController {

	protected $layout;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->layout = 'user.index';
		
		if ($this->userInfo->role_id == 1)
		{
			$this->layout = 'admin.index';
		}
	}
	
	
	/* === VIEW === */
	public function index()
	{
		$estimate = new Estimate;
		
		$data = array(
			'estimates' 	=> $estimate->getAll(),
			'products'		=> Product::where('status', 1)->count(),
			'currencies'	=> Currency::count(),
			'taxes'			=> Tax::count(),
			'owner'			=> Setting::find(1)
		);

		if ( Request::ajax() )
		{
			return $this->loadDataTable();
		}
		else
		{
			$this->layout->content = View::make('user.estimates.index', $data);
		}		
	}

	public function create()
	{
		$data = array(
			'clients'		=> Client::all(),
			'products'		=> Product::where('status', 1)->get(),
			'currencies'	=> Currency::all(),
			'taxes'			=> Tax::orderBy('value', 'asc')->get()
		);

		return View::make('user.estimates.create', $data);
	}

	public function show($id)
	{
		$estimate 	= new Estimate;
		$products	= new EstimateProduct;
		
		if ( Auth::user()->role_id == 5 )
		{
			$update = Estimate::find($id);
				
			if ($update->status == 0)
			{
				$update->status = 1;
				$update->save();
			}
		}
		
		$data = array(
			'owner'		=> Setting::find(1),
			'logo'		=> Image::find(1),
			'estimate'	=> $estimate->getOne($id),
			'products'	=> $products->getProducs($id)
		);		
		
		return View::make('user.estimates.show', $data);
	}	
	
	public function edit($id)
	{	
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));
		}

		$estimate 	= new Estimate;
		$products	= new EstimateProduct;
		
		$data = array(
			'estimate'			=> $estimate->getOne($id),
			'estimateProducts'	=> $products->getProducs($id),			
			'clients' 			=> Client::all(),
			'products'			=> Product::where('status', 1)->get(),
			'currencies'		=> Currency::all(),
			'taxes'				=> Tax::all(),
		);
		
		return View::make('user.estimates.edit', $data);
	}
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));
		}
	
		$rules = array(
			'client_id'		=> 'required',
			'estimate'		=> 'required',
			'reference'		=> 'required',
			'start_date'	=> 'required|date|date_format:"Y-m-d"',
			'due_date'		=> 'required|date|date_format:"Y-m-d"',	
			'currency_id'	=> 'required'	
		);	
		
		$validator = Validator::make(Input::all(), $rules);	
		
		if ($validator->passes())
		{
			$invoice			= new Invoice;
			
			$store				= new Estimate;
			$store->user_id 	= Auth::id();
			$store->discount	= Input::get('invoiceDiscount') ? Input::get('invoiceDiscount') : 0;
			$store->type    	= Input::get('invoiceDiscountType') ? Input::get('invoiceDiscountType') : 0;
			$store->amount		= $invoice->calculateInvoice(Input::get('qty'), Input::get('price'), Input::get('taxes'), Input::get('discount'), Input::get('discountType'), Input::get('invoiceDiscount'), Input::get('invoiceDiscountType'));				
			$store->fill(Input::all());
			$store->save();	
			
			$products			= Input::get('products');
			
			foreach ($products as $k => $v)
			{
				$product 					= new EstimateProduct;
				$product->user_id			= Auth::id();
				$product->estimate_id		= $store->id;
				$product->product_id		= $v;
				$product->quantity			= Input::get('qty')[$k];
				$product->price    			= Input::get('price')[$k];
				$product->tax	    		= Input::get('taxes')[$k];
				$product->discount    		= Input::get('discount')[$k] ? Input::get('discount')[$k] : 0;
				$product->discount_type		= Input::get('discountType')[$k] ? Input::get('discountType')[$k] : 0;
				$product->discount_value	= $invoice->calculateProductPrice(1, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				$product->amount			= $invoice->calculateProductPrice(2, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				$product->save();		
			}			
		}
		else
		{
			$products	= new EstimateProduct;
			
			$data = array(
				'owner'				=> Setting::find(1),
				'logo'				=> Image::find(1),
				'estimate'			=> $estimate->getOne($id),
				'estimateProducts'	=> $products->getProducs($id),			
				'clients' 			=> Client::all(),
				'products'			=> Product::where('status', 1)->get(),
				'currencies'		=> Currency::all(),
				'taxes'				=> Tax::all(),
				'errors' 			=> $validator->errors(),
				'inputs'			=> Input::all()				
			);				
			
			return View::make('user.estimates.create', $data);
		}
		
		return $this->loadDataTable();
	}
	
	public function update($id)
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('login')->with('message', trans('translate.permissions_denied') );	
		}
	
		$rules = array(
			'client_id'		=> 'required',
			'estimate'		=> 'required',
			'reference'		=> 'required',
			'start_date'	=> 'required|date|date_format:"Y-m-d"',
			'due_date'		=> 'required|date|date_format:"Y-m-d"',	
			'currency_id'	=> 'required'	
		);		
		
		$validator = Validator::make(Input::all(), $rules);		
		
		if ($validator->passes())
		{		
			$invoice	= new Invoice;

			$delete 	= EstimateProduct::where('estimate_id', $id)->where('user_id', Auth::id());
			$delete->delete();

			$update 			= Estimate::where('id', $id)->where('user_id', Auth::id())->first();
			$update->discount	= Input::get('invoiceDiscount') ? Input::get('invoiceDiscount') : 0;
			$update->type    	= Input::get('invoiceDiscountType') ? Input::get('invoiceDiscountType') : 0;
			$update->amount		= $invoice->calculateInvoice(Input::get('qty'), Input::get('price'), Input::get('taxes'), Input::get('discount'), Input::get('discountType'), Input::get('invoiceDiscount'), Input::get('invoiceDiscountType'));				
			$update->fill(Input::all());
			$update->save();	
			
			$products			= Input::get('products');
			
			foreach ($products as $k => $v)
			{
				$product 					= new EstimateProduct;
				$product->user_id			= Auth::id();
				$product->estimate_id		= $id;
				$product->product_id		= $v;
				$product->quantity			= Input::get('qty')[$k];
				$product->price    			= Input::get('price')[$k];
				$product->tax	    		= Input::get('taxes')[$k];
				$product->discount    		= Input::get('discount')[$k] ? Input::get('discount')[$k] : 0;
				$product->discount_type		= Input::get('discountType')[$k] ? Input::get('discountType')[$k] : 0;
				$product->discount_value	= $invoice->calculateProductPrice(1, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				$product->amount			= $invoice->calculateProductPrice(2, Input::get('qty')[$k], Input::get('price')[$k], Input::get('taxes')[$k], Input::get('discount')[$k], Input::get('discountType')[$k]);	
				$product->save();		
			}			
		}
		else
		{
			$estimate 	= new Estimate;
			$products	= new EstimateProduct;
			
			$data = array(
				'owner'				=> Setting::find(1),
				'logo'				=> Image::find(1),
				'estimate'			=> $estimate->getOne($id),
				'estimateProducts'	=> $products->getProducs($id),			
				'clients' 			=> Client::all(),
				'products'			=> Product::where('status', 1)->get(),
				'currencies'		=> Currency::all(),
				'taxes'				=> Tax::all(),
				'errors' 			=> $validator->errors(),
				'inputs'			=> Input::all()				
			);				
			
			return View::make('user.estimates.create', $data);				
		}
		
		return $this->loadDataTable();
	}

	public function destroy($id)
	{
		if ( Auth::user()->role_id != 1)
		{
			return Redirect::to('login')->with('message', trans('translate.permissions_denied') );	
		}		

		$delete = Estimate::where('id', $id)->where('user_id', Auth::id());
		$delete->delete();			
		
		$products = EstimateProduct::where('estimate_id', $id)->where('user_id', Auth::id());
		$products->delete();
		
		return $this->loadDataTable();
	}
	/* === END C.R.U.D. === */
	
	
	/* === PRIVATE === */
	public function loadDataTable()
	{
		$estimate = new Estimate;
		
		$data = array(
			'owner'		=> Setting::find(1),
			'estimates' => $estimate->getAll(),
			'alert'	=> 1
		);
		
		return View::make('user.estimates.table', $data);		
	}
	/* === END PRIVATE === */
	
	
	/* === OTHERS === */
	public function approve($id)
	{
		if ( Auth::user()->role_id != 5 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));		
		}
		
		$estimare = new Estimate;
		$estimare->transformIntoInvoice($id);

		return Redirect::to('estimate')->with('message', trans('translate.data_was_updated'));
	}
	/* === END OTHERS === */
}