<?php

class CurrencyController extends \BaseController {

	protected $layout = 'index';
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		$rules = array(
			'value' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$currency = Currency::where('user_id', Auth::id())->where('name', Input::get('value'))->first();
			
			if(!$currency)
			{
				$store				= new Currency;
				$store->user_id		= Auth::id();
				$store->name		= Input::get('value');
				$store->position	= 2;
				$store->save();	
			}
			else
			{
				$data = array(
					'company' 		=> Setting::where('id', 1)->first(),				
					'currencies'	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
					'errors'		=> $validator->errors(),
					'inputs'		=> Input::all(),
					'alert'			=> 2
				);
				
				return View::make('admin.settings.currency', $data);		
			}
		}
		else
		{		
			$data = array(
				'company' 		=> Setting::where('id', 1)->first(),
				'currencies' 	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
				'errors'		=> $validator->errors(),
				'inputs'		=> Input::all(),
				'alert'			=> 3
			);
			
			return View::make('admin.settings.currency', $data);
		}
		
		$data = array(
			'company' 		=> Setting::where('id', 1)->first(),
			'currencies' 	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
			'alert'			=> 1
		);
		
		return View::make('admin.settings.currency', $data);	
	}
	
	public function update($id)
	{
		$rules = array(
			'currency' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$currency = Currency::where('user_id', Auth::id())->where('name', Input::get('currency'))->first();
			
			if(!$currency)
			{			
				$update				= Currency::where('id', $id)->where('user_id', Auth::id())->first();
				$update->name		= Input::get('currency');
				$update->save();
			}
			else
			{
				$data = array(
					'company' 		=> Setting::where('id', 1)->first(),				
					'currencies'	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
					'errors'		=> $validator->errors(),
					'inputs'		=> Input::all(),
					'alert'			=> 2
				);
				
				return View::make('admin.settings.currency', $data);		
			}				
		}
		else
		{		
			$data = array(
				'company'		=> Setting::where('id', 1)->first(),
				'currencies'	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
				'errors'		=> $validator->errors(),
				'inputs'		=> Input::all(),
			);
			
			return View::make('admin.settings.currency', $data);
		}
		
		$data = array(
			'company' 		=> Setting::where('id', 1)->first(),
			'currencies' 	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
			'alert'			=> 1
		);
		
		return View::make('admin.settings.currency', $data);			
	}

	public function destroy($id)
	{
		$delete = Currency::where('id', $id)->where('user_id', Auth::id())->first();
		$delete->delete();
		
		$data = array(
			'company' 		=> Setting::where('id', 1)->first(),
			'currencies' 	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
		);
		
		return View::make('admin.settings.currency', $data);			
	}
	/* === END C.R.U.D. === */

	
	/* === OTHERS === */
	public function currencyPosition()
	{
		$update				= Currency::where('id', Input::get('itemID'))->where('user_id', Auth::id())->first();
		$update->position	= Input::get('itemValue');
		$update->save();
		
		$data = array(
			'company' 		=> Setting::where('id', 1)->first(),
			'currencies' 	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
			'alert'			=> 1
		);
		
		return View::make('admin.settings.currency', $data);	
	}

	public function defaultCurrency()
	{
		$update					= Setting::where('id', 1)->first();
		$update->currency_id	= Input::get('itemID');
		$update->save();
		
		$data = array(
			'company' 		=> Setting::where('id', 1)->first(),
			'currencies' 	=> Currency::where('user_id', Auth::id())->orderBy('name', 'asc')->get(),
		);		
		
		return View::make('admin.settings.currency', $data);	
	}	
	/* === EDN OTHERS === */
}