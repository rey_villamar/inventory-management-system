<?php

class TaxController extends \BaseController {

	protected $layout = 'index';
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		$rules = array(
			'value' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$tax = Tax::where('user_id', Auth::id())->where('value', Input::get('value'))->first();
			
			if( ! $tax )
			{
				$store			= new Tax;
				$store->user_id	= Auth::id();
				$store->value	= Input::get('value');
				$store->save();	
			}
			else
			{
				$data = array(
					'taxes'	=> Tax::where('user_id', Auth::id())->orderBy('value', 'asc')->get(),
					'alert'	=> 2
				);
				
				return View::make('admin.settings.tax', $data);		
			}
		}
		else
		{		
			$data = array(
				'taxes' 	=> Tax::where('user_id', Auth::id())->orderBy('value', 'asc')->get(),
				'errors'	=> $validator->errors()
			);
			
			return View::make('admin.settings.tax', $data);
		}
		
		return $this->loadDataTable();	
	}
	
	public function update($id)
	{
		$rules = array(
			'tax' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$tax = Tax::where('user_id', Auth::id())->where('value', Input::get('tax'))->first();
			
			if( ! $tax )
			{			
				$update			= Tax::where('id', $id)->where('user_id', Auth::id())->first();
				$update->value	= Input::get('tax');
				$update->save();
			}
			else
			{
				$data = array(
					'taxes'	=> Tax::where('user_id', Auth::id())->orderBy('value', 'asc')->get(),
					'alert'	=> 2
				);
				
				return View::make('admin.settings.tax', $data);		
			}			
		}
		else
		{		
			$data = array(
				'taxes' 	=> Tax::where('user_id', Auth::id())->orderBy('value', 'asc')->get(),
				'errors'	=> $validator->errors()
			);
			
			return View::make('admin.settings.tax', $data);
		}
		
		return $this->loadDataTable();			
	}

	public function destroy($id)
	{
		$delete = Tax::where('id', $id)->where('user_id', Auth::id())->first();
		$delete->delete();
		
		return $this->loadDataTable();			
	}
	/* === END C.R.U.D. === */

	
	/* === PRIVATE === */
	private function loadDataTable()
	{
		$data = array(
			'taxes'	=> Tax::where('user_id', Auth::id())->orderBy('value', 'asc')->get(),
			'alert'	=> 1
		);
		
		return View::make('admin.settings.tax', $data);		
	}	
	/* === END PRIVATE === */
}