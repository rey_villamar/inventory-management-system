<?php

class DashboardController extends \BaseController {

	protected $layout = 'user.index';
	
	
	public function index()
	{
		$estimate 	= new Estimate;
		$invoice 	= new Invoice;
		
		$data = array(
			'totalInvoices'	=> sizeof($invoice->getAll()),
			'amount'		=> Invoice::where('client_id', Client::where('user_id', Auth::id())->first()->id)->sum('amount'),
			
			'estimates'		=> array_slice($estimate->getAll(), -5),
			'invoices'		=> array_slice($invoice->getAll(), -5),
			
			'owner'			=> Setting::find(1)
		);		
		
		$this->layout->content = View::make('user.dashboard', $data);
	}

}