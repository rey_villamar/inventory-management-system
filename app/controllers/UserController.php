<?php

class UserController extends \BaseController {

	protected $layout = 'user.index';
	
	
	/* === C.R.U.D. === */
	public function update($id)
	{
		$update = User::find($id);
		
		if ( Input::get('action') == 'email' )
		{
			$rules = array(
				'email'     	=> 'required|email',
				'repeat-email'	=> 'required|same:email',
			);	
			
			$validator = Validator::make(array_map('trim', Input::all()), $rules);	
			
			if ($validator->passes())
			{
				$update->email	= Input::get('email');
			}
			else
			{
				$data = array(
					'errors'	=> $validator->errors()
				);
				
				return View::make('user.settings.account', $data);	
			}
			
			$update->save();
			
			$data = array(
				'alert'	=> 1
			);		
			
			return View::make('user.settings.account', $data);			
		}
		
		if ( Input::get('action') == 'password' )
		{
			$rules = array(
				'old-password'	=> 'required|min:6',
				'new-password'	=> 'required|min:6',
			);	
			
			$validator = Validator::make(array_map('trim', Input::all()), $rules);	
			
			if ($validator->passes())
			{
				if ( Hash::check(Input::get('old-password'), $update->password) )
				{
					$update->password = Hash::make(Input::get('new-password'));
				}
				else
				{
					$data = array(
						'errors'	=> $validator->errors(),
						'alert'		=> 2
					);
					return View::make('user.settings.password', $data);	
				}
			}
			else
			{	
				$data = array(
					'errors' => $validator->errors()
				);
				
				return View::make('user.settings.password', $data);	
			}
		
			$update->save();
			
			$data = array(
				'alert'	=> 1
			);		
			
			return View::make('user.settings.password', $data);			
		}
	}
	/* === END C.R.U.D. === */	
	
	
	/* === OTHERS === */
	public function setDefaultLanguage()
	{
		$rules = array(
			'language' 	=> 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{		
			$update					= User::where('id', Auth::id())->first();
			$update->language_id	= Input::get('language');
			$update->save();	
		}
		else
		{
			$data = array(
				'languages'	=> Language::all(),
				'errors'	=> $validator->errors(),
				'inputs'	=> Input::all()
			);
			
			return View::make('user.settings.language', $data);				
		}
		
		$language = new Language;
		
		$data = array(
			'languages'			=> Language::all(),
			'defaultLanguage'	=> $language->defaultLanguage(),
			'alert'				=> 1
		);		
		
		return View::make('user.settings.language', $data);
	}
	/* === END OTHERS === */
}