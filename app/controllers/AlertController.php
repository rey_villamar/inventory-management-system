<?php

class AlertController extends \BaseController {

	protected $layout = 'index';
	
	
	/* === C.R.U.D. === */
	public function storeAlertProduct()
	{
		$rules = array(
			'products' => 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);		
		
		if ($validator->passes())
		{
			$update = Alert::find(1);
			
			if( $update )
			{
				$update->products	= Input::get('products');
				$update->save();
			}
			else
			{
				$store				= new Alert;
				$store->products	= Input::get('products');
				$store->save();			
			}
		}
		else
		{		
			$data = array(
				'alerts'	=> Alert::find(1),
				'errors'	=> $validator->errors()
			);
			
			return View::make('admin.settings.alerts', $data);
		}
		
		return $this->loadDataTable();	
	}
	/* === END C.R.U.D. === */

	
	/* === PRIVATE === */
	private function loadDataTable()
	{
		$data = array(
			'alerts'	=> Alert::find(1),
			'alert'		=> 1
		);
		
		return View::make('admin.settings.alerts', $data);		
	}	
	/* === END PRIVATE === */
}