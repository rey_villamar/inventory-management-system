<?php

class ExportController extends \BaseController {

	protected $layout = 'user.index';
	
	
	/* === VIEW === */
	public function exportToPDF($id)
	{
		$invoiceSettings	= InvoiceSetting::find(1);
		$invoiceCode		= isset($invoiceSettings->code) ? $invoiceSettings->code : false;
		
		if (Request::segment(2) == 'estimate')
		{
			$estimate 	= new Estimate;
			$products	= new EstimateProduct;

			$data = array(
				'owner'			=> Setting::find(1),
				'logo'			=> Image::find(1),
				'estimate'		=> $estimate->getOne($id),
				'products'		=> $products->getProducs($id),
				'invoiceCode'	=> $invoiceCode
			);		
			
			$pdf 		= PDF::loadView('user.estimates.export.pdf', $data)->setPaper('letter')->setOrientation('portrait');
			$pdfName	= 'estimate_' . $estimate->getOne($id)->estimate . '_' . date('Y-m-d');
		}		
		
		if (Request::segment(2) == 'invoice')
		{
			$newInvoice	= new Invoice;
			$products	= new InvoiceProduct;
			$invoice	= $newInvoice->getOne($id);
			
			$data = array(
				'owner'				=> Setting::find(1),
				'logo'				=> Image::find(1),
				'invoice'			=> $invoice,
				'products'			=> $products->getProducs($id),
				'invoiceCode'		=> $invoiceCode
			);
			
			$pdf 		= PDF::loadView('user.invoices.export.pdf', $data)->setPaper('letter')->setOrientation('portrait');
			$pdfName	= 'invoice_' . $invoice->number . '_' . date('Y-m-d');
		}
		
		return $pdf->download( $pdfName . '.pdf');	
	}
	
	public function exportToExcel($id)
	{
		$invoiceSettings	= InvoiceSetting::find(1);
		$invoiceCode		= isset($invoiceSettings->code) ? $invoiceSettings->code : false;		
		
		if (Request::segment(2) == 'estimate')
		{
			$estimate 	= new Estimate;
			$products	= new EstimateProduct;

			$data = array(
				'owner'			=> Setting::find(1),
				'logo'			=> Image::find(1),
				'estimate'		=> $estimate->getOne($id),
				'products'		=> $products->getProducs($id),
				'invoiceCode'	=> $invoiceCode
			);	
			
			Excel::create('Estimate', function($excel) use ($data) {

				return $excel->sheet('Estimate', function($sheet) use ($data) {

					$sheet->loadView('user.estimates.export.excel', $data);

				})->export('xlsx');

			});			
		}		
		
		if (Request::segment(2) == 'invoice')
		{
			$newInvoice	= new Invoice;
			$products	= new InvoiceProduct;
			$invoice	= $newInvoice->getOne($id);
			
			$data = array(
				'owner'				=> Setting::find(1),
				'logo'				=> Image::find(1),
				'invoice'			=> $invoice,
				'products'			=> $products->getProducs($id),
				'invoiceCode'		=> $invoiceCode
			);	
			
			Excel::create('Invoice', function($excel) use ($data) {

				return $excel->sheet('Invoice', function($sheet) use ($data) {

					$sheet->loadView('user.invoices.export.excel', $data);

				})->export('xlsx');

			});
		}
	}	
	
	public function exportToCSV($id)
	{
			
	}
	/* === END VIEW === */


	/* === PRIVATE === */
	public function loadDataTable()
	{
		$invoice = new Invoice;
		
		$data = array(
			'invoices' 	=> $invoice->getAll(),
			'alert'		=> 1
		);
		
		return View::make('user.invoices.table', $data);		
	}
	/* === END PRIVATE === */

}