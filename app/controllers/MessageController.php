<?php

class MessageController extends \BaseController {

	protected $layout;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->layout = 'user.index';
		
		if ($this->userInfo->role_id == 1)
		{
			$this->layout = 'admin.index';
		}
	}

	
	/* === VIEW === */
	public function index()
	{
		$message = new Message;
		
		$data = array(
			'received' 	=> $message->getAll(1),
			'sent' 		=> $message->getAll(2),
		);

		if ( Request::ajax() )
		{
			return $this->loadDataTable();	
		}
		else
		{
			$this->layout->content = View::make('user.messages.index', $data);
		}		
	}

	public function create()
	{
		$users = User::where('role_id', '!=', 1)->orderBy('name', 'asc')->get();	
		
		if ( Auth::user()->role_id != 1 )
		{
			$users = User::where('role_id', 1)->get();
		}	
		
		$data = array(
			'users' => $users
		);
		
		return View::make('user.messages.create', $data);
	}

	public function show($id)
	{
		$message	= new Message;
		$type		= 1;
		
		if ( Request::segment(3) == 'sent' )
		{
			$type = 2;	
		}
		
		$newMessage = $message->getOne($id, $type);
		

		if ( $type == 1 )
		{
			$update = Message::where('id', $id)->where('user_id', Auth::id())->first();
			$update->status = 1;
			$update->save();		
		}

		$data = array(
			'message' 	=> $newMessage,
			'histories'	=> $message->messageHistories($newMessage->parent_id),
			'type'		=> $type,
			'owner'		=> $newMessage->from_id == $this->userInfo->id ? true : false
		);		

		return View::make('user.messages.show', $data);	
	}	
	
	public function edit($id)
	{
		if ( Auth::user()->role_id != 5 )
		{
			$update = Message::where('id', $id)->where('user_id', Auth::id())->first();
			$update->status = 1;
			$update->save();		
			
			$message = new Message;
			
			$data = array(
				'message' => $message->getOne($id, 2)
			);
			
			return View::make('messages.edit', $data);
		}
		else
		{
			$update = Message::where('id', $id)->where('user_id', Auth::id())->first();
			$update->status = 1;
			$update->save();		
			
			$message = new Message;
			
			$data = array(
				'message' => $message->getOne($id, 2)
			);
			
			return View::make('messages.edit', $data);			
		}
	}
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		$rules = array(
			'user'    	=> 'required',
			'title'     => 'required',
			'content'	=> 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{
			$store			= new Message;
			$store->user_id	= Input::get('user');
			$store->from_id	= Auth::id();
			$store->fill(Input::all());
			$store->save();
		}
		else
		{
			$data = array(
				'users' => User::where('id', '!=', Auth::id())->where('role_id', '!=', 5)->get(),
				'errors'	=> $validator->errors(),
				'inputs'	=> Input::all(),
			);
			
			return View::make('user.messages.create', $data);			
		}	
		
		return $this->loadDataTable();
	}
	
	public function update($id)
	{
		$update = Message::where('id', $id)->where('from_id', Auth::id())->first();
		$update->state = 1;
		$update->save();
		
		$delete = Message::where('id', $id)->where('from_id', Auth::id())->where('status', 2)->where('state', 1)->first();
		
		if ($delete)
		{
			$delete->delete();			
		}		
		
		return $this->loadDataTable();
	}

	public function destroy($id)
	{
		if ( (Auth::user()->role_id == 1) )
		{
			$update = Message::where('id', $id)->where('from_id', Auth::id())->first();
			$update->state = 1;
			$update->save();
			
			$delete = Message::where('id', $id)->where('from_id', Auth::id())->where('status', 2)->where('state', 1)->first();
		}
		elseif ( (Auth::user()->role_id == 5) )
		{
			$update = Message::where('id', $id)->where('user_id', Auth::id())->first();
			$update->status = 2;
			$update->save();			
			
			$delete = Message::where('id', $id)->where('user_id', Auth::id())->where('status', 2)->where('state', 1)->first();
		}
		else
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));
		}		
			
		if ($delete)
		{
			$delete->delete();			
		}	
		
		return $this->loadDataTable();	
	}
	/* === END C.R.U.D. === */
	
	
	/* === PRIVATE === */
	public function loadDataTable()
	{
		$message = new Message;
		
		$data = array(
			'received' 	=> $message->getAll(1),
			'sent' 		=> $message->getAll(2),
			'alert'		=> 1
		);
		
		return View::make('user.messages.table', $data);		
	}
	/* === END PRIVATE === */
	
	
	/* === OHTERS === */
	public function reply($id)
	{
		$rules = array(
			'title'     => 'required',
			'content'	=> 'required'
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{		
			$store				= new Message;
			$store->user_id		= Input::get('userID');
			$store->from_id		= Auth::id();
			$store->parent_id	= $id;
			$store->fill(Input::all());
			$store->save();
		}
		else
		{	
			$message = new Message;
			
			$data = array(
				'message'	=> $message->getOne($id, 1),
				'errors'	=> $validator->errors(),
				'inputs'	=> Input::all(),
			);
			
			return View::make('user.messages.show', $data);			
		}	
		
		$message = new Message;
		
		$data = array(
			'received' 	=> $message->getAll(1),
			'sent' 		=> $message->getAll(2),
			'alert'		=> 1
		);
		
		return View::make('user.messages.table', $data);					
	}
	/* === END OHTERS === */
	
}