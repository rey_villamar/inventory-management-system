<?php

class ProductController extends \BaseController {

	protected $layout;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->layout = 'user.index';
		
		if ($this->userInfo->role_id == 1)
		{
			$this->layout = 'admin.index';
		}
	}
	
	
	/* === VIEW === */
	public function index()
	{
		$currency = new Currency;	
		
		$data = array(
			'products' 	=> Product::where('user_id', Auth::id())->where('status', 1)->get(),
			'currency'	=> $currency->defaultCurrency()
		);

		if (Request::ajax())
		{
			return $this->loadDataTable();
		}
		else
		{			
			$this->layout->content = View::make('user.products.index', $data);
		}		
		
	}

	public function create()
	{
		return View::make('user.products.create');
	}

	public function show($id)
	{
		$currency = new Currency;
		
		$data = array(
			'product'	=> Product::where('id', $id)->where('user_id', Auth::id())->first(),
			'currency'	=> $currency->defaultCurrency()
		);
		
		return View::make('user.products.show', $data);
	}	
	
	public function edit($id)
	{
		$data = array(
			'product'	=> Product::where('id', $id)->where('user_id', Auth::id())->first(),
		);
		
		return View::make('user.products.edit', $data);
	}
	/* === END VIEW === */
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));	
		}
		
		$rules = array(
			'name'		=> 'required',
			// 'code'		=> 'required',
			// 'price'		=> 'required',
		);	
		
		$validator = Validator::make(Input::all(), $rules);	
		
		if ($validator->passes())
		{
			$store			= new Product;
			$store->user_id	= Auth::id();
			$store->status	= 1;
			$store->fill(Input::all());
			$store->save();		
		}
		else
		{
			$currency = new Currency;
			
			$data = array(
				'currency'	=> $currency->defaultCurrency(),
				'errors' 	=> $validator->errors(),
				'inputs'	=> Input::all(),
			);
			
			return View::make('user.invoices.create', $data);
		}	
		
		return $this->loadDataTable();
	}
	
	public function update($id)
	{
		if ( Auth::user()->role_id != 1 )
		{
			return Redirect::to('dashboard')->with('error', trans('translate.permissions_denied'));	
		}
		
		$rules = array(
			'name'		=> 'required',
			// 'code'		=> 'required',
			// 'price'		=> 'required',
		);		
		
		$validator = Validator::make(Input::all(), $rules);	
		
		if ($validator->passes())
		{
			$update	= Product::where('id', $id)->where('user_id', Auth::id())->first();
			$update->fill(Input::all());
			$update->save();	
		}
		else
		{
			$currency = new Currency;
			
			$data = array(
				'currency'	=> $currency->defaultCurrency(),	
				'errors' 	=> $validator->errors(),
				'inputs'	=> Input::all(),
			);
			
			return View::make('user.invoices.edit', $data);
		}	
		
		return $this->loadDataTable();
	}

	public function destroy($id)
	{
		$update 		= Product::where('id', $id)->where('user_id', Auth::id())->first();
		$update->status	= 0;
		$update->save();
		
		return $this->loadDataTable();	
	}
	/* === END C.R.U.D. === */

	
	/* === PRIVATE ===  */
	public function loadDataTable()
	{
		$currency = new Currency;
		
		$data = array(
			'products' 	=> Product::where('user_id', Auth::id())->where('status', 1)->get(),
			'currency'	=> $currency->defaultCurrency(),
			'alert'		=> 1
		);
		
		return View::make('user.products.table', $data);		
	}	
	/* === END PRIVATE ===  */	
	
	
	/* === ADDONS === */
	public function showQuantity($id)
	{
		$data = array(
			'product' 	=> Product::find($id),
		);
		
		return View::make('user.products.addons.quantity', $data);
	}
	
	public function updateQuantity($id)
	{
		$update 			= Product::where('id', $id)->where('user_id', Auth::id())->first();
		$update->quantity	= Input::get('quantity');
		$update->save();		
		
		return $this->loadDataTable();	
	}
	/* === END ADDONS === */
	
	
	/* === OTHERS === */
	public function manageQuantity($id, $qty)
	{
		$update 			= Product::find($id);
		$update->quantity	= $update->quantity - $qty;
		$update->save();		
	}
	/* === END OTHERS === */
	
	
	/* === AJAX === */
	public function getProductPrice()
	{
		return json_encode( Product::where('id', Input::get('product'))->where('user_id', Auth::id())->first()->price);
	}	
	
	/* === END AJAX === */


	public function getPriceType(){

	}

	public function getPrice(){
		$priceType = Input::get('priceType');
		$productID = Input::get('products');
		$product = new Product;
		$data = $product->getPrice($priceType,$productID);
		return $data;

	}
	
}