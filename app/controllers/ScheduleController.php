<?php

class ScheduleController extends \BaseController {

	protected $layout;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->layout = 'user.index';
		
		if ($this->userInfo->role_id == 1)
		{
			$this->layout = 'admin.index';
		}
	}


	/* === VIEW === */
	public function index()
	{
		$schedule = new Schedule;
		
		$data = array(
			'schedules' => $schedule->getAll(),
			'invoices'	=> Invoice::count()
		);

		if ( Request::ajax() )
		{
			return $this->loadDataTable();
		}
		else
		{
			$this->layout->content = View::make('user.schedules.index', $data);
		}		
	}

	public function create()
	{
		$schedule = new Schedule;
		
		$data = array(
			'invoices' => $schedule->invoicesWithoutSchedule(),
		);

		return View::make('user.schedules.create', $data);
	}	
	
	public function edit($id)
	{
		$schedule 		= new Schedule;
		$fromInvoice	= 0;
		
		if ( Request::segment(4) )
		{
			$fromInvoice = 1;
		}
		
		$data = array(
			'schedule' 	=> $schedule->getOne($id),
			'action'	=> $fromInvoice
		);
		
		return View::make('user.schedules.edit', $data);			
	}	
	/* === VIEW === */
	
	
	/* === C.R.U.D. === */
	public function store()
	{
		$rules = array(
			'invoice'	=> 'required',
			'startDate'	=> 'required',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{
			$store = new Schedule;
			$store->invoice_id 	= Input::get('invoice');
			$store->user_id 	= Auth::id();
			$store->start_date 	= Input::get('startDate');
			$store->save();		
		}	
		else
		{
			$schedule = new Schedule;
			
			$data = array(
				'invoices' 	=> $schedule->invoicesWithoutSchedule(),
				'errors'	=> $validator->errors()
			);

			return View::make('user.schedules.create', $data);				
		}
		
		if ( Input::get('action') )
		{
			return $this->loadInvoiceDataTable();					
		}
		
		return $this->loadDataTable();
	}
	
	public function update($id)
	{
		$rules = array(
			'startDate'	=> 'required',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{			
			$update 			= Schedule::find($id);
			$update->start_date	= Input::get('startDate');
			$update->save();					
		}
		else
		{
			$schedule = new Schedule;

			$data = array(
				'schedule'	=> $schedule->getOne($id),
				'errors'	=> $validator->errors()
			);
			
			return View::make('user.schedules.edit', $data);
		}

		if ( Input::get('action') == 1 )
		{
			return $this->loadInvoiceDataTable();
		}
		
		return $this->loadDataTable();	
	}	
	
	public function destroy($id)
	{
		$delete = Schedule::find($id);
		$delete->delete();
		
		return $this->loadDataTable();	
	}
	/* === END C.R.U.D. === */


	/* === PRIVATE === */
	private function loadDataTable()
	{
		$schedule = new Schedule;
		
		$data = array(
			'schedules' => $schedule->getAll(),
			'alert'		=> 1
		);		
		
		return View::make('user.schedules.table', $data);
	}
	
	public function loadInvoiceDataTable()
	{
		$invoice = new Invoice;
		
		$data = array(
			'invoices' => $invoice->getAll(),
			'alert'	=> 1
		);
		
		return View::make('user.invoices.table', $data);		
	}
	/* === END PRIVATE === */
	
	
	/* === ADDONS === */
	public function showCreateFormInvoice($id)
	{
		$data = array(
			'invoice' => Invoice::where('id', $id)->select('id')->first()
		);

		return View::make('user.schedules.addons.create-from-invoice', $data);
	}	
	/* === END ADDONS === */
}