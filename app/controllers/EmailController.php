<?php

class EmailController extends \BaseController {

	protected $layout = 'index';
	
	
	/* === VIEW === */
	public function sendEstimate($id)
	{
		$newEstimate 		= new Estimate;
		$estimate			= $newEstimate->getOne($id);
		$products			= new EstimateProduct;
		$invoiceSettings	= InvoiceSetting::find(1);
		$invoiceCode		= isset($invoiceSettings->code) ? $invoiceSettings->code : false;
			
		$data = array(
			'owner'			=> Setting::find(1),
			'logo'			=> Image::find(1),
			'estimate'		=> $estimate,
			'products'		=> $products->getProducs($id),
			'invoiceCode'	=> $invoiceCode
		);		
		
		$pathToFile 	= storage_path() . '/pdf/' . 'estimate_' . $estimate->estimate . '_' . date('Y-m-d') .'.pdf';
		$pdf 			= PDF::loadView('user.estimates.export.pdf', $data)->setPaper('letter')->setOrientation('portrait');
		$pdf->save($pathToFile);

		$clientEmail	= $estimate->email;
		$ownerEmail		= Setting::find(1)->receive_emails;
		
		if ($ownerEmail && $clientEmail)
		{
			$values = array(
				'text' 	=> trans('translate.new_estimate_from') . " " . Setting::find(1)->name
			);		
			
			Mail::send('assets.emails.email', $values, function($message) use ($ownerEmail, $clientEmail, $pathToFile)
			{
				$message->from($ownerEmail, trans('translate.app_name'));

				$message->to($clientEmail)->subject(trans('translate.new_estimate'));
				
				$message->attach($pathToFile);

			});	
		}
		
		unlink($pathToFile);
		
		return Redirect::back()->with('message', trans('translate.email_was_sent_to_client'));
	}	
	
	public function sendInvoice($id)
	{
		$newInvoice			= new Invoice;
		$products			= new InvoiceProduct;
		$invoiceSettings	= InvoiceSetting::find(1);
		$invoice			= $newInvoice->getOne($id);
		
		$data = array(
			'owner'			=> Setting::find(1),
			'logo'			=> Image::find(1),
			'invoice'		=> $invoice,
			'products'		=> $products->getProducs($id),
			'invoiceCode'	=> isset($invoiceSettings->code) ? $invoiceSettings->code : false
		);		
		
		$pathToFile 	= storage_path() . '/pdf/' . 'invoice_' . $invoice->number . '_' . date('Y-m-d') .'.pdf';
		$pdf 			= PDF::loadView('user.invoices.export.pdf', $data)->setPaper('letter')->setOrientation('portrait');
		$pdf->save($pathToFile);

		$clientEmail	= $invoice->email;
		$ownerEmail		= Setting::find(1)->receive_emails;
		
		if ($ownerEmail && $clientEmail)
		{
			$values = array(
				'text' 	=> trans('translate.new_invoice_from') . " " . Setting::find(1)->name
			);		
			
			Mail::send('assets.emails.email', $values, function($message) use ($ownerEmail, $clientEmail, $pathToFile)
			{
				$message->from($ownerEmail, trans('translate.app_name'));

				$message->to($clientEmail)->subject(trans('translate.new_invoice'));
				
				$message->attach($pathToFile);

			});	
		}
		
		unlink($pathToFile);
		
		return Redirect::back()->with('message', trans('translate.email_was_sent_to_client'));
	}	
	/* === END VIEW === */

}