<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	
	protected	$userInfo;
	protected	$userIsClient;
	
	public function __construct()
	{
		if ( Auth::check() )
		{
			$user			= User::where('id', Auth::id())->select('id', 'role_id', 'email', 'name')->first();	
			$userIsAdmin  	= $user->role_id == 1 ? true : false;	
			$userIsClient	= $user->role_id == 5 ? true : false;			
			
			$this->userInfo 	= $user;
			$this->userIsClient = $userIsClient;
			
			View::share('userIsClient', $userIsClient);
			View::share('user', $user);
			
			View::share('outOfStock', Product::where('quantity', '<', 1)->count());
			View::share('alertProduct', false);
			
			$alerts = Alert::find(1);
			View::share('alerts', $alerts);
			
			if ( isset($alerts->products) )
			{
				View::share('alertProduct', Product::where('quantity', '<', $alerts->products)->where('quantity', '>', 0)->count());
			}	
		}
	}
	
}