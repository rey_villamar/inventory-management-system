<?php

class UploadController extends \BaseController {

	protected $layout = 'admin.index';
	
	
	public function uploadImage()
	{
		if (Input::hasFile('image')) 
		{
			try 
			{
				$update = Image::where('user_id', Auth::id())->first();
				
				if ($update)
				{
					$update->name 	= $this->saveImageToServer(Input::file('image'), $update->width, $update->height);
					$update->save();			
				}
				else
				{
					$store			= new Image;
					$store->user_id	= Auth::id();
					$store->name	= $this->saveImageToServer(Input::file('image'), 100, 100);
					$store->width	= 100;
					$store->height	= 100;
					$store->save();						
				}
			}
			catch(Exception $e) 
			{
				return Redirect::to('admin/settings')->with('error', $e->getMessage()); 
			}
		}
		else
		{
			$data = array(
				'logo' 	=> Image::where('user_id', Auth::id())->first(),
				'alert'	=> 3
			);
			
			return View::make('admin.settings.logo', $data);				
		}

		$data = array(
			'logo' 	=> Image::where('user_id', Auth::id())->first(),
			'alert'	=> 1
		);
		
		return View::make('admin.settings.logo', $data);
	}

	public function imageSize()
	{
		$rules = array(
			'width'		=> 'required|integer',
			'height'	=> 'required|integer',
		);	
		
		$validator = Validator::make(array_map('trim', Input::all()), $rules);	
		
		if ($validator->passes())
		{
			$update = Image::where('user_id', Auth::id())->first();
			
			if ($update)
			{
				$update->fill(Input::all());
				$update->save();
			}
			else
			{
				$store = new Image;
				$store->user_id	= Auth::id();
				$store->fill(Input::all());
				$store->save();
			}
		}	
		else
		{
			$data = array(
				'logo'		=> Image::find(1),			
				'errors'	=> $validator->errors(),
				'inputs'	=> Input::all(),
			);
			
			return View::make('admin.settings.logo', $data);			
		}
		
		$data = array(
			'logo'	=> Image::find(1),			
			'alert'	=> 1
		);
		
		return View::make('admin.settings.logo', $data);		
	}
	
	private function saveImageToServer($image, $width, $height)
	{
		$filename  		= time() . '.' . $image->getClientOriginalExtension();
		$path 			= public_path('upload/' . $filename);
		
		$img 			= UploadImage::make($image->getRealPath());
		$img->resize($width, $height, function ($constraint) {
			$constraint->aspectRatio();
		});
		$img->save($path);
		
		return $filename;
	}
	
}