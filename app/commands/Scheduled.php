<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class Scheduled extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'create:cronjob';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set a cron job';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$invoice = new Invoice;
		$invoice->invoiceCronJob();
	}

}