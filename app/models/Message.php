<?php

class Message extends Eloquent {

	protected 	$guarded 	= array('id','user_id', 'from_id');
	protected 	$fillable 	= array('title', 'content', 'status', 'state');
	private 	$ownerID;
	
	public function __construct()
	{
		$this->ownerID = Auth::id();
	}

	public function getAll($type)
	{
		if ($type == 1)
		{
			$query = DB::table('messages')
					->join('users', 'users.id', '=', 'messages.from_id')
					->select(	'messages.*',
								'users.name as name'
							)
					->where('messages.user_id', $this->ownerID)
					->whereIn('messages.status', array(0, 1))
					->get();
		}

		if ($type == 2)
		{
			$query = DB::table('messages')
					->join('users', 'users.id', '=', 'messages.from_id')
					->select(	'messages.*',
								'users.name as name'
							)
					->where('messages.from_id', $this->ownerID)
					->where('messages.state', '=', 0)
					->whereIn('messages.status', array(0, 1, 2))
					->get();
		}

		return $query;
	}

	public function getOne($id, $type)
	{
		if ($type == 1)
		{
			$query = DB::table('messages')
					->join('users', 'users.id', '=', 'messages.from_id')
					->select(	'messages.*',
								'users.name as name'
							)
					->where('messages.user_id', $this->ownerID)
					->where('messages.id', $id)
					->first();
		}

		if ($type == 2)
		{
			$query = DB::table('messages')
					->join('users', 'users.id', '=', 'messages.from_id')
					->select(	'messages.*',
								'users.name as name'
							)
					->where('messages.from_id', $this->ownerID)
					->where('messages.id', $id)
					->first();
		}

		return $query;	
	}

	public function messageHistories($id)
	{
		if($id)
		{
		$query = DB::table('messages')
				->where('parent_id', $id)
				->orderBy('created_at', 'desc')
				->get();	
		
		return $query;	
		}
		
		return false;
	}
	
}