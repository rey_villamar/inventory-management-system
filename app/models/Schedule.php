<?php

class Schedule extends Eloquent {

	
	public function getAll()
	{
		$query = DB::table('schedules')
				->leftJoin('invoices', 'invoices.id', '=', 'schedules.invoice_id')
				->leftJoin('clients', 'clients.id', '=', 'invoices.client_id')
				->leftJoin('currencies', 'currencies.id', '=', 'invoices.currency_id')
				->select(	'schedules.id as scheduleID', 'schedules.start_date as scheduleDate', 
							'invoices.*',
							'clients.name',
							'currencies.name as currency', 'currencies.position'
						)
				->get();

		return $query;		
	}
	
	public function getOne($id)
	{
		$query = DB::table('schedules')
				->leftJoin('invoices', 'invoices.id', '=', 'schedules.invoice_id')
				->select(	'schedules.id as scheduleID', 'schedules.start_date',
							'invoices.number'
						)
				->where('schedules.id', $id)
				->first();

		return $query;			
	}
	
	public function invoicesWithoutSchedule()
	{
		$query = DB::table('schedules')
				->rightJoin('invoices', 'invoices.id', '=', 'schedules.invoice_id')
				->select('invoices.id', 'invoices.number')
				->whereNull('schedules.id')
				->get();

		return $query;		
	}
	
}