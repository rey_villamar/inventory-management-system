<?php

class InvoiceProduct extends Eloquent {

	public $timestamps = false;
	
	
	public function getProducs($invoiceID)
	{
		$query = DB::table('invoice_products')
				->leftJoin('products', 'products.id', '=', 'invoice_products.product_id')
				->select(	'invoice_products.*', 
							'products.name', 'products.description'
						)
				->where('invoice_products.invoice_id', $invoiceID)
				->get();

		return $query;		
	}
	
}