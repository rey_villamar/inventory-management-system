<?php

class Solso {

	
	public static function cutString($string)
	{
		$limit 	= 30;
		$data 	= substr($string, 0, $limit);
		
		if (strlen($string) > $limit)
		{
			return $data . ' ...';
		}
		
		return $data;
	}
	
	public static function currencyPosition($currency, $position, $amount)
	{
		if ($position == 1)	
		{
			return $currency . ' ' . $amount;
		}
		
		return $amount . ' ' . $currency;
	}
	
}