<?php

class Product extends Eloquent {

	protected 	$guarded 	= array('id', 'user_id');
	protected 	$fillable 	= array('name', 'quantity', 'code', 'price','retail','reseller','wholesale','bulk','factory', 'description');	
	
	
	public function productWithLowQuantity()
	{
			
	}

	// public function selectPriceType($user_id,$type,$prodName){
	// 	$sql = "SELECT $type FROM products WHERE user_id = $id AND name='$prodName'";
	// 	$query = $this->db->query($sql);
	// 	return $query->result();

	// }

	public function getPrice($priceType,$productID){
		$data = DB::table('products')->where('id', $productID)->pluck($priceType);
		return $data;
	}
}