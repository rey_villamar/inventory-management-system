<?php

class Invoice extends Eloquent {

	protected $guarded 	= array('id', 'user_id');
	protected $fillable = array('client_id', 'currency_id', 'number', 'amount','shipping_fee' ,'type', 'start_date', 'due_date', 'description', 'terms');	

	
	public function getAll() 
	{
		$userIsClient = Auth::user()->role_id == 5 ? true : false;
		
		$query = DB::table('invoices')
				->leftJoin('clients', 'clients.id', '=', 'invoices.client_id')
				->leftJoin('invoice_statuses', 'invoice_statuses.id', '=', 'invoices.status_id')
				->leftJoin('currencies', 'currencies.id', '=', 'invoices.currency_id')
				->leftJoin('invoice_payments','invoice_payments.invoice_id', '=', 'invoices.id')
				->leftJoin('schedules','schedules.invoice_id', '=', 'invoices.id')
				->select(	'invoices.*', 'invoices.id as invoiceID',
							'clients.name',
							'invoice_statuses.name as status',
							'currencies.name as currency', 'currencies.position',
							'schedules.id as scheduleID',
							DB::raw('IFNULL(SUM(`invoice_payments`.`payment_amount`), 0) as paid')
						)
						
				->where(function($querySplit) use ($userIsClient) {
				
					if ( $userIsClient )
					{
						$client = DB::table('users')
								->leftJoin('clients', 'clients.user_id', '=', 'users.id')
								->select('clients.id')
								->where('users.id', Auth::id())
								->first();
								
						$querySplit->where('invoices.client_id', $client->id);		
					}
					
				})						
						
				->orderBy('due_date', 'desc')
				->groupBy('invoices.id')
				->get();
		
		return $query;		
	}	
	
	public function get_mode_of_payment(){
		$query = DB::table('mode_of_payment')
				->get();
		return $query;	
			
	}
	
	public function updateMode($invoiceID,$selected){
			$query = DB::table('invoices')
			->where('id', $invoiceID)
			->update(array('mode_of_payment' => $selected));	
			return $query;	
			
	}
	
	
	public function getOne($id) 
	{
		$userIsClient = Auth::user()->role_id == 5 ? true : false;
		
		$query = DB::table('invoices')
				->leftJoin('clients', 'clients.id', '=', 'invoices.client_id')
				->leftJoin('invoice_statuses', 'invoice_statuses.id', '=', 'invoices.status_id')
				->leftJoin('currencies', 'currencies.id', '=', 'invoices.currency_id')
				->leftJoin('invoice_payments','invoice_payments.invoice_id', '=', 'invoices.id')
				->select(	'invoices.*', 'invoices.id as invoiceID',
							'clients.*', 'clients.id as clientID', 'clients.name as name',
							'invoice_statuses.name as status',
							'currencies.id as currencyID', 'currencies.name as currency', 'currencies.position',
							DB::raw('IFNULL(SUM(`invoice_payments`.`payment_amount`), 0) as paid')
						)
				->where('invoices.id', $id)	

				->where(function($querySplit) use ($userIsClient) {
				
					if ( $userIsClient )
					{
						$client = DB::table('users')
								->leftJoin('clients', 'clients.user_id', '=', 'users.id')
								->select('clients.id')
								->where('users.id', Auth::id())
								->first();
								
						$querySplit->where('invoices.client_id', $client->id);		
					}
					
				})		
				
				->first();
		
		return $query;	
	}	
	
	public function lastUnpaidInvoices()
	{
		$today = new DateTime('today');
		
		$query = DB::table('invoices')
				->join('clients', 'clients.id', '=', 'invoices.client_id')
				->leftJoin('invoice_statuses', 'invoice_statuses.id', '=', 'invoices.status_id')
				->select(	'invoices.id', 'invoices.number', 'invoices.due_date',
							'clients.name as client',
							'invoice_statuses.name as status'
						)				
				->where('invoices.user_id', Auth::id())	
				->whereIn('invoices.status_id', array(2, 3))
				->get();
				
		return $query;		
	}
	
	public function overdueInvoices()
	{
		$today = new DateTime('today');
		
		$query = DB::table('invoices')
				->join('clients', 'clients.id', '=', 'invoices.client_id')
				->select(	'invoices.id', 'invoices.number', 'invoices.due_date',
							'clients.name as client'
						)				
				->where('invoices.user_id', Auth::id())	
				->where('invoices.status_id', '5')
				->get();
				
		return $query;		
	}	
	
	public function invoiceStatus()
	{
		$today = new DateTime('today');
		
		DB::table('invoices')
				->whereIn('status_id', array(2, 3))
				->where('due_date', '<=', $today)
				->update(array('status_id' => 5));	
	}
	
	
	/* === CALCULATES AMOUNTS === */
		// public function calculateProductPrice($option, $productQty, $productPrice, $productTax, $productDiscount, $discountType)
	public function calculateProductPrice($option, $productQty, $productPrice)
	{
		$value		= $productQty * $productPrice;
		// $tax		= $value * ($productTax / 100);
		// $price		= $value + $tax;
		$discount	= 0;
		
		// if ( $discountType == 1 )
		// {			
		// 	$discount = $productDiscount;
		// }
		
		// if ( $discountType == 2 )
		// {
		// 	$discount = $price * ($productDiscount / 100);
		// }
		
		// if ($option == 1)
		// {
		// 	return $discount;
		// }
			
		// return round($price - $discount, 2);	
		return round($value, 2);	
	}	
	
	public function calculateInvoice($productQty, $productPrice)
	{
		$total 		= 0;
		$discount	= 0;
		
		foreach ($productQty as $k => $q)
		{
			$total += $this->calculateProductPrice(2, $productQty[$k], $productPrice[$k]);
		}
		
		
		// if ( $invoiceDiscountType == 1 )
		// {
		// 	$discount = $invoiceDiscount;
		// }
		
		// if ( $invoiceDiscountType == 2 )
		// {
		// 	$discount = $total * ($invoiceDiscount / 100);
		// }	
		
		// return round(abs($total - $discount), 2);
		return round(abs($total), 2);
	}	
	
	public static function calculateTotal($amount)
	// public static function calculateTotal($amount, $discount, $discount_type)
	{
		// $value = $discount;
		
		// if ($discount_type == '%')
		// {
		// 	$value = $amount * ($discount/100);
		// }
		
		$total = ($amount);
		// $total = ($amount - $value);
		
		return array(
			// 'discount'	=> $value,
			'total' 	=> round($total, 2)
		);
	}	

	public static function calculatePaid($amount, $paid)
	{
		return round($amount - $paid, 2);
	}
	/* === CALCULATES AMOUNTS === */
	
	
	/* === CHART === */
	public function invoiceChart()
	{
		$total = Invoice::where('user_id', Auth::id())->count();
		
		$query = DB::table('invoices')
				->leftJoin('invoice_statuses', 'invoice_statuses.id', '=', 'invoices.status_id')
				->select( DB::raw('COUNT(*) as num'), 'invoice_statuses.name' )
				->groupBy('invoices.status_id')
				->get();

		$count	= $query;
		$chart 	= array(
			'paid' => array(
				'count'		=> 0,
				'percent'	=> 0
			),
			'unpaid' => array(
				'count'		=> 0,
				'percent'	=> 0
			),		
			'partiallypaid' => array(
				'count'		=> 0,
				'percent'	=> 0
			),	
			'cancelled' => array(
				'count'		=> 0,
				'percent'	=> 0
			),		
			'overdue' => array(
				'count'		=> 0,
				'percent'	=> 0
			)			
		);
		
		foreach ($count as $v)
		{
			$chart[str_replace(' ', '', $v->name)] = array(
				'count'		=> $v->num,
				'percent'	=> $total > 0 ? ( $v->num * 100) / $total : 0
			);
		}
		
		return $chart;
	}	
	/* === CHART === */
	

	/* === CRONJOB === */
	public function invoiceCronJob()
	{
		$day	= date('d');
		$crons	= DB::table('schedules')->get();
		
		foreach ($crons as $cron)
		{
			if ($day == $cron->start_date)
			{
				$this->cloneInvoice($cron->invoice_id);
			}
		}	
	}
	
	private function cloneInvoice($invoiceID)
	{
		$newInvoice 		= DB::table('invoices')->where('id', $invoiceID)->first();
		$invoiceSettings	= DB::table('invoice_settings')->where('user_id', 1)->first();

		if (isset($invoiceSettings->number))
		{
			$invoiceNumber 				= $invoiceSettings->number + 1;
			$invoiceSettings->number	= $invoiceNumber;
			$invoiceSettings->save();
		}

		$store					= new Invoice;
		$store->user_id			= $newInvoice->user_id;
		$store->client_id		= $newInvoice->client_id;
		$store->status_id		= 2;
		$store->currency_id		= $newInvoice->currency_id;
		$store->number			= isset($invoiceSettings->number) ? $invoiceNumber : 0;
		$store->amount			= $newInvoice->amount;
		$store->tax				= $newInvoice->tax;
	
		
		$store->discount		= $newInvoice->discount;
		$store->type   			= $newInvoice->type;		
		$store->start_date		= date('Y-m-d');
		$store->due_date		= date('Y-m-d', strtotime("+30 days"));
		$store->description		= $newInvoice->description;
		$store->terms			= $newInvoice->terms;
		$store->save();		
		
		$invoice = new Invoice;
		$invoice->invoiceStatus();				
	}	
	/* === END CRONJOB === */	

	public function saveShippingFee($shipping_fee){
		DB::table('invoices')->insert(
    	array(
          'shipping_fee' => $shipping_fee)
		);
	}
	
}