<?php

class Alert extends Eloquent {

	protected $guarded 	= array('id');
	protected $fillable = array('products');
	public	$timestamps	= false;
	
}