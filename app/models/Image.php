<?php

class Image extends Eloquent {
	
	protected 	$guarded 	= array('id', 'user_id');
	protected 	$fillable 	= array('name', 'width', 'height');		
	public 		$timestamps	= false;
	
}