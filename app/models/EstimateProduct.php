<?php

class EstimateProduct extends Eloquent {

	public $timestamps = false;
	
	
	public function getProducs($estimateID)
	{
		$query = DB::table('estimate_products')
				->leftJoin('products', 'products.id', '=', 'estimate_products.product_id')
				->select(	'estimate_products.*', 
							'products.name', 'products.description'
						)
				->where('estimate_products.estimate_id', $estimateID)
				->get();

		return $query;		
	}

}