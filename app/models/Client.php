<?php

class Client extends Eloquent {

	protected $guarded 	= array('id', 'user_id');
	protected $fillable = array('name', 'country', 'state', 'city', 'zip', 'address', 'contact', 'phone', 'email', 'website', 'bank', 'bank_account', 'description');
	
	private $owner;

	public function __construct()
	{
		$this->owner = Auth::id();	
	}
	
	
	public function insertClient(){
		$result = DB::getPdo()->lastInsertId();;
		// $result DB::table('clients')->insert( $data )->lastInsertId();


		return $result;
	}

	public function addClient($name, $address, $email, $contact, $description,$user_id){
		// $user = new User;$user_id
		$query =  DB::table('clients')->insert(
    		['name' => $name, 'address' => $address,'email' => $email, 'contact'=>$contact,'description'=>$description,'user_id'=>$user_id]
		);
		$result = DB::getPdo()->lastInsertId();
		return $result;
	}
	public function getAll()
	{
		$query = DB::table('clients')
					->leftJoin('users','users.id', '=', 'clients.user_id')
					->leftJoin('invitations','invitations.client_id', '=', 'clients.id')
					->select(	'clients.*',
								'invitations.status'
						)
					->where('users.parent_id', $this->owner)
					->orderBy('id', 'desc')
					->get();		
					
		return $query;			
	}	
	
}