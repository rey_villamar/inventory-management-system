<?php

class Currency extends Eloquent {

	public $timestamps = false;
	
	
	public function defaultCurrency()
	{
		$query = DB::table('settings')
				->leftJoin('currencies', 'currencies.id', '=', 'settings.currency_id')
				->select('currencies.name', 'currencies.position')
				->first();

		return $query;
	}	
	
}