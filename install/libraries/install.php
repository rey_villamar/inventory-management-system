<?php

/**
 *-----------------------------------------------------------------------------------
 * === CLASS Install ===
 *-----------------------------------------------------------------------------------
 * Install Class
 * 
 */

class Install {

	private $segments;
	
	
	public function __construct()
	{
		$this->segments = $this->routeTo();		
	}
	
	public function start()
	{
		if ($this->checkInstalation() == 0)
		{
			if ( phpversion() < 5.4 )
			{
				$data = array(
					'content'	=> 'error_php'
				);
						
				$this->loadView('index', $data);
			}
			else
			{
				if ($this->segments[0] == '')
				{
					$this->runStep1();
				}
				elseif ($this->segments[0] == 'database')
				{
					$this->runStep2();
				}				
				elseif ($this->segments[0] == 'success')
				{
					$this->runStep3();
				}
				else
				{
					$data = array(
						'content'	=> 'error_404'
					);
							
					$this->loadView('index', $data);
				}
			}	
			
			exit;
		}
	}
	
	
	private function runStep1()
	{
		$data = array(
			'phpVersion' 		=> phpversion(),
			'pdo' 				=> extension_loaded('PDO'),
			'pdo_mysql'			=> extension_loaded('pdo_mysql'),
			'mcrypt'			=> extension_loaded("mcrypt"),
			'file_info'			=> finfo_open(FILEINFO_MIME_TYPE),

			'mod_rewrite'		=> in_array('mod_rewrite', apache_get_modules()),
			
			'dir_lang'			=> is_writable(dirname(LARAVEL_PATH.'lang/')),
			'dir_logs'			=> is_writable(dirname(LARAVEL_PATH.'storage/logs/')),
			'dir_sessions'		=> is_writable(dirname(LARAVEL_PATH.'storage/sessions/')),
			'dir_views'			=> is_writable(dirname(LARAVEL_PATH.'storage/views/')),			
			'dir_upload'		=> is_writable(dirname('public/upload/')),

			'file_config'		=> is_writable(BASE_PATH .'config/index.php'),
			'file_translate'	=> is_writable(LARAVEL_PATH.'lang/en/translate.php'),
			'file_database'		=> is_writable(LARAVEL_PATH.'config/database.php'),
			'file_log'			=> $this->checkFileLog(),
			
			'continue'			=> $this->checkSettings(),
			
			'content'			=> 'step1'
		);		

		$this->loadView('index', $data);		
	}
	
	private function runStep2()
	{
		$data = array(
			'formUrl'	=> BASE_URL . $this->segments[0],
			'content'	=> 'step2'
		);	
			
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			if (
				strlen($_POST['host']) 		== 0	||
				strlen($_POST['database']) 	== 0	||
				strlen($_POST['username']) 	== 0	||
				strlen($_POST['password']) 	== 0	
			)
			{
				$data = array(
					'formUrl'	=> BASE_URL . $this->segments[0],
					'content'	=> 'step2'
				);
			}
			else
			{
				$con = $this->checkConnection('mysql', $_POST['host'], $_POST['database'], $_POST['username'], $_POST['password']);

				if ($con)
				{
					$this->parseDatabaseConfig($_POST['host'], $_POST['database'], $_POST['username'], $_POST['password']);
					$this->loadSqlFile();

					header("location:" . BASE_URL . "success");					
				}
				else
				{
					$data = array(
						'formUrl'		=> BASE_URL . $this->segments[0],
						'connection'	=> false,
						'content'		=> 'step2'
					);
				}
			}
		}

		$this->loadView('index', $data);		
	}
	
	private function runStep3()
	{
		$contents = "<?php return array(\n";
		$contents .= '"install" => "1"';
		$contents .= "\n);";
		
		file_put_contents(BASE_PATH .'config/index.php', $contents);
		
		$data = array(
			'content'	=> 'step3'
		);		
		
		$this->loadView('index', $data);
	}
	
	
	private function checkInstalation()
	{
		$data = include_once BASE_PATH . 'config/index.php';
		
		return $data['install'];		
	}
	
	private function checkSettings()
	{
		if 
		( 
			extension_loaded('PDO')									&&
			extension_loaded('pdo_mysql')							&&
			extension_loaded("mcrypt")								&&

			in_array('mod_rewrite', apache_get_modules())			&&
			
			is_writable(dirname(LARAVEL_PATH.'lang/'))				&&
			is_writable(dirname(LARAVEL_PATH.'storage/logs/'))		&&
			is_writable(dirname(LARAVEL_PATH.'storage/sessions/'))	&&
			is_writable(dirname(LARAVEL_PATH.'storage/views/'))		&&
			is_writable(dirname('public/upload/'))					&&
			
			is_writable(BASE_PATH .'config/index.php')				&&
			is_writable(LARAVEL_PATH.'lang/en/translate.php')		&&
			is_writable(LARAVEL_PATH.'config/database.php')			&&
			$this->checkFileLog()									
		)
		{
			return true;
		}
		
		return false;
	}
	
	private function checkFileLog()
	{
		$filename = LARAVEL_PATH . 'storage' . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'laravel.log';

		if (file_exists($filename)) 
		{
			if (is_writable($filename))
			{
				return true;
			}
		}
		else
		{
			if ( file_put_contents($filename, '') )
			{
				return true;
			}
			
			return false;
		}		
		
		return false;
	}	
	
	private function checkConnection($driver, $host, $database, $username, $password)
	{
		$options = array(
			PDO::ATTR_PERSISTENT => true,
			PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION
		);	
		
		try {
			$db = new PDO(
				$driver . ':host=' .
				$host . ';dbname='.
				$database,
				$username,
				$password,
				$options
			);
			
			return true;
		} catch(PDOException $e) {
			return false;
		}
	}
	
	private function loadSqlFile()
	{
		$sqlFile	= "database";
		$filename 	= BASE_PATH . 'data/sql/' . $sqlFile . ".sql";

		if (file_exists($filename) && filesize($filename) != 0) 
		{
			$data = require_once LARAVEL_PATH . 'config/database.php';

			$options = array(
				PDO::ATTR_PERSISTENT => true,
				PDO::ATTR_ERRMODE    => PDO::ERRMODE_EXCEPTION
			);
		
			try {
				$db = new PDO(
					$data['connections']['mysql']['driver'] . ':host=' .
					$data['connections']['mysql']['host'] . ';dbname='.
					$data['connections']['mysql']['database'],
					$data['connections']['mysql']['username'],
					$data['connections']['mysql']['password'],
					$options
				);
			} catch(PDOException $e) {
				echo $e->getMessage();
			}			

			$sql = file_get_contents($filename);
			$db->exec($sql);
		}
	}

	private function parseDatabaseConfig($host, $database, $username, $password)
	{

$contents = "<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| PDO Fetch Style
	|--------------------------------------------------------------------------
	|
	| By default, database results will be returned as instances of the PHP
	| stdClass object; however, you may desire to retrieve records in an
	| array format for simplicity. Here you can tweak the fetch style.
	|
	*/

	'fetch' => PDO::FETCH_CLASS,

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => 'mysql',

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => array(

		'sqlite' => array(
			'driver'   => 'sqlite',
			'database' => __DIR__.'/../database/production.sqlite',
			'prefix'   => '',
		),

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => '" . $host . "',
			'database'  => '" . $database . "',
			'username'  => '" . $username . "',
			'password'  => '" . $password . "',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'pgsql' => array(
			'driver'   => 'pgsql',
			'host'     => '',
			'database' => '',
			'username' => '',
			'password' => '',
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		),

		'sqlsrv' => array(
			'driver'   => 'sqlsrv',
			'host'     => 'localhost',
			'database' => 'database',
			'username' => 'root',
			'password' => '',
			'prefix'   => '',
		),

	),

	/*
	|--------------------------------------------------------------------------
	| Migration Repository Table
	|--------------------------------------------------------------------------
	|
	| This table keeps track of all the migrations that have already run for
	| your application. Using this information, we can determine which of
	| the migrations on disk haven't actually been run in the database.
	|
	*/

	'migrations' => 'migrations',

	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => array(

		'cluster' => false,

		'default' => array(
			'host'     => '127.0.0.1',
			'port'     => 6379,
			'database' => 0,
		),

	),

);";		
		file_put_contents(LARAVEL_PATH .'config/database.php', $contents);	
	}
	
	
	private function loadView($name, $data = array())
	{
		extract($data);
		include_once BASE_PATH . 'views/'. $name . '.php';
	}
	
	private function routeTo()
	{
        $uri = $_SERVER['REQUEST_URI'];

        if (strpos($uri, $_SERVER['SCRIPT_NAME']) === 0) {
            $uri = substr($uri, strlen($_SERVER['SCRIPT_NAME']));
        }

        if (strpos($uri, dirname($_SERVER['SCRIPT_NAME'])) === 0) {
            $uri = substr($uri, strlen(dirname($_SERVER['SCRIPT_NAME'])));
        }
        return explode('/', trim($uri, '/'));
	}
	
}