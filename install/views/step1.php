<div class="container">
<div class="row">
	<div class="col-md-12">
		<h1 class="text-center">
			- Inventory System Installer -
		</h1>

		<h3 class="text-center">
			Installation guide - STEP 1
		</h3>
	
		<?php if (!$continue) { ?>
			<div role="alert" class="alert alert-danger text-center">
				<h4>
					Please check your server is set-up correctly !
				</h4>	
				
				<p>
					you can not continue until you fix all errors !
				</p>
			</div>
		<?php } ?>
		
		<table class="table">
			<tr>
				<td class="table-th" colspan="4">
					Please configure your PHP settings to match requirements listed below.
				</td>
			</tr>
			
			<tr>
				<td class="col-md-3">
					Required PHP Settings
				</td>

				<td class="col-md-3">
					Current Settings
				</td>	

				<td class="col-md-3">
					Required Settings
				</td>	

				<td class="col-md-3">
					Status
				</td>
			</tr>
			
			<tr>
				<td>
					PHP Version
				</td>

				<td>
					<?php echo $phpVersion;?> 
				</td>	

				<td>
					5.4+
				</td>
				
				<td class="col-md-4">
					<?php if ($phpVersion >= 5.4) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>
				</td>
			</tr>   

			<tr>                
				<td>
					PDO Support 
				</td>	
				
				<td>
					<?php if ($pdo == 1) { ?>
						<label class="label-green">enabled</label>
					<?php } else { ?>
						<label class="label-red">disabled</label>
					<?php } ?>				
				</td>				
				
				<td>
					enabled
				</td>
				
				<td>
					<?php if ($pdo == 1) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>					
				</td>				
			</tr>		

			<tr>                
				<td>
					Database Extension (pdo_mysql)
				</td>	
				
				<td>
					<?php if ($pdo_mysql == 1) { ?>
						<label class="label-green">enabled</label>
					<?php } else { ?>
						<label class="label-red">disabled</label>
					<?php } ?>						
				</td>				
				
				<td>
					enabled
				</td>
				
				<td>
					<?php if ($pdo_mysql == 1) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>			
				</td>				
			</tr>
			
			<tr> 
				<td>
					MCrypt PHP Extension
				</td>	                
				
				<td>
					<?php if ($mcrypt) { ?>
						<label class="label-green">enabled</label>
					<?php } else { ?>
						<label class="label-red">disabled</label>
					<?php } ?>						
				</td>				
				
				<td>
					enabled
				</td>
				
				<td>
					<?php if ($mcrypt) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>				
				</td>
			</tr>		

			<tr>
				<td class="table-th" colspan="4">
					Modes
				</td>
			</tr>
			
			<tr>                 
				<td>Mode Rewrite</td>	                
				
				<td>
					<?php if ($mod_rewrite) { ?>
						<label class="label-green">installed</label>
					<?php } else { ?>
						<label class="label-red">not installed</label>
					<?php } ?>					
				</td>				
				
				<td>
					enabled
				</td>
				
				<td>
					<?php if ($mod_rewrite) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>					
				</td>
			</tr>	

			<tr> 
				<td>
					PHP_FILEINFO extension
				</td>	                
				
				<td>
					<?php if ($file_info) { ?>
						<label class="label-green">enabled</label>
					<?php } else { ?>
						<label class="label-red">disabled</label>
					<?php } ?>						
				</td>				
				
				<td>
					enabled
				</td>
				
				<td>
					<?php if ($file_info) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>				
				</td>
			</tr>			
			
			<tr>
				<td class="table-th" colspan="4">
					Directories
				</td>
			</tr>
			
			<tr>  
				<td>
					app/lang/
				</td>	                
				
				<td>
					<?php if ($dir_lang) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>				
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($dir_lang) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>			
				</td>
			</tr>		

			<tr>  
				<td>
					app/storage/logs/
				</td>	                
				
				<td>
					<?php if ($dir_logs) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>				
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($dir_logs) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>			
				</td>
			</tr>			

			<tr>  
				<td>
					app/storage/sessions/
				</td>	                
				
				<td>
					<?php if ($dir_sessions) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>						
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($dir_sessions) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>					
				</td>
			</tr>			

			<tr>  
				<td>
					app/storage/views/
				</td>	                
				
				<td>
					<?php if ($dir_views) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>						
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($dir_views) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>			
				</td>
			</tr>
			
			<tr>  
				<td>
					public/upload
				</td>	                
				
				<td>
					<?php if ($dir_upload) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>						
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($dir_upload) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>			
				</td>
			</tr>			
			
			<tr>
				<td class="table-th" colspan="4">
					Files
				</td>
			</tr>	
			
			<tr>  
				<td>
					install/config/index.php
				</td>	
				
				<td>
					<?php if ($file_config) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>						
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($file_config) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>				
				</td>
			</tr>			
			
			<tr>  
				<td>
					app/lang/en/translate.php
				</td>	
				
				<td>
					<?php if ($file_translate) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>						
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($file_translate) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>				
				</td>
			</tr>			
		
			<tr>  
				<td>
					app/config/database.php
				</td>
				
				<td>
					<?php if ($file_database) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>					
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($file_database) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>					
				</td>
			</tr>				
			
			<tr>  
				<td>
					app/storage/logs/laravel.log
				</td>
				
				<td>
					<?php if ($file_log) { ?>
						<label class="label-green">writable</label>
					<?php } else { ?>
						<label class="label-red">not writable</label>
					<?php } ?>					
				</td>				
				
				<td>
					writable
				</td>
				
				<td>
					<?php if ($file_log) { ?>
						<label class="label-green">passed</label>
					<?php } else { ?>
						<label class="label-red">not passed</label>
					<?php } ?>					
				</td>
			</tr>			
			
			<?php if ($continue) { ?>
				<tr>
					<td colspan="4">
						<!--<a href="<?php echo BASE_URL . 'index.php?step=2';?>" class="btn btn-success pull-right">-->
						<a href="<?php echo BASE_URL . 'database';?>" class="btn btn-success pull-right">
							Continue
						</a>
					</td>
				</tr>
			<?php } ?>	
		</table>   
	</div>	
</div>
</div>