<div class="container">
<div class="row">	
	<div role="alert" class="alert alert-danger text-center">
		<h2>
			You need at least PHP 5.4 to run this application !!!
		</h2>	
		
		<p>
			Your current PHP version is <?php echo phpversion();?>
		</p>
	</div>
</div>
</div>