<div class="container">
<div class="row">
	<div class="col-md-12">
		<h1 class="text-center">
			- Inventory System Installer -
		</h1>

		<h3 class="text-center">
			Installation guide - STEP 2
		</h3>

		<div role="alert" class="alert alert-info text-center">
			<h4>
				Configure your database by filling out the following fields.
			</h4>	
			
			<p>
				Before getting started, we need some information on the database. You will need to know the following items before proceeding.
			</p>
		</div>

		<div id="database" class="thumbnail">

			<?php if(isset($connection)) { ?>
				<div role="alert" class="alert alert-danger text-center">
					<h3>
						Error establishing a database connection !!!
					</h3>	
					
					<p>
						This either means that the username and password information provided are incorrect or we can't contact the database server at localhost. This could mean your host's database server is down.
					</p>
				</div>	
			<?php } ?>	
			
			<form role="form" action="<?php echo $formUrl;?>" method="POST" class="parsleyValidation" accept-charset="UTF-8">
				<div class="form-group">
					<label for="host">Database host</label>
					<input type="text" name="host" class="form-control required" 
					data-toggle="popover" title="Database host - field required" data-content="You should be able to get this info from your web host, if localhost does not work."
					autocomplete="off">
					
					<?php if( (isset($_POST['host'])) && (strlen($_POST['host']) == 0) ) { ?>
						<p class="error">This value is required.</p>
					<?php } ?>				
				</div>
				
				<div class="form-group">
					<label for="database">Database name</label>
					<input type="text" name="database" class="form-control required" 
					data-toggle="popover" title="Database name - field required" data-content="The name of the database you want to run app in"
					autocomplete="off">
					
					<?php if( (isset($_POST['database'])) && (strlen($_POST['database']) == 0) ) { ?>
						<p class="error">This value is required.</p>
					<?php } ?>				
				</div>				
				
				<div class="form-group">
					<label for="username">Database username</label>
					<input type="text" name="username" class="form-control required" 
					data-toggle="popover" title="Database username - field required" data-content="Your MySQL username"
					autocomplete="off">
									
					<?php if( (isset($_POST['username'])) && (strlen($_POST['username']) == 0) ) { ?>
						<p class="error">This value is required.</p>
					<?php } ?>								
				</div>
				
				<div class="form-group">
					<label for="password">Database password</label>
					<input type="password" name="password" class="form-control required" 
					data-toggle="popover" title="Database password - field required" data-content="Your MySQL password"
					autocomplete="off">
					
					<?php if( (isset($_POST['password'])) && (strlen($_POST['password']) == 0) ) { ?>
						<p class="error">This value is required.</p>
					<?php } ?>				
				</div>
				
				<div class="form-group">
					<button class="btn solso-submit btn-block" type="submit">
						<i class="fa fa-check"></i> Submit
					</button>
				</div>
			</form>
		</div>	
	</div>	
</div>	
</div>
