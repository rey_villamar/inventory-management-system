<div class="container">
<div class="row">	
	<div class="col-md-12">
		<h1 class="text-center">
			- Inventory System Installer -
		</h1>

		<h3 class="text-center">
			Installation guide - STEP 3
		</h3>

		<div role="alert" class="alert alert-success text-center">
			<h3>
				Your application was successfully installed !!!
			</h3>
			
			<p>
				If you want you can go to index.php and comment the following code line <br>
				require 'install/index.php';
			</p>
		</div>		
	</div>	
	
	<div class="col-md-4 col-md-offset-4">
		<a href="<?php echo BASE_URL;?>" class="btn solso-submit btn-block">
			Click to run your application
		</a>
	</div>
</div>
</div>