<?php

/**
 *-----------------------------------------------------------------------------------
 * === ERROR REPORTING ===
 *-----------------------------------------------------------------------------------
 * Set error reporting and display errors settings
 * development	= error_reporting(E_ALL);
 * production	= error_reporting(0);
 *
 */
error_reporting(0);


/**
 *-----------------------------------------------------------------------------------
 * === DEFINE BASE URL OF APPLICATION ===
 *-----------------------------------------------------------------------------------
 */
$script     = $_SERVER['SCRIPT_NAME'];
$baseUrl    = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off'
            ? 'https'
            : 'http';
$baseUrl .= '://' . $_SERVER['HTTP_HOST'];
$baseUrl .= str_replace(basename($script), '', $script);


/**
 *-----------------------------------------------------------------------------------
 * === DEFINE CONSTANTS ===
 *-----------------------------------------------------------------------------------
 */

define('BASE_URL', $baseUrl);
define('PUBLIC_URL', $baseUrl . 'install/public/');
define('LARAVEL_PATH', dirname(__DIR__) . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR);
define('BASE_PATH', (__DIR__) . DIRECTORY_SEPARATOR);


/**
 *-----------------------------------------------------------------------------------
 * === LOAD LIBRARIES ===
 *-----------------------------------------------------------------------------------
 * These are the classes located in the libraries folder
 *
 */

require_once BASE_PATH . 'libraries/install.php';


/**
 *-----------------------------------------------------------------------------------
 * === RUN INSTALLER ===
 *-----------------------------------------------------------------------------------
 * Initialize the class
 * Run method "start"
 */
 
$install = new Install;
$install->start();


/* end of file index.php */